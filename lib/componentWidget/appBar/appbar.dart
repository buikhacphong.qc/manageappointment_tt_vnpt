import 'package:flutter/material.dart';

class BaseAppBar extends StatelessWidget implements PreferredSizeWidget {
  final Color backgroundColor = const Color(0xFF6f9bd4);
  final Text title;
  final AppBar appBar;
  final List<Widget> widgets;
  final Function()? onPress;
  const BaseAppBar({
    Key? key,
    required this.title, 
    required this.appBar, 
    required this.widgets, 
    this.onPress
  }) : super(key: key);    

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: IconButton(
          onPressed: onPress,
          // (){
          //   //Navigator.pop(context);
          //   Navigator.of(context).pop();
          //}, 
          
          icon: Icon(Icons.chevron_left)
        ),
      centerTitle: true,
      title: title,
      backgroundColor: backgroundColor,
      actions: widgets,
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(appBar.preferredSize.height);
}