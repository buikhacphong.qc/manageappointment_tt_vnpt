import 'dart:io';
import 'package:flutter/material.dart';

import '../styles/CustomColor.dart';
import '../styles/Dimens.dart';

class TextFieldCheckBox extends StatelessWidget {
  final String title;
  final String title2;
  final Color? color;
  final TextEditingController? textController;
  final Function()? onTap;
  final Function(String)? onChanged;
  final String? Function(String?)? validate;

  const TextFieldCheckBox(
      {Key? key,
      required this.title,
      this.color,
      this.textController,
      this.onChanged,
      required this.onTap,
      required this.title2, 
      this.validate, 
      }): super(key: key);
  
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double deviceHeight = MediaQuery.of(context).size.height;
    final isSmallMobile =
        Platform.isAndroid ? deviceHeight < 600 : deviceHeight < 700;
    return Row(
      children: <Widget>[
        Expanded(
          flex: 4,
          child: Align(
            alignment: Alignment.topLeft,       
            child: RichText(
              text: TextSpan(
                text: title,
                  style: new TextStyle(
                    color: Color(0xFF535858),
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                  ),
                children: [
                  TextSpan(
                    text: title2,
                    style: TextStyle(
                      color: Colors.red,
                    )
                  )
                ]
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ),
        Expanded(
          flex: 6,
          child: Container(
            height: 40,
            child: TextFormField(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              style: TextStyle(
                color: color ?? Colors.black,
                fontSize: 14,
                fontWeight: FontWeight.w400
              ),
              controller: textController,
              validator: title2 == '' ? null : validateCheckbox,
              onTap: onTap,
              onChanged: onChanged,
              decoration: new InputDecoration(
                errorStyle: TextStyle(
                  fontSize: 12,
                  height: 0.4
                ),
                contentPadding: const EdgeInsets.only(
                  left: defaultTextInsideBoxPadding, right: 10),
                fillColor: Colors.white,
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(
                    color: primaryColor, width: 2.0
                  )
                ),
                enabledBorder: const OutlineInputBorder(
                  borderSide: const BorderSide(
                    color: Colors.grey, width: 1.0
                  )
                ),
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(4.0)
                ),
                suffixIcon: Icon(Icons.arrow_drop_down, color: Colors.grey,),
                suffixIconConstraints: BoxConstraints(
                  minHeight: 25, 
                  minWidth: 30
                ) 
              ),
              showCursor: false,
              readOnly: true,
            ),
          ),
        ),
      ],
    );
  }
}

String? validateCheckbox(String? value){
  if(value == '--Lựa chọn--'){
    return 'Không được để trống';
  }else{
    return null;
  }
}
