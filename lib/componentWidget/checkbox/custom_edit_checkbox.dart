import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

import '../styles/CustomColor.dart';

class editCheckBox extends StatefulWidget {
  final String title;
  final bool? checks;
  //final bool? checkedValue;
  final ValueChanged<bool> onChanged;
  const editCheckBox({
    super.key, 
    required this.checks, 
    required this.title,
    //this.checkedValue, 
    required this.onChanged
  });

  @override
  State<editCheckBox> createState() => _editCheckBoxState();
}

class _editCheckBoxState extends State<editCheckBox> {
  // late bool isChecked;

  // @override
  // void initState() {
  //   super.initState();
  //   isChecked = widget.checks!;
  // }
  // bool stringToBool(String value){
  // if (value.toLowerCase() == 'false'){
  //   return false;
  // }
  // return true;
  // }

  @override
  Widget build(BuildContext context) {
    //bool checkedValue = stringToBool(widget.checks);
    //bool? checkedValue = widget.checks;
    late bool isChecked;
    isChecked = widget.checks!;
  // @override
  // void initState() {
  //   super.initState();
  //   isChecked = widget.checks!;
  // }
    return StatefulBuilder(
      builder: (BuildContext context, StateSetter setState){
        return Center(
          child: CheckboxListTile(
            
            activeColor: primaryColor,
            title: Text(
              widget.title,
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w400
              ),
            ),
            value: isChecked,
            // onChanged: (newValue) {
            //   setState(() {
            //     checkedValue = newValue!;
            //     print(checkedValue);
            //   });
            // },
            onChanged: (value) {
              setState(() {
                isChecked = value!;
              });
              widget.onChanged!(value!);
            },
            controlAffinity: ListTileControlAffinity.leading, 
            visualDensity: VisualDensity(vertical: -3),
          )
        );
      }
    );
  }
}