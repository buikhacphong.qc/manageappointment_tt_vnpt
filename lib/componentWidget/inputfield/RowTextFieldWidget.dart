import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../icon/qlhk_icons.dart';
import '../styles/CustomColor.dart';
import '../styles/Dimens.dart';

class RowTextField extends StatefulWidget {
  final String title;
  final String title2;
  final Color? color;
  final TextEditingController? textController;
  final bool? isShowIcon;
  final bool? isNumberKeyboard;
  final Function(String)? onChanged;
  final String? Function(String?)? validate;
  //final String? savevalue;
  const RowTextField(
      {Key? key,
      required this.title,
      this.color,
      this.textController,
      this.isShowIcon,
      this.onChanged,
      this.isNumberKeyboard = false, 
      required this.title2, 
      this.validate, })
      : super(key: key);

  @override
  State<RowTextField> createState() => _RowTextFieldState();
}

class _RowTextFieldState extends State<RowTextField> {
  late String check;
  @override
  Widget build(BuildContext context) {
    //String  check = '';
    return Row(
      children: <Widget>[
        Expanded(
          flex: 4,
          child: Align(
            alignment: Alignment.topLeft,
            //thay doi de them *
            // child: Text(
            //   title,
            //   style: new TextStyle(
            //     color: color ?? Color(0xFF535858),
            //     fontSize: 14,
            //     fontWeight: FontWeight.w400,
            //   ),
            //   textAlign: TextAlign.left,
            // ),
            child: RichText(
              text: TextSpan(
                text: widget.title,
                  style: new TextStyle(
                    color: widget.color ?? Color(0xFF535858),
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                  ),
                children: [
                  TextSpan(
                    text: widget.title2,
                    style: TextStyle(
                      color: Colors.red,
                    )
                  )
                ]
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ),
        Expanded(
          flex: 6,
          child: Container(
            height: 40,
            child: TextFormField( 

              cursorColor: primaryColor,   
              autovalidateMode: AutovalidateMode.onUserInteraction,        
              maxLines: 1,
              keyboardType: widget.isNumberKeyboard!
                  ? TextInputType.number
                  : TextInputType.text,
              style: TextStyle(
                color: widget.color ?? Color(0xFF444444),
                fontSize: 14,
                fontWeight: FontWeight.w400,
                decoration: TextDecoration.none,
                decorationThickness: 0
              ),
              controller: widget.textController,
              validator: widget.title2 == '' ? null : validateTextField,
              //onSaved: savevalue,
              //onChanged: onChanged,
              onChanged: (text) => setState(() => check = text),
              decoration: new InputDecoration(
                //isDense: false,
                errorStyle: TextStyle(fontSize: 12, height: 0.4),
                contentPadding: const EdgeInsets.only(
                    left: defaultTextInsideBoxPadding, right: 0, ),
                fillColor: Colors.white,
                focusedBorder: const OutlineInputBorder(
                  borderSide: BorderSide(color: primaryColor, width: 2.0)
                ),
                enabledBorder: const OutlineInputBorder(
                  borderSide:
                      const BorderSide(color: Colors.grey, width: 1.0),
                ),
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(4.0),
                ),
                suffixIcon: Visibility(
                  visible: widget.isShowIcon ?? false,
                  child: Icon(
                    Qlhk.check_circle,
                    size: 14,
                    color: secondColor,
                  ),
                ),
                suffixIconConstraints: BoxConstraints(
                  minWidth: 30,
                  minHeight: 25,
                ),
              )),
          ),
        ),
      ],
    );
  }
}
/*
r'^
  (?=.*[A-Z])       // should contain at least one upper case
  (?=.*[a-z])       // should contain at least one lower case
  (?=.*?[0-9])      // should contain at least one digit
  (?=.*?[!@#\$&*~]) // should contain at least one Special character
  .{8,}             // Must be at least 8 characters in length  
$
*/
String? validatePassword(String? value){
  RegExp regex = RegExp(
    r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$'
  );
  if(value!.isEmpty){
    return 'Mật khẩu không được để trống.';   
  }else{
    if(!regex.hasMatch(value) && (value.length) >= 8){
      return 'Mật khẩu phải chứa chữ hoa, chữ thường, chữ số và ký tự đặc biệt.';
    }else if(value.length < 8){
      return 'Mật khẩu tối thiểu 8 kí tự';
    }else if(!regex.hasMatch(value) && (value.length) < 8){
      return 'Mật khẩu phải chứa chữ hoa, chữ thường, chữ số và ký tự đặc biệt, tối thiểu 8 kí tự';
    }
    else{
      return null;
    }
  }
}

String? validateTextField(String? value){
  RegExp regex = RegExp('[a-zA-Z0-9á-úÁ-Ú]');
  if(value!.isEmpty){
    return 'Không được để trống';
  }else{
    if(!regex.hasMatch(value)){
      return 'Vui lòng nhập đúng định dạng.';
    }
    if(value.length < 3){
      return 'Tối thiểu 3 kí tự';
    }else{
      return null;
    }
  }
}
