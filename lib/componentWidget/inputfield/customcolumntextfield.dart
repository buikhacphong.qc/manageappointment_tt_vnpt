import 'package:flutter/material.dart';

import '../styles/CustomColor.dart';

class textField extends StatefulWidget {
  final String title;
  final TextEditingController textController;
  final FocusNode focusNode;
  final Color? color;
  final bool? isNumberKeyboard;
  const textField({
    super.key, 
    required this.title, 
    required this.textController, 
    this.isNumberKeyboard = false,
    required this.focusNode, 
    this.color, 
  });
  @override
  State<textField> createState() => _textFieldState();
}

class _textFieldState extends State<textField> {
  @override
  Widget build(BuildContext context) {
    double width_new = MediaQuery.of(context).size.width;
    double height_new = MediaQuery.of(context).size.height;
    String entertext;
    return Column(
      children: [
        Expanded(
          flex: 4,
          child: Align(
            alignment: Alignment.topLeft,
            child: Text(
              widget.title,
              style: TextStyle(
                color: widget.color ?? Color(0xFF535858),
                fontSize: 14,
                fontWeight: FontWeight.w400,
                decoration: TextDecoration.none,
                decorationThickness: 0
              ),
              textAlign: TextAlign.left,
            ),
          )
        ),
        Expanded(
          flex: 6,
          child: Container( 
            width: width_new * 0.5,
            child: TextFormField(
              maxLines: 1,
              keyboardType: widget.isNumberKeyboard!
                  ? TextInputType.number
                  : TextInputType.text,
              cursorColor: primaryColor,
              decoration: new InputDecoration(
                filled: true,
                contentPadding: EdgeInsets.only(
                  left: 7, right: 0, top: 20
                ),
                fillColor: Colors.white,
                enabledBorder: const OutlineInputBorder(
                  borderSide:
                      const BorderSide(color: Colors.transparent, width: 1.0),
                ),
                border: new OutlineInputBorder(
                  borderRadius: new BorderRadius.circular(4.0),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: primaryColor
                  )
                ),
                suffixIcon: widget.textController.text.length > 0 ? IconButton(
                  onPressed: (){
                    setState(() {
                      widget.textController.clear();
                    });
                  },
                  icon: Icon(Icons.cancel_outlined, size: 18,color: primaryColor,)
                ) : Container(height: 0, width: 0,),
             
              ),
              onChanged: (text) {
                setState(() {
                  entertext = text.toLowerCase();
                  print(entertext);
                });
              },
              controller: widget.textController,   
              focusNode: widget.focusNode,  
                  
            ),
          ),
        ),
                
      ],
    );
  }
}