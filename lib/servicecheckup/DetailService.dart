import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:product_layout_app/common/BaseApiNoToken.dart';

import '../common/CustomAppBar.dart';
import '../common/CustomButton.dart';
import '../common/CustomTextValue.dart';
import '../model/dichVuKham.dart';
import 'AddEditService.dart';

// ignore: must_be_immutable
class DetailServiceCheckup extends StatefulWidget {
  DichVuKham dichvukham;
  DetailServiceCheckup({
    Key? key,
    required this.dichvukham,
  }) : super(key: key);

  @override
  State<DetailServiceCheckup> createState() => _DetailServiceCheckupState();
}

class _DetailServiceCheckupState extends State<DetailServiceCheckup> {
  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: CustomAppBar(
          text: 'Chi tiết dịch vụ khám',
          check: false,
          onPress: () {
            Navigator.pop(context, [widget.dichvukham, 'sua']);
          },
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 8,
          vertical: 8,
        ),
        child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: const Color.fromRGBO(255, 255, 255, 1),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    spreadRadius: 3,
                    blurRadius: 10,
                    offset: Offset(0, 3),
                  ),
                ]),
            width: MediaQuery.of(context).size.width,
            height: 270,
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 12,
                vertical: 12,
              ),
              child: Column(
                children: [
                  CustomTextValue(
                    key1: 'Mã dịch vụ',
                    value1: widget.dichvukham.maDichVu,
                    left: 2,
                    right: 2,
                  ),
                  CustomTextValue(
                    key1: 'Tên dịch vụ',
                    value1: widget.dichvukham.tenDichVu,
                    left: 2,
                    right: 2,
                  ),
                  CustomTextValue(
                    key1: 'Phí tư vấn online cũ',
                    value1: widget.dichvukham.phiTuVanCu,
                    left: 2,
                    right: 2,
                  ),
                  CustomTextValue(
                    key1: 'Phí tư vấn online mới',
                    value1: widget.dichvukham.phiTuVanMoi,
                    left: 2,
                    right: 2,
                  ),
                  CustomTextValue(
                    key1: 'Phí đặt lịch khám cũ',
                    value1: widget.dichvukham.phiDatLichCu,
                    left: 2,
                    right: 2,
                  ),
                  CustomTextValue(
                    key1: 'Phí đặt lịch khám mới',
                    value1: widget.dichvukham.phiDatLichMoi,
                    left: 2,
                    right: 2,
                  ),
                  CustomTextValue(
                    key1: 'Ngày a/d giá mới',
                    value1: widget.dichvukham.ngayAdGiaMoi,
                    left: 2,
                    right: 2,
                  ),
                  CustomTextValue(
                    key1: 'Trạng thái',
                    value1: widget.dichvukham.trangThai == true
                        ? 'Hiệu lực'
                        : 'Hết hiệu lực',
                    left: 2,
                    right: 2,
                  ),
                ],
              ),
            )),
      ),
      bottomNavigationBar: Container(
        width: MediaQuery.of(context).size.width,
        height: 50,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Color.fromARGB(255, 88, 87, 87).withOpacity(0.2),
              spreadRadius: 3,
              blurRadius: 10,
              offset: Offset(0, 3),
            ),
          ],
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                child: CustomButton(
                    width: width_screen / 2,
                    height: 30,
                    textButton: 'Xóa',
                    buttonIcon: SvgPicture.asset('assets/deny.svg'),
                    onPressed: () {
                      print(
                          'DetailService -> yêu cầu xóa dịch vụ khám có id = ${widget.dichvukham.id}');
                      Navigator.pop(context, [widget.dichvukham.id, 'xoa']);
                      deleteData();
                    }),
              ),
              Container(
                width: 5,
              ),
              Expanded(
                child: CustomButton(
                    width: width_screen / 2,
                    height: 30,
                    textButton: 'Sửa',
                    buttonIcon: SvgPicture.asset('assets/edit.svg'),
                    onPressed: () {
                      Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => AddServiceCheckup(
                                      check: false,
                                      dichvukham: widget.dichvukham)))
                          .then((value) {
                        if (value[1] == 'edit') {
                          loadData(value[0]);
                        }
                      });
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> deleteData() async {
    String apiUrl = 'https://64d2f44d67b2662bf3db88c8.mockapi.io/';
    String param = 'dichVuKham/${widget.dichvukham.id}';
    await BaseApiNoToken().delete(apiUrl, param);
  }

  void loadData(DichVuKham value) {
    setState(() {
      widget.dichvukham = value;
      print('DetailService -> dịch vụ khám sau khi sửa: ${widget.dichvukham}');
    });
  }
}
