import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:product_layout_app/common/BaseApiNoToken.dart';

import '../common/CustomButton.dart';
import '../item/ContainerSelectRoom.dart';
import '../item/ItemSelectRoom.dart';
import '../model/dichVuKham.dart';
import '../model/listPhongKham.dart';
import '../model/nhanVien.dart';
import '../popup/SelectPopup.dart';

// ignore: must_be_immutable
class SelectRoom extends StatefulWidget {
  final DichVuKham dichVuKham;
  final NhanVien nhanVien;
  final bool checkBool;
  SelectRoom({
    Key? key,
    required this.nhanVien,
    required this.checkBool,
    required this.dichVuKham,
  }) : super(key: key);

  @override
  State<SelectRoom> createState() => _SelectRoomState();
}

List<ListPhongKham> listPhongKham = [];

class _SelectRoomState extends State<SelectRoom> {
  bool checkLoading = false;
  List<dynamic> trave = [];
  @override
  void initState() {
    super.initState();
    loadData();
  }

  Future<void> loadData() async {
    String apiUrl = 'https://64d2f44d67b2662bf3db88c8.mockapi.io/';
    String param = 'listPhongKham';
    final result = await BaseApiNoToken().get(apiUrl, param);
    List<dynamic> kqua = listPhongKhamFromJson(result);
    setState(() {
      //quan ly dich vu kham
      if (widget.checkBool == true) {
        trave = kqua;
        for (int i = 0; i < trave.length; i++) {
          ListPhongKham listPhongKham = trave[i];
          for (int i = 0; i < widget.dichVuKham.listPhong.length; i++) {
            if (listPhongKham.id == widget.dichVuKham.listPhong[i].id) {
              listPhongKham.tuVanOnline =
                  widget.dichVuKham.listPhong[i].tuVanOnline;
              listPhongKham.datLichCsyt =
                  widget.dichVuKham.listPhong[i].datLichCsyt;
            }
          }
        }
      } else {
        //quan ly nhan vien
        trave = kqua;
        for (int i = 0; i < trave.length; i++) {
          ListPhongKham listPhongKham = trave[i];
          for (int i = 0; i < widget.nhanVien.listPhong.length; i++) {
            if (listPhongKham.id == widget.nhanVien.listPhong[i].id) {
              listPhongKham.tuVanOnline =
                  widget.nhanVien.listPhong[i].tuVanOnline;
              listPhongKham.datLichCsyt =
                  widget.nhanVien.listPhong[i].datLichCsyt;
            }
          }
        }
      }
    });
    checkLoading = true;
  }

  // kiểm tra trang thai checkall tư vấn + đặt lịch
  bool checkAllTuVan() {
    for (int i = 0; i < trave.length; i++) {
      ListPhongKham listPhongKham = trave[i];
      if (listPhongKham.tuVanOnline == false) {
        return false;
      }
    }
    return true;
  }

  bool checkAllDatLich() {
    for (int i = 0; i < trave.length; i++) {
      ListPhongKham listPhongKham = trave[i];
      if (listPhongKham.datLichCsyt == false) {
        return false;
      }
    }
    return true;
  }

  // update all gia tri tu van + dat lich
  void updateTuVan() {
    setState(() {
      for (int i = 0; i < trave.length; i++) {
        ListPhongKham listPhongKham = trave[i];
        listPhongKham.tuVanOnline = true;
      }
      checkAllTuVan();
    });
  }

  void updateDatLich() {
    setState(() {
      for (int i = 0; i < trave.length; i++) {
        ListPhongKham listPhongKham = trave[i];
        listPhongKham.datLichCsyt = true;
      }
      checkAllDatLich();
    });
  }

  // remove all gia tri tu van + dat lich
  void removeTuVan() {
    setState(() {
      for (int i = 0; i < trave.length; i++) {
        ListPhongKham listPhongKham = trave[i];
        listPhongKham.tuVanOnline = false;
      }
      checkAllTuVan();
    });
  }

  void removeDatLich() {
    setState(() {
      for (int i = 0; i < trave.length; i++) {
        ListPhongKham listPhongKham = trave[i];
        listPhongKham.datLichCsyt = false;
      }
      checkAllDatLich();
    });
  }

//update tung item
  void updateItemTuVan(ListPhongKham listPhongKham, bool value) {
    setState(() {
      listPhongKham.tuVanOnline = !value;
    });
  }

  void updateItemDatLich(ListPhongKham listPhongKham, bool value) {
    setState(() {
      listPhongKham.datLichCsyt = !value;
    });
  }

  //lay kqua tu container ve
  Map<String, String> resultTimKiem = {};
  void receiveData(Map<String, String> ketqua) {
    setState(() {
      resultTimKiem = ketqua;
      print('SelectRoom -> Giá trị text đã nhập: ${resultTimKiem} ');
    });
  }

  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('Danh sách phòng khám'),
          // centerTitle: true,
          actions: <Widget>[
            Builder(
              builder: (context) {
                return IconButton(
                  onPressed: () async {
                    String? ketqua = await showDialog(
                        context: context,
                        builder: (context) {
                          return SelectPopup(
                              result: [checkAllTuVan(), checkAllDatLich()]);
                        });
                    print(ketqua);
                    if (ketqua == 'allTuVan') {
                      updateTuVan();
                    } else if (ketqua == 'allDatLich') {
                      updateDatLich();
                    } else if (ketqua == 'removeTuVan') {
                      removeTuVan();
                    } else if (ketqua == 'removeDatLich') {
                      removeDatLich();
                    }
                  },
                  icon: Icon(
                    Icons.more_vert,
                    // size: 14,
                  ),
                );
              },
            )
          ],
          backgroundColor: Color(0xFF6F9BD4),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(
                  context,
                  widget.checkBool == true
                      ? [widget.dichVuKham, 'back']
                      : [widget.nhanVien, 'back']);
            },
            icon: Icon(
              Icons.arrow_back_ios,
              size: 14,
            ),
          ),
        ),
        body: Scaffold(
          body: Container(
            width: width_screen,
            height: MediaQuery.of(context).size.height,
            child: Padding(
              padding: EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 0),
              child: ListView.builder(
                  itemCount: 2,
                  itemBuilder: (BuildContext context, index) {
                    if (index == 0) {
                      return Card(
                          elevation: 4,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8),
                              side: BorderSide(
                                color: Color.fromARGB(255, 178, 200, 219),
                              )),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              color: Color.fromARGB(255, 178, 200, 219),
                            ),
                            width: MediaQuery.of(context).size.width,
                            height: 180,
                            child: ContainerSelectRoom(
                              dataSend: receiveData,
                            ),
                          ));
                    } else {
                      return Container(
                        margin: EdgeInsets.only(top: 8),
                        // color: Color.fromARGB(255, 178, 200, 219),
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          children: [
                            Visibility(
                              child: CircularProgressIndicator(),
                              visible: !checkLoading,
                            ),
                            Visibility(
                              visible: checkLoading,
                              child: ListView.separated(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: trave.length,
                                itemBuilder: (BuildContext context, int index) {
                                  ListPhongKham listPhongKham = trave[index];
                                  return ItemSelectRoom(
                                    tenPhong: listPhongKham.tenPhong,
                                    maPhong: listPhongKham.maPhong,
                                    ktraTuVan: listPhongKham.tuVanOnline,
                                    ktraDatLich: listPhongKham.datLichCsyt,
                                    onClickTuVan: () {
                                      updateItemTuVan(listPhongKham,
                                          listPhongKham.tuVanOnline);
                                    },
                                    onClickDatLich: () {
                                      updateItemDatLich(listPhongKham,
                                          listPhongKham.datLichCsyt);
                                    },
                                  );
                                  // }
                                },
                                separatorBuilder:
                                    (BuildContext context, int index) {
                                  return Divider(
                                    height: 8,
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                      );
                    }
                  }),
            ),
          ),
          bottomNavigationBar: Container(
            width: MediaQuery.of(context).size.width,
            height: 50,
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Color.fromARGB(255, 88, 87, 87).withOpacity(0.2),
                  spreadRadius: 3,
                  blurRadius: 10,
                  offset: Offset(0, 3),
                ),
              ],
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 5),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: CustomButton(
                        width: width_screen,
                        height: 30,
                        textButton: 'Cập nhật',
                        buttonIcon: SvgPicture.asset('assets/save.svg'),
                        onPressed: () {
                          if (widget.checkBool == true) {
                            for (int i = 0; i < trave.length; i++) {
                              ListPhongKham listPhongKham = trave[i];
                              if (listPhongKham.tuVanOnline == true &&
                                      listPhongKham.checkDangKiHen == true ||
                                  listPhongKham.tuVanOnline == false &&
                                      listPhongKham.checkDangKiHen == true ||
                                  listPhongKham.checkDangKiHen == true &&
                                      listPhongKham.tuVanOnline == true ||
                                  listPhongKham.checkDangKiHen == false &&
                                      listPhongKham.tuVanOnline == false) {
                                widget.dichVuKham.listPhong.add(listPhongKham);
                              }
                            }
                            print(
                                'SelectRoom -> Dịch vụ khám đã chọn phòng: ${widget.dichVuKham}');
                            Navigator.pop(context,
                                [widget.dichVuKham, 'update_dichVuKham']);
                          } else {
                            //quan ly nhan vien
                            for (int i = 0; i < trave.length; i++) {
                              ListPhongKham listPhongKham = trave[i];
                              if (listPhongKham.tuVanOnline == true &&
                                      listPhongKham.checkDangKiHen == true ||
                                  listPhongKham.tuVanOnline == false &&
                                      listPhongKham.checkDangKiHen == true ||
                                  listPhongKham.checkDangKiHen == true &&
                                      listPhongKham.tuVanOnline == true ||
                                  listPhongKham.checkDangKiHen == false &&
                                      listPhongKham.tuVanOnline == false) {
                                widget.nhanVien.listPhong.add(listPhongKham);
                              }
                            }
                            print(
                                'SelectRoom -> Nhân viên đã chọn phòng: ${widget.nhanVien}');
                            Navigator.pop(
                                context, [widget.nhanVien, 'update_nhanVien']);
                          }
                        }),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
