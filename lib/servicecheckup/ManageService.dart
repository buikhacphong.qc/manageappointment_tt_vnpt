import 'package:flutter/material.dart';
import 'package:product_layout_app/common/BaseApiNoToken.dart';

import '../common/CustomAppBar.dart';
import '../item/ContainerServiceCheckup.dart';
import '../item/ItemServiceCheckup.dart';
import '../model/dichVuKham.dart';
import 'DetailService.dart';

// ignore: must_be_immutable
class ManageService extends StatefulWidget {
  bool checkLoading;
  ManageService({
    Key? key,
    required this.checkLoading,
  }) : super(key: key);

  @override
  State<ManageService> createState() => _ManageServiceState();
}

class _ManageServiceState extends State<ManageService> {
  Map<String, String> resultTimKiem = {};
  List<dynamic> trave = [];
  // nhận data tìm kiếm từ container
  void receiveData(Map<String, String> ketqua) {
    setState(() {
      resultTimKiem = ketqua;
      print('ManageService -> Giá trị text đã nhập: $resultTimKiem');
    });
  }

  //nhận data thêm mới
  void receiveDataAdd(DichVuKham dichVuKham) {
    setState(() {
      print('MangageService -> Dịch vụ khám được nhận thêm: ${dichVuKham}');
      trave.add(dichVuKham);
      widget.checkLoading = true;
    });
  }

  @override
  void initState() {
    super.initState();
    widget.checkLoading = false;
    loadData();
  }

  Future<void> loadData() async {
    String apiUrl = 'https://64d2f44d67b2662bf3db88c8.mockapi.io/';
    String param = 'dichVuKham';
    final result = await BaseApiNoToken().get(apiUrl, param);
    List<dynamic> kqua = dichVuKhamFromJson(result);
    if (kqua.length != 0) {
      setState(() {
        trave = kqua;
        widget.checkLoading = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: 'Danh sách dịch vụ khám',
            check: false,
            onPress: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Container(
          width: width_screen,
          height: MediaQuery.of(context).size.height,
          child: Padding(
            padding: EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 0),
            child: ListView.builder(
                itemCount: 2,
                itemBuilder: (BuildContext context, index) {
                  if (index == 0) {
                    return Card(
                      elevation: 4,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8),
                          side: BorderSide(
                            color: Color.fromARGB(255, 178, 200, 219),
                          )),
                      child: Container(
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          color: Color.fromARGB(255, 178, 200, 219),
                        ),
                        width: MediaQuery.of(context).size.width,
                        height: 230,
                        child: ContainerServiceCheckup(
                          onData: receiveData,
                          senData: receiveDataAdd,
                        ),
                      ),
                    );
                  } else {
                    return Container(
                      padding:
                          EdgeInsets.only(top: 8, left: 0, right: 0, bottom: 0),
                      child: Column(
                        children: [
                          Visibility(
                            child: CircularProgressIndicator(),
                            visible: !widget.checkLoading,
                          ),
                          Visibility(
                            visible: widget.checkLoading,
                            child: ListView.separated(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: trave.length,
                              itemBuilder: (BuildContext context, int index) {
                                DichVuKham dichVuKham = trave[index];
                                return GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      DetailServiceCheckup(
                                                          dichvukham:
                                                              dichVuKham)))
                                          .then((value) {
                                        print(
                                            'ManageService -> Mảng giá trị nhận được từ DetailService: $value');
                                        if (value[1] == 'xoa') {
                                          for (int i = 0;
                                              i < trave.length;
                                              i++) {
                                            DichVuKham dichvu = trave[i];
                                            if (dichvu.id == value[0]) {
                                              setState(() {
                                                trave.removeAt(i);
                                                print(
                                                    'ManageService xóa dịch vụ khám id = ${value[0]} thành công');
                                              });
                                            }
                                          }
                                        } else if (value[1] == 'sua') {
                                          DichVuKham dichVuTraVe = value[0];
                                          for (int i = 0;
                                              i < trave.length;
                                              i++) {
                                            DichVuKham dichVu = trave[i];
                                            if (dichVu.id == dichVuTraVe.id) {
                                              setState(() {
                                                dichVu = dichVuTraVe;
                                              });
                                            }
                                          }
                                        }
                                      });
                                    },
                                    child: ItemServiceCheckup(
                                      trangThai: dichVuKham.trangThai,
                                      tenDichVu: dichVuKham.tenDichVu,
                                      maDichVu: dichVuKham.maDichVu,
                                      giaTuVan: dichVuKham.phiTuVanCu,
                                      giaDatLich: dichVuKham.phiDatLichCu,
                                    ));
                                // }
                              },
                              separatorBuilder:
                                  (BuildContext context, int index) {
                                return Divider(
                                  height: 8,
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                }),
          ),
        ),
      ),
    );
  }
}
