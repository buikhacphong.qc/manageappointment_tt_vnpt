import 'dart:convert';

import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:product_layout_app/common/BaseApiNoToken.dart';

import '../common/CustomAppBar.dart';
import '../common/CustomButton.dart';
import '../common/CustomButtonCheckbox.dart';
import '../common/CustomeTextField.dart';
import '../model/dichVuKham.dart';
import '../model/nhanVien.dart';
import 'SelectRoom.dart';

// ignore: must_be_immutable
class AddServiceCheckup extends StatefulWidget {
  DichVuKham dichvukham;
  final bool check;
  AddServiceCheckup({Key? key, required this.check, required this.dichvukham})
      : super(key: key);

  @override
  State<AddServiceCheckup> createState() => _AddServiceCheckupState();
}

class _AddServiceCheckupState extends State<AddServiceCheckup> {
  //Map luu ket qua
  final FocusNode _focusMaDV = FocusNode();
  final FocusNode _focusTenDV = FocusNode();
  final FocusNode _focusTuVanCu = FocusNode();
  final FocusNode _focusTuVanMoi = FocusNode();
  final FocusNode _focusDatLichCu = FocusNode();
  final FocusNode _focusDatLichMoi = FocusNode();
  final FocusNode _focusNgay = FocusNode();

  final TextEditingController textTen = TextEditingController();
  final TextEditingController textMa = TextEditingController();
  final TextEditingController textTuVanCu = TextEditingController();
  final TextEditingController textTuVanMoi = TextEditingController();
  final TextEditingController textDatLichCu = TextEditingController();
  final TextEditingController textDatLichMoi = TextEditingController();
  final TextEditingController textDatNgay = TextEditingController();

  bool ktraHieuLuc = false;
  bool ktraHetHieuLuc = false;
  @override
  void initState() {
    super.initState();
    //set gia tri ban dau khi sua
    if (widget.check == false) {
      textTen.text = widget.dichvukham.tenDichVu;
      textMa.text = widget.dichvukham.maDichVu;
      textTuVanCu.text = widget.dichvukham.phiDatLichCu;
      textTuVanMoi.text = widget.dichvukham.phiTuVanMoi;
      textDatLichCu.text = widget.dichvukham.phiDatLichCu;
      textDatLichMoi.text = widget.dichvukham.phiDatLichMoi;
      textDatNgay.text = widget.dichvukham.ngayAdGiaMoi;
      if (widget.dichvukham.trangThai == true) {
        ktraHieuLuc = true;
      } else {
        ktraHetHieuLuc = true;
      }
    }
    _focusTenDV.addListener(_onFocusChange);
    _focusMaDV.addListener(_onFocusChange);
    _focusTuVanCu.addListener(_onFocusChange);
    _focusTuVanMoi.addListener(_onFocusChange);

    _focusDatLichCu.addListener(_onFocusChange);
    _focusDatLichMoi.addListener(_onFocusChange);
    _focusNgay.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    _focusTenDV.dispose();
    _focusMaDV.dispose();
    _focusTuVanCu.dispose();
    _focusTuVanMoi.dispose();

    _focusDatLichCu.dispose();
    _focusDatLichMoi.dispose();
    _focusNgay.dispose();

    super.dispose();
  }

  void _onFocusChange() {
    setState(() {
      widget.dichvukham.tenDichVu = textTen.text;
      widget.dichvukham.maDichVu = textMa.text;
      widget.dichvukham.phiTuVanCu = textTuVanCu.text;
      widget.dichvukham.phiTuVanMoi = textTuVanMoi.text;
      widget.dichvukham.phiDatLichCu = textDatLichCu.text;
      widget.dichvukham.phiDatLichMoi = textDatLichMoi.text;
      widget.dichvukham.ngayAdGiaMoi = textDatNgay.text;
    });
  }

  DateTime dateTime = DateTime.now();
  void updateHieuLuc() {
    setState(() {
      ktraHieuLuc = !ktraHieuLuc;
      //chi duoc chon 1
      if (ktraHetHieuLuc == ktraHieuLuc) {
        ktraHetHieuLuc = !ktraHetHieuLuc;
      }
      if (ktraHieuLuc == true) {
        widget.dichvukham.trangThai = true;
      } else {
        widget.dichvukham.trangThai = false;
      }
    });
  }

  void updateHetHieuLuc() {
    setState(() {
      ktraHetHieuLuc = !ktraHetHieuLuc;
      if (ktraHieuLuc == ktraHetHieuLuc) {
        ktraHieuLuc = !ktraHieuLuc;
      }
      if (ktraHetHieuLuc == true) {
        widget.dichvukham.trangThai = false;
      } else {
        widget.dichvukham.trangThai = true;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: widget.check == true
                  ? 'Thêm mới dịch vụ khám'
                  : 'Sửa dịch vụ khám',
              check: false,
              onPress: () {
                Navigator.pop(context, [widget.dichvukham, 'back']);
              },
            ),
          ),
          body: Scaffold(
            body: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 8,
                  vertical: 8,
                ),
                child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        color: const Color.fromRGBO(255, 255, 255, 1),
                        // color: Colors.blue,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.2),
                            spreadRadius: 3,
                            blurRadius: 10,
                            offset: Offset(0, 3),
                          ),
                        ]),
                    width: MediaQuery.of(context).size.width,
                    height: 300,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 4,
                        vertical: 4,
                      ),
                      child: Column(
                        children: [
                          CustomTextField(
                            keyText: 'Mã dịch vụ',
                            right: 2,
                            left: 2,
                            checkRangBuoc: true,
                            focusNode: _focusMaDV,
                            textEditingController: textMa,
                            onpress: () {
                              textMa.clear();
                            },
                          ),
                          CustomTextField(
                            keyText: 'Tên dịch vụ',
                            right: 2,
                            left: 2,
                            checkRangBuoc: true,
                            focusNode: _focusTenDV,
                            textEditingController: textTen,
                            onpress: () {
                              textTen.clear();
                            },
                          ),
                          CustomTextField(
                            keyText: 'Phí tư vấn online cũ',
                            right: 2,
                            left: 2,
                            checkRangBuoc: true,
                            focusNode: _focusTuVanCu,
                            textEditingController: textTuVanCu,
                            onpress: () {
                              textTuVanCu.clear();
                            },
                          ),
                          CustomTextField(
                            keyText: 'Phí tư vấn online mới',
                            right: 2,
                            left: 2,
                            checkRangBuoc: true,
                            focusNode: _focusTuVanMoi,
                            textEditingController: textTuVanMoi,
                            onpress: () {
                              textTuVanMoi.clear();
                            },
                          ),
                          CustomTextField(
                            keyText: 'Phí đặt lịch khám cũ',
                            right: 2,
                            left: 2,
                            checkRangBuoc: true,
                            focusNode: _focusDatLichCu,
                            textEditingController: textDatLichCu,
                            onpress: () {
                              textDatLichCu.clear();
                            },
                          ),
                          CustomTextField(
                            keyText: 'Phí đặt lịch khám mới',
                            right: 2,
                            left: 2,
                            checkRangBuoc: true,
                            focusNode: _focusDatLichMoi,
                            textEditingController: textDatLichMoi,
                            onpress: () {
                              textDatLichMoi.clear();
                            },
                          ),
                          CustomTextField(
                            keyText: 'Ngày a/d giá mới',
                            right: 2,
                            left: 2,
                            icon: Icon(
                              Icons.calendar_month,
                              size: 14,
                            ),
                            checkRangBuoc: true,
                            focusNode: _focusNgay,
                            textEditingController: textDatNgay,
                            onpress: () {
                              _show();
                            },
                          ),
                          Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Expanded(
                                  child: CustomButtonCheckbox(
                                      width: width_screen / 2,
                                      height: 30,
                                      textButton: 'Hiệu lực',
                                      checkIcon: ktraHieuLuc,
                                      onPressed: () {
                                        updateHieuLuc();
                                      }),
                                ),
                                Container(
                                  width: 5,
                                ),
                                Expanded(
                                  child: CustomButtonCheckbox(
                                      width: width_screen / 2,
                                      height: 30,
                                      textButton: 'Hết hiệu lực',
                                      checkIcon: ktraHetHieuLuc,
                                      onPressed: () {
                                        updateHetHieuLuc();
                                      }),
                                ),
                              ])
                        ],
                      ),
                    )),
              ),
            ),
            bottomNavigationBar: Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(255, 88, 87, 87).withOpacity(0.2),
                    spreadRadius: 3,
                    blurRadius: 10,
                    offset: Offset(0, 3),
                  ),
                ],
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: CustomButton(
                          width: width_screen / 2,
                          height: 30,
                          textButton: 'Lưu',
                          buttonIcon: SvgPicture.asset('assets/save.svg'),
                          onPressed: () {
                            if (widget.check == true) {
                              postData();
                              print(
                                  'AddService -> Dịch vụ khám được thêm mới là: ${widget.dichvukham} ');
                              Navigator.pop(
                                  context, [widget.dichvukham, 'add']);
                            } else {
                              putData();
                              print(
                                  'EditService -> Dịch vụ khám được sửa -> Giá trị mới : ${widget.dichvukham} ');
                              Navigator.pop(
                                  context, [widget.dichvukham, 'edit']);
                            }
                          }),
                    ),
                    Container(
                      width: 5,
                    ),
                    Expanded(
                      child: CustomButton(
                          width: 0,
                          height: 30,
                          textButton: 'Chọn phòng',
                          buttonIcon: SvgPicture.asset(
                            'assets/room.svg',
                            width: 14,
                            height: 14,
                            color: Colors.green,
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SelectRoom(
                                      dichVuKham: widget.dichvukham,
                                      nhanVien: NhanVien(
                                          maNv: '',
                                          tenNv: '',
                                          maBs: '',
                                          loaiNv: '',
                                          khoa: '',
                                          phong: '',
                                          email: '',
                                          chucDanh: '',
                                          trinhDo: '',
                                          tenPhong: '',
                                          hocHam: '',
                                          hocVi: '',
                                          sdt: '',
                                          diaChi: '',
                                          ngaySinh: '',
                                          gioiTinh: '',
                                          soBhyt: '',
                                          soBhxh: '',
                                          id: '',
                                          listPhong: []),
                                      checkBool: true)),
                            ).then((value) {
                              if (value[1] == 'update_dichVuKham') {
                                print(
                                    'AddEditService -> Dịch vụ khám sau khi thêm phòng: ${widget.dichvukham}');
                                setState(() {
                                  widget.dichvukham = value[0];
                                });
                              }
                            });
                          }),
                    ),
                  ],
                ),
              ),
            ),
          )),
    );
  }

  void _show() async {
    final DateTime? result = await showDatePicker(
        context: context,
        initialDate: dateTime,
        firstDate: DateTime(2019),
        lastDate: DateTime(2024),
        locale: const Locale('vi'),
        );
    if (result != null) {
      setState(() {
        dateTime = result;
        textDatNgay.text = '${formatDate(dateTime, [dd, '/', mm, '/', yyyy])}';
        dateTime.toString();
        print(dateTime);
      });
    }
  }

  Future<void> postData() async {
    String apiUrl = 'https://64d2f44d67b2662bf3db88c8.mockapi.io/';
    String param = 'dichVuKham/';
    var test1 = jsonEncode(widget.dichvukham.toJson());
    await BaseApiNoToken().post(apiUrl, param, test1);
  }

  Future<void> putData() async {
    String apiUrl = 'https://64d2f44d67b2662bf3db88c8.mockapi.io/';
    String param = 'dichVuKham/${widget.dichvukham.id}';
    var test1 = jsonEncode(widget.dichvukham.toJson());
    await BaseApiNoToken().put(apiUrl, param, test1);
  }
}
