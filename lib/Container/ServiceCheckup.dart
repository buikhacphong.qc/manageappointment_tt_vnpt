import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:product_layout_app/Container/ItemServiceCheckup.dart';

import '../common/CustomButton.dart';

class ServiceCheckup extends StatefulWidget {
  const ServiceCheckup({super.key});

  @override
  State<ServiceCheckup> createState() => _ServiceCheckupState();
}

class _ServiceCheckupState extends State<ServiceCheckup> {
  List<List<String>> list = [
    ['Hiệu lực', 'Dich vu 1', 'Mã dịch vụ 1', '2000', '2000'],
    ['Hết hiệu lực', 'Dich vu 2', 'Mã dịch vụ 2', '2000', '2000'],
    ['Hiệu lực', 'Dich vu 3', 'Mã dịch vụ 3', '2000', '2000'],
    ['Hết hiệu lực', 'Dich vu 4', 'Mã dịch vụ 4', '2000', '2000'],
  ];
  // ScrollController _scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          title: const Text('Danh sách dịch vụ khám'),
          backgroundColor: Color(0xFF6F9BD4),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              child: Container(
                  margin: EdgeInsets.only(bottom: 5),
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    color: Color.fromARGB(255, 178, 200, 219),
                  ),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height - 145,
                  child: Container(
                      width: width_screen,
                      child: ListView.separated(
                        shrinkWrap: true,
                        itemCount: list.length,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                              onTap: () {
                                print('ok');
                              },
                              child: ItemServiceCheckup(
                                trangThai: list[index][0],
                                tenDichVu: list[index][1],
                                maDichVu: list[index][2],
                                giaTuVan: list[index][3],
                                giaDatLich: list[index][4],
                              ));
                          // }
                        },
                        separatorBuilder: (BuildContext context, int index) {
                          return Divider(
                            height: 10,
                          );
                        },
                      ))),
            ),
            Container(
              width: width_screen,
              height: 45,
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    spreadRadius: 3,
                    blurRadius: 10,
                    offset: Offset(0, 3),
                  ),
                ],
              ),
              margin: EdgeInsets.only(bottom: 7),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CustomButton(
                      width: 340,
                      height: 35,
                      textButton: 'Thêm',
                      buttonIcon: SvgPicture.asset('assets/add.svg'),
                      onPressed: () {}),
                ],
              ),
            )
          ],
        ));
  }
}
