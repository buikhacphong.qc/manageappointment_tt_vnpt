import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../Popup/CallPopup.dart';
import '../Popup/DenyPopup.dart';
import '../common/BaseApi.dart';
import '../common/CustomButton.dart';
import '../edit_appointment/edit_schedule.dart';
import '../model/ChiTietLichHen.dart';
import 'ItemDetailAppoint.dart';

class DetailManageCaculator extends StatefulWidget {
  final String idHenKham;
  const DetailManageCaculator({
    super.key,
    required this.idHenKham,
  });

  @override
  State<DetailManageCaculator> createState() => _DetailManageCaculatorState();
}

class _DetailManageCaculatorState extends State<DetailManageCaculator> {
  //bubbles icon call
  final GlobalKey _floatingKey = GlobalKey(); //biến tham chiếu đến widget call
  late Size floatingSize; //biến lưu kích thước widget call
  Offset floatingLocation = Offset(300, 400); // biến lưu vị trí x,y
  // lấy kích thước của widget được xác định bằng _floatingKey và gán kích thước đó cho biến floatingSize
  void getFloatingSize() {
    RenderBox _floatingBox =
        _floatingKey.currentContext!.findRenderObject() as RenderBox;
    floatingSize = _floatingBox.size;
  }
//hàm cập nhật vị trí widget call trong quá trình kéo
  void onDrapUpdate(BuildContext context, DragUpdateDetails details) {
    //vi tri cu chi
    final RenderBox box = context.findRenderObject()! as RenderBox;
    final Offset offset = box.globalToLocal(details.globalPosition);

    //khu vuc man hinh
    const double startX = 0;
    final double endX = MediaQuery.of(context).size.width - floatingSize.width;
    // print(endX);
    final double startY = MediaQuery.of(context).padding.top;
    // final double endY = context.size?.height ?? -floatingSize.height;
    final double endY =
        MediaQuery.of(context).size.height - 115 - floatingSize.height;
    // print(endY);
    //kiem tra nam trong kich thuoc phu hop k
    if (startX < offset.dx && offset.dx < endX) {
      if (startY < offset.dy && offset.dy < endY) {
        setState(() {
          floatingLocation = Offset(offset.dx, offset.dy);
        });
      }
    }

    //update vi tri
  }

  Map<String, String> ketqua = {};
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      getFloatingSize();
    });
    initApi();
    // fetchTest();
  }

  void initApi() async {
    String apiUrl =
        'https://staging-apidatlichkham.vncare.vn/patient_api/api/QlhkMobile/v1.3.0/business-service/';
    String param = 'chiTietLichHenKham?henKhamId=${widget.idHenKham}';
    final result = await BaseApi().get(apiUrl, param);
    final kqua = chiTietLichHenFromJson(result);
    setState(() {
      ketqua = kqua.result;
    });
  }

  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    final height_screen = MediaQuery.of(context).size.height;
    // print(widget.idHenKham);
    return Scaffold(
        appBar: AppBar(
          title: Text('Chi tiết lịch hẹn khám CSYT'),
          backgroundColor: Color(0xFF6F9BD4),
        ),
        body: GestureDetector(
          onTap: () {
            FocusManager.instance.primaryFocus?.unfocus();
          },
          child: Scaffold(
            body: GestureDetector(
              onVerticalDragUpdate: (DragUpdateDetails details) {
                onDrapUpdate(context, details);
              },
              onHorizontalDragUpdate: (DragUpdateDetails details) {
                onDrapUpdate(context, details);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Stack(children: [
                    Container(
                      width: width_screen,
                      height: height_screen - 136,
                      child: SingleChildScrollView(
                        child: Padding(
                            padding: EdgeInsets.all(10),
                            child: Container(
                              child: ItemDetailAppoint(
                                valueSate:
                                    ketqua['trangThaiLichHen'].toString(),
                                valueStateAppoint:
                                    ketqua['trangThaiDenKham'].toString(),
                                valueRegistry: ketqua['kenhDangKy'].toString(),
                                valueObject:
                                    ketqua['doiTuongBenhNhan'].toString(),
                                valueBHYT: ketqua['soTheBhyt'].toString(),
                                valueName: ketqua['hoTen'].toString(),
                                valueBirthday: ketqua['ngaySinh'].toString(),
                                valueJob: ketqua['ngheNghiep'].toString(),
                                valueSex: ketqua['gioiTinh'].toString(),
                                valuePeople: ketqua['danToc'].toString(),
                                valueCountry: ketqua['quocTich'].toString(),
                                valueProvince:
                                    ketqua['tinhKhaiSinh'].toString(),
                                valuePhone:
                                    ketqua['trangThaiLichHen'].toString(),
                                valueIdCart: ketqua['sdt'].toString(),
                                valueCountryLiving: ketqua['tinhTp'].toString(),
                                valueDistrict: ketqua['huyen'].toString(),
                                valueCommune: ketqua['xa'].toString(),
                                valueHouse: ketqua['soNha'].toString(),
                                valueRequest: ketqua['yeuCauKham'].toString(),
                                valueRoom: ketqua['phongKham'].toString(),
                                valueDoctor: ketqua['bacSi'].toString(),
                                valueDate: ketqua['thoiGianDenKham'].toString(),
                                valueContent: ketqua['yeuCauKham'].toString(),
                                valueNote: ketqua['ghiChu'].toString(),
                                trangThaiDenKham:
                                    ketqua['maTrangThaiDenKham'].toString(),
                              ),
                            )),
                      ),
                    ),
                    Builder(
                      builder: (context) {
                        return Positioned(
                          child: GestureDetector(
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return CallPopup();
                                  });
                            },
                            child: SvgPicture.asset(
                              'assets/call_bubbles.svg',
                              key: _floatingKey,
                            ),
                          ),
                          //set vị trí ban đầu
                          left: floatingLocation.dx,
                          top: floatingLocation.dy,
                        );
                      },
                    ),
                  ]),
                ],
              ),
            ),
            bottomNavigationBar: Container(
              width: width_screen,
              height: 40,
              margin: EdgeInsets.only(bottom: 7),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: 5,
                  ),
                  Expanded(
                    flex: 3,
                    child: Builder(
                      builder: (context) {
                        return CustomButton(
                            width: width_screen / 3,
                            height: 30,
                            textButton: 'Từ chối',
                            buttonIcon: SvgPicture.asset('assets/deny.svg'),
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return DenyPopup();
                                  });
                            });
                      },
                    ),
                  ),
                  Container(
                    width: 5,
                  ),
                  Expanded(
                    flex: 3,
                    child: CustomButton(
                        width: width_screen / 3,
                        height: 30,
                        textButton: 'Sửa',
                        buttonIcon: SvgPicture.asset('assets/edit.svg'),
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => EditSchedule()));
                        }),
                  ),
                  Container(
                    width: 5,
                  ),
                  Expanded(
                    flex: 4,
                    child: CustomButton(
                        width: width_screen / 3,
                        height: 30,
                        textButton: 'Xác nhận lịch',
                        buttonIcon: SvgPicture.asset('assets/confirm.svg'),
                        onPressed: () {
                          // Navigator.push(
                          //   context,
                          //   MaterialPageRoute(
                          //       builder: (context) => DetailManageCaculator()),
                          // );
                        }),
                  ),
                  Container(
                    width: 5,
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}
