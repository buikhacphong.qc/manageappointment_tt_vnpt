import 'package:flutter/material.dart';
import 'package:product_layout_app/common/CustomTitleDetail.dart';
// import 'package:flutter_svg/flutter_svg.dart';

class ItemDetailAppoint extends StatelessWidget {
  final String valueSate;
  final String valueStateAppoint;
  final String valueRegistry;
  final String valueObject;
  final String valueBHYT;
  final String valueName;
  final String valueBirthday;
  final String valueJob;
  final String valueSex;
  final String valuePeople;
  final String valueCountry;
  final String valueProvince;
  final String valuePhone;
  final String valueIdCart;
  final String valueCountryLiving;
  final String valueDistrict;
  final String valueCommune;
  final String valueHouse;
  final String valueRequest;
  final String valueRoom;
  final String valueDoctor;
  final String valueDate;
  final String valueContent;
  final String valueNote;
  final String trangThaiDenKham;

  const ItemDetailAppoint(
      {super.key,
      required this.valueSate,
      required this.valueStateAppoint,
      required this.valueRegistry,
      required this.valueObject,
      required this.valueBHYT,
      required this.valueName,
      required this.valueBirthday,
      required this.valueJob,
      required this.valueSex,
      required this.valuePeople,
      required this.valueCountry,
      required this.valueProvince,
      required this.valuePhone,
      required this.valueIdCart,
      required this.valueCountryLiving,
      required this.valueDistrict,
      required this.valueCommune,
      required this.valueHouse,
      required this.valueRequest,
      required this.valueRoom,
      required this.valueDoctor,
      required this.valueDate,
      required this.valueContent,
      required this.valueNote,
      required this.trangThaiDenKham});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Wrap(children: [
          Container(
            margin: EdgeInsets.only(top: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: const Color.fromRGBO(255, 255, 255, 1),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    spreadRadius: 3,
                    blurRadius: 10,
                    offset: Offset(0, 3),
                  ),
                ]),
            child: Column(
              children: [
                Wrap(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 8, top: 8, bottom: 8, right: 0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          CustomTitleDetail(
                            keyItem: 'Trạng thái',
                            valueItem: valueSate,
                          ),
                          CustomTitleDetail(
                            keyItem: 'Trạng thái đến khám',
                            valueItem: valueStateAppoint,
                            trangThaiDenKham: trangThaiDenKham,
                          ),
                          SizedBox(height: 5,),
                          CustomTitleDetail(
                            keyItem: 'Kênh đăng ký (*)',
                            valueItem: valueRegistry,
                          ),
                          CustomTitleDetail(
                            keyItem: 'Đối tượng (*)',
                            valueItem: valueObject,
                          ),
                          CustomTitleDetail(
                            keyItem: 'Số thẻ BHYT (*)',
                            valueItem: valueBHYT,
                          ),
                          CustomTitleDetail(
                            keyItem: '',
                            valueItem: 'Xác thực thẻ bhyt',
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ]),
        SizedBox(
          width: 5,
        ),
        Container(
            margin: EdgeInsets.only(top: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: const Color.fromRGBO(255, 255, 255, 1),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    spreadRadius: 3,
                    blurRadius: 10,
                    offset: Offset(0, 3),
                  ),
                ]),
            child: Column(children: [
              Wrap(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 8, top: 8, bottom: 8, right: 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        CustomTitleDetail(
                          keyItem: 'Họ và tên (*)',
                          valueItem: valueName,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Ngày sinh (*)',
                          valueItem: valueBirthday,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Nghề nghiệp (*)',
                          valueItem: valueJob,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Giới tính (*)',
                          valueItem: valueSex,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Dân tộc (*)',
                          valueItem: valuePeople,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Quốc tịch (*)',
                          valueItem: valueCountry,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Tỉnh khai sinh',
                          valueItem: valueProvince,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Số điện thoại (*)',
                          valueItem: valuePhone,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Chứng minh thư',
                          valueItem: valueIdCart,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Tỉnh/Thành phố (*)',
                          valueItem: valueCountryLiving,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Huyện (*)',
                          valueItem: valueDistrict,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Xã (*)',
                          valueItem: valueCommune,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Số nhà (*)',
                          valueItem: valueHouse,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ])),
        Container(
            margin: EdgeInsets.only(top: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: const Color.fromRGBO(255, 255, 255, 1),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.2),
                    spreadRadius: 3,
                    blurRadius: 10,
                    offset: Offset(0, 3),
                  ),
                ]),
            child: Column(children: [
              Wrap(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 8, top: 8, bottom: 8, right: 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        CustomTitleDetail(
                          keyItem: 'Yêu cầu khám (*)',
                          valueItem: valueRequest,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Phòng khám (*)',
                          valueItem: valueRoom,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Bác sĩ',
                          valueItem: valueDoctor,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Thời gian khám (*)',
                          valueItem: valueDate,
                          check: true,
                          icon: Icon(
                            Icons.date_range,
                            size: 15,
                          ),
                        ),
                        CustomTitleDetail(
                          keyItem: 'Nội dung khám',
                          valueItem: valueContent,
                        ),
                        CustomTitleDetail(
                          keyItem: 'Ghi chú',
                          valueItem: valueNote,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ]))
      ],
    );
  }
}
