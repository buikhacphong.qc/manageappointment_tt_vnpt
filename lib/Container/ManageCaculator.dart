import 'package:flutter/material.dart';
import 'package:product_layout_app/Container/Container1.dart';

import '../common/BaseApi.dart';
import '../model/DanhSachLichHen.dart';
import 'DetailManageCaculator.dart';
import 'ItemAppointment.dart';

// ignore: must_be_immutable
class ManageCaculator extends StatefulWidget {
  //điều hướng tĩnh
  static const routerName = '/manageCaculator';
  ManageCaculator({super.key});

  @override
  State<ManageCaculator> createState() => _ManageCaculatorState();
}

class _ManageCaculatorState extends State<ManageCaculator> {
  Map<String, String> resultTimKiem = {};
  //khoi tao số lượng hiển thị list ban đầu
  int trang = 1;
  int soDong = 3;
  bool loading = true;
  ScrollController _scrollController = ScrollController();

//nhận data trả về từ container1
  void receivedData(Map<String, String> ketqua) {
    setState(() {
      resultTimKiem = ketqua;
      print('ManageCaculator -> Giá trị text đã nhập: ${resultTimKiem}');
      initApi();
    });
  }

  @override
  void initState() {
    super.initState();
    // fetchTest();
    // initApi();
    _scrollController.addListener(
      () {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          load();
        }
      },
    );
  }

  //call api
  List<DanhSach> danhsachlichhen = [];
  void initApi() async {
    String apiUrl =
        'https://staging-apidatlichkham.vncare.vn/patient_api/api/QlhkMobile/v1.3.0/business-service/';
    String param =
        'danhSachLichHenKhamCsyt?tuNgay=${resultTimKiem['tuNgay']}&denNgay=${resultTimKiem['denNgay']}&trangThaiDongPhi&kenhDangKy&trangThaiLichHen&trangThaiDenKham&trang=1&soDong=10';
    final ketqua = await BaseApi().get(apiUrl, param);
    print("Result: $ketqua");
    DanhSachLichHen kqua = danhSachLichHenFromJson(ketqua);
    setState(() {
      Result ketqua = kqua.result;
      danhsachlichhen = ketqua.danhSach;
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    return Scaffold(
        appBar: AppBar(
          title: const Text('Quản lý lịch hẹn khám CSYT'),
          backgroundColor: Color(0xFF6F9BD4),
        ),
        body: Container(
          width: width_screen,
          height: MediaQuery.of(context).size.height,
          child: Padding(
            padding: EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 0),
            child: ListView.builder(
                controller: _scrollController,
                itemCount: 2,
                itemBuilder: (BuildContext context, int index) {
                  if (index == 0) {
                    return Card(
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                            side: BorderSide(
                              color: Color.fromARGB(255, 178, 200, 219),
                            )),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            color: Color.fromARGB(255, 178, 200, 219),
                          ),
                          width: MediaQuery.of(context).size.width,
                          height: 230,
                          child: Contain1(
                            sentData: receivedData,
                          ),
                        ));
                  } else {
                    print('trang $trang');
                    print('soDong $soDong');
                    return Container(
                        margin: EdgeInsets.only(top: 8),
                        child: Column(
                          children: [
                            ListView.separated(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount:
                                  soDong * trang <= danhsachlichhen.length
                                      ? soDong
                                      : danhsachlichhen.length,
                              itemBuilder: (BuildContext context, int index) {
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              DetailManageCaculator(
                                                  idHenKham:
                                                      danhsachlichhen[index]
                                                          .henKhamId)),
                                    );
                                  },
                                  child: ItemAppointment(
                                    trangThaiLichHen: danhsachlichhen[index]
                                        .maTrangThaiLichHen,
                                    title:
                                        danhsachlichhen[index].trangThaiLichHen,
                                    valueSate:
                                        danhsachlichhen[index].trangThaiDenKham,
                                    valueActor:
                                        danhsachlichhen[index].tenBenhNhan,
                                    valueLocation:
                                        danhsachlichhen[index].tenPhongKham,
                                    valueCause:
                                        danhsachlichhen[index].yeuCauKham,
                                    valueDate:
                                        danhsachlichhen[index].ngayGioHen,
                                    trangThaiDenKham: danhsachlichhen[index]
                                        .maTrangThaiDenKham,
                                  ),
                                );
                                // }
                              },
                              separatorBuilder:
                                  (BuildContext context, int index) {
                                return Divider(
                                  height: 8,
                                );
                              },
                            ),
                            Visibility(
                              visible: loading,
                              child: CircularProgressIndicator(),
                            ),
                          ],
                        ));
                  }
                }),
          ),
        ));
  }

  void load() {
    setState(() {
      if (soDong < danhsachlichhen.length) {
        print(soDong);
        trang += 1;
        loading = !loading;
      }
    });
  }
}
