import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:product_layout_app/Popup/CallPopup.dart';
import 'package:product_layout_app/Popup/DenyPopup.dart';
import 'package:product_layout_app/common/CustomButton.dart';
import 'package:product_layout_app/common/CustomTitleValue.dart';

class ItemAppointment extends StatelessWidget {
  final String trangThaiLichHen;
  final String trangThaiDenKham;
  final String title;
  final String valueSate;
  final String valueActor;
  final String valueLocation;
  final String valueCause;
  final String valueDate;
  // final VoidCallback onPressed;
  const ItemAppointment(
      {super.key,
      required this.title,
      required this.valueSate,
      required this.valueActor,
      required this.valueLocation,
      required this.valueCause,
      required this.valueDate,
      required this.trangThaiLichHen,
      required this.trangThaiDenKham});

  @override
  Widget build(BuildContext context) {
    Color? titleColor;
    if (trangThaiLichHen == '2') {
      titleColor = Color(0xFF5CBBB8);
    } else if (trangThaiLichHen == '0') {
      titleColor = Color(0xFFB43939);
    } else {
      titleColor = Color(0xFF6F9BD4);
    }
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
          side: BorderSide(color: Color.fromARGB(255, 152, 158, 167))),
      child: Container(
          width: MediaQuery.of(context).size.width,
          height: title == 'Mới đăng ký' ? 275 : 205,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: 37,
                decoration: BoxDecoration(
                  color: titleColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8),
                    topRight: Radius.circular(8),
                  ),
                ),
                child: Padding(
                    padding:
                        EdgeInsets.only(top: 8, bottom: 8, right: 12, left: 12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 130,
                          height: 21,
                          child: Text(
                            // title,
                            title,
                            style: TextStyle(color: Colors.white, fontSize: 14),
                          ),
                        ),
                        GestureDetector(
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return CallPopup();
                                  });
                            },
                            child: SvgPicture.asset(
                              'assets/call_svg.svg',
                              width: 24,
                              height: 21,
                            ))
                      ],
                    )),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                child: Container(
                  width: 300,
                  height: 155,
                  color: Colors.white,
                  child: Column(
                    children: [
                      CustomTitleValue(
                        key1: 'Trạng thái',
                        value1: valueSate,
                        trangThaiDenKham: trangThaiDenKham,
                      ),
                      CustomTitleValue(
                        key1: 'Bệnh nhân',
                        value1: valueActor,
                      ),
                      CustomTitleValue(
                        key1: 'Phòng khám',
                        value1: valueLocation,
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      CustomTitleValue(
                        key1: 'Lý do khám',
                        value1: valueCause,
                      ),
                      CustomTitleValue(
                        key1: 'Thời gian',
                        value1: valueDate,
                      ),
                    ],
                  ),
                ),
              ),
              Visibility(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: 33,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        CustomButton(
                            width: 120,
                            height: 32,
                            textButton: 'Từ chối',
                            buttonIcon: SvgPicture.asset('assets/deny.svg'),
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    return DenyPopup();
                                  });
                            }),
                        CustomButton(
                            width: 160,
                            height: 32,
                            textButton: 'Xác nhận lịch',
                            buttonIcon: SvgPicture.asset('assets/confirm.svg'),
                            onPressed: () {})
                      ],
                    ),
                  ),
                ),
                visible: title == 'Mới đăng ký' ? true : false,
                // maintainSize: widget.title == 'Mới đăng ký'? true : false,
              )
            ],
          )),
    );
  }
}
