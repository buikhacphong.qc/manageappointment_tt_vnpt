import 'package:flutter/material.dart';

class DetailSpinner extends StatefulWidget {
  //id nhận được từ bên container 1
  final String value1;

  DetailSpinner({super.key, required this.value1});

  @override
  State<DetailSpinner> createState() => _DetailSpinnerState();
}

class _DetailSpinnerState extends State<DetailSpinner> {
  final List<String> listPay = ['Đóng phí 1', 'Đóng phí 2', 'Đóng phí 3'];
  final List<String> listRegistry = ['Kênh 1', 'Kênh 2', 'Kênh 3'];
  final List<String> listState = ['State 1', 'State 2', 'State3'];
  final List<String> listStatus = ['Status 1', 'Status 2', 'Status 3'];
  //list trung gian
  final List<String> list = [];

  // late String result = 'Tất cả';
  //map lưu kết quả lựa chọn
  Map<String, String> result = {};

  @override
  Widget build(BuildContext context) {
    print('Giá trị id bên listDrop nhận được là: ${widget.value1}');
    if (widget.value1 == 'idPay') {
      listPay.map((e) => list.add(e)).toList();
    } else if (widget.value1 == 'idRegistry') {
      listRegistry.map((e) => list.add(e)).toList();
    } else if (widget.value1 == 'idState') {
      listState.map((e) => list.add(e)).toList();
    } else if (widget.value1 == 'idStatus') {
      listStatus.map((e) => list.add(e)).toList();
    }
    
    return Scaffold(
        // backgroundColor: Colors.grey,
        appBar: AppBar(
          title: Text("Danh muc"),
        ),
        body: Container(
          margin: EdgeInsets.only(top: 10),
          child: ListView.separated(
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      // list.clear();
                      result[widget.value1] = list[index].toString();
                      print(result);
                      Navigator.pop(context, result);
                    });
                  },
                  child: Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
                    height: 45,
                    // color: Colors.white,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: const [
                          BoxShadow(
                            color: Colors.black,
                            offset: Offset(1.0, 1.0),
                            blurRadius: 0.5,
                            spreadRadius: 0.0,
                          ),
                          BoxShadow(
                            color: Colors.white,
                            offset: const Offset(0.0, 0.0),
                            blurRadius: 0.0,
                            spreadRadius: 0.0,
                          ),
                        ]),
                    // margin: EdgeInsets.all(10),
                    child: Text(
                      list[index],
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
              itemCount: list.length),
        ));
  }
}
