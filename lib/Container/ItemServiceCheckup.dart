import 'package:flutter/material.dart';
import '../common/CustomRow.dart';

class ItemServiceCheckup extends StatelessWidget {
  final String trangThai;
  final String maDichVu;
  final String tenDichVu;
  final String giaTuVan;
  final String giaDatLich;
  // final VoidCallback onPressed;
  const ItemServiceCheckup({
    super.key,
    required this.trangThai,
    required this.maDichVu,
    required this.tenDichVu,
    required this.giaTuVan,
    required this.giaDatLich,
  });

  @override
  Widget build(BuildContext context) {
    Color? titleColor;
    if (trangThai == 'Hết hiệu lực') {
      titleColor = Color(0xFFB43939);
    } else {
      titleColor = Color(0xFF6F9BD4);
    }
    return Container(
        width: MediaQuery.of(context).size.width,
        height: 160,
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.2),
                spreadRadius: 3,
                blurRadius: 10,
                offset: Offset(0, 3),
              ),
            ],
            // color: Color(0xFFF5F5F5),
            borderRadius: BorderRadius.all(Radius.circular(5))),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: 37,
              decoration: BoxDecoration(
                color: titleColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(5),
                  topRight: Radius.circular(5),
                ),
              ),
              child: Padding(
                  padding:
                      EdgeInsets.only(top: 8, bottom: 8, right: 12, left: 12),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 130,
                        height: 21,
                        child: Text(
                          // title,
                          trangThai,
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  )),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
              child: Container(
                width: 300,
                height: 100,
                color: Colors.white,
                child: Column(
                  children: [
                    CustomRow(
                      key1: 'Tên dịch vụ',
                      value1: tenDichVu,
                    ),
                    CustomRow(
                      key1: 'Mã dịch vụ',
                      value1: maDichVu,
                    ),
                    CustomRow(
                      key1: 'Giá tư vấn online',
                      value1: giaTuVan,
                    ),
                    CustomRow(
                      key1: 'Giá đặt lịch CSYT',
                      value1: giaDatLich,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
