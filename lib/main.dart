import 'dart:io';

import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:product_layout_app/login.dart';

import 'package:flutter_localizations/flutter_localizations.dart';

//design layout
Future<void> main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Color(0xFF6f9bd4),
  ));
  WidgetsFlutterBinding.ensureInitialized();
  await SharedPrefs().init();
  HttpOverrides.global = MyHttpOverrides();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('vi', ''), // Thêm ngôn ngữ tiếng Việt
        // ...Thêm các ngôn ngữ khác nếu cần
      ],
      debugShowCheckedModeBanner: false,
      //title: 'Layout demo',
      theme: ThemeData(
        //fontFamily: GoogleFonts.nunitoSans().fontFamily,
        appBarTheme: AppBarTheme(
            systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: Color(0xFF6f9bd4),
        )),
        scaffoldBackgroundColor: Colors.white,
        //primarySwatch: Colors.black,
      ),
      //home: welcomeScreen(),
      home: splashScreen(),
      //home: LoginScreen(),
    );
  }
}

class splashScreen extends StatelessWidget {
  const splashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      splash: SvgPicture.asset(
        // 'assets/logo4.png',
        'assets/logo_app.svg',
      ),
      //nextScreen: const BottomNavigationBarHome(),
      nextScreen: const LoginScreen(),
      splashIconSize: 100,
    );
  }
}
class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}