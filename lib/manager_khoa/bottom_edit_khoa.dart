// import 'package:flutter/material.dart';
// import '../componentWidget/checkbox/custom_edit_checkbox.dart';
// import '../componentWidget/inputfield/TextAreaWidget.dart';
// import '../componentWidget/textfieldDropbox/Custom_textfield-dropbox.dart';
// import 'drop_box_khoa.dart';

// class bottomEditKhoa extends StatefulWidget {
//   final bool? s1;
//   final bool? s2;
//   final bool? s3;
//   final bool? s4;
//   final String? truongkhoa;
//   final String? hangbh;
//   final String? csyt;
//   final String? ghichu;
//   const bottomEditKhoa({super.key, required this.s1, required this.s2, required this.s3, required this.s4, required this.truongkhoa, required this.hangbh, required this.csyt, required this.ghichu});

//   @override
//   State<bottomEditKhoa> createState() => _bottomEditKhoaState();
// }


// class _bottomEditKhoaState extends State<bottomEditKhoa> {
//   var dropbox1Controller = TextEditingController();
//   var dropbox2Controller = TextEditingController();
//   var dropbox3Controller = TextEditingController();
//   var ghichuController = TextEditingController();

//   @override
//   void initState() {
//     super.initState();
//     dropbox1Controller.text = widget.truongkhoa!;
//     dropbox2Controller.text = widget.hangbh!;
//     dropbox3Controller.text = widget.csyt!;
//     ghichuController.text = widget.ghichu!;
//   }

//   @override
//   Widget build(BuildContext context) {
//     double width_new = MediaQuery.of(context).size.width;
//     double height_new = MediaQuery.of(context).size.height;

//     return Container(
//       height: height_new * 0.65,
//       decoration: BoxDecoration(
//         color: Colors.white,
//         borderRadius: BorderRadius.all(Radius.circular(8))
//       ),
//       child: Padding(
//         padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
//         child: Column(
//           children: [
//             Container(
//               margin: EdgeInsets.symmetric(vertical: 8),
//               child: Column(
//                 children: [
//                   editCheckBox(checks: widget.s1, title: 'Nhận cấp cứu'),
//                   editCheckBox(checks: widget.s2, title: 'Nhận PTTT'),
//                   editCheckBox(checks: widget.s3, title: 'Chọn giường khi chuyển đến'),
//                   editCheckBox(checks: widget.s4, title: 'Tính hóa đơn theo nguồn hoạch toán')                
//                 ],
//               ),
//             ),
//             Container(
//               padding: EdgeInsets.symmetric(vertical: 8, horizontal: 10),
//             child: Column( 
//               mainAxisAlignment: MainAxisAlignment.spaceAround,           
//               children: [ 
//                 TextFieldCheckBox(
//                   title: 'Trưởng khoa', 
//                   textController: dropbox1Controller,
//                   title2: '',
//                   onTap: () async{
//                     String? selectedValue = await
//                     Navigator.push( context, 
//                       MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'truongKhoa'))
//                       );
//                     if(selectedValue != null){
//                       setState(() {
//                         dropbox1Controller.text = selectedValue;
//                       });
//                     }
//                   },
//                 ),
//                 SizedBox(height: height_new * 0.015),
//                 TextFieldCheckBox(
//                   title: 'Hạng BH trái tuyến', 
//                   textController: dropbox2Controller,
//                   title2: '',
//                   onTap: () async{
//                     String? selectedValue = await
//                     Navigator.push( context, 
//                       MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'hangBH'))
//                       );
//                     if(selectedValue != null){
//                       setState(() {
//                         dropbox2Controller.text = selectedValue;
//                       });
//                     }
//                   },
//                 ),
//                 SizedBox(height: height_new * 0.015),               
//                 TextFieldCheckBox(
//                   title: 'Cơ sở y tế', 
//                   textController: dropbox3Controller,
//                   title2: '',
//                   onTap: () async{
//                     String? selectedValue = await
//                     Navigator.push( context, 
//                       MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'csyt'))
//                       );
//                     if(selectedValue != null){
//                       setState(() {
//                         dropbox3Controller.text = selectedValue;
//                       });
//                     }
//                   },
//                 ),  
                
//                 SizedBox(height: height_new * 0.015),
//                 TextArea(
//                   title: 'Ghi chú', 
//                   textController: ghichuController,
//                   onChanged: (p0) => ghichuController.text = p0,
//                 ),
//               ],
//             )
//           ),
//           ],
//         ),
//       )     
//     );
//   }
// }
