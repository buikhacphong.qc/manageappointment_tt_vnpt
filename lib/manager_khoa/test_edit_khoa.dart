import 'package:flutter/material.dart';
import 'package:product_layout_app/componentWidget/icon/qlhk_icons.dart';
import '../componentWidget/API/customAPI.dart';
import '../componentWidget/appBar/appbar.dart';
import '../componentWidget/checkbox/custom_edit_checkbox.dart';
import '../componentWidget/inputfield/RowTextFieldWidget.dart';
import '../componentWidget/inputfield/TextAreaWidget.dart';
import '../componentWidget/textfieldDropbox/Custom_textfield-dropbox.dart';
import '../model/departmentModel.dart';
import 'drop_box_khoa.dart';
import 'test_list_khoa.dart';

class editKhoa extends StatefulWidget {
  final Department editList;
  const editKhoa({
    Key? key, 
    required this.editList
  }) : super(key: key);

  @override
  State<editKhoa> createState() => _editKhoaState();
}

class _editKhoaState extends State<editKhoa> {
  final formKey = GlobalKey<FormState>();
  late Future<Department> department;

  var makhoaController = TextEditingController();
  var makhoaybtController = TextEditingController();
  var tenkhoaController = TextEditingController();
  var maluutruController = TextEditingController();
  var dropbox1Controller = TextEditingController();
  var dropbox2Controller = TextEditingController();

  var dropbox12Controller = TextEditingController();
  var dropbox22Controller = TextEditingController();
  var dropbox3Controller = TextEditingController();
  var ghichuController = TextEditingController();
  late bool ck1Controller;
  late bool ck2Controller;
  late bool ck3Controller;
  late bool ck4Controller;
  @override
  void initState() {
    super.initState();
    dropbox1Controller.text = widget.editList.type!;
    dropbox2Controller.text = widget.editList.specialty!;
    makhoaController.text = widget.editList.idDep!;
    makhoaybtController.text = widget.editList.idDepYbt!;
    tenkhoaController.text = widget.editList.name!;
    maluutruController.text = widget.editList.idAddress!;

    ck1Controller = widget.editList.ck1!;
    ck2Controller = widget.editList.ck2!;
    ck3Controller = widget.editList.ck3!;
    ck4Controller = widget.editList.ck4!;

    dropbox12Controller.text = widget.editList.managerDep!;
    dropbox22Controller.text = widget.editList.levelBh!;
    dropbox3Controller.text = widget.editList.csyt!;
    ghichuController.text = widget.editList.note!;
  }


  @override
  Widget build(BuildContext context) {
    double width_new = MediaQuery.of(context).size.width;
    double height_new = MediaQuery.of(context).size.height;

    return GestureDetector(
      onTap: (){
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        appBar: BaseAppBar(title: Text('Sửa thông tin Khoa'), appBar: AppBar(), widgets: [], onPress: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => listKhoa()))),
        backgroundColor: Color(0xFFF5F5F5),
        body: Form(
          key: formKey,
          child: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                padding: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                  // topEditKhoa(
                  //   ma_khoa: widget.editList.idDep, 
                  //   ma_khoabyt: widget.editList.idDepYbt, 
                  //   ten_khoa: widget.editList.name, 
                  //   loai_khoa: widget.editList.type, 
                  //   chuyen_khoa: widget.editList.specialty, 
                  //   ma_luu_tr: widget.editList.idAddress),
                  Container(
                    height: height_new * 0.5,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(8))
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      child: Column(
                        
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          RowTextField(
                            title: 'Mã khoa', title2: ' (*)', 
                            textController: makhoaController,
                            onChanged: (p0) => makhoaController.text = p0,
                            //onChanged: ,
                          ),
                          SizedBox(height: height_new * 0.015),
                          RowTextField(
                            title: 'Mã khoa(BYT)', 
                            title2: '', 
                            textController: makhoaybtController,
                            onChanged: (p0) => makhoaybtController.text = p0,
                          ),
                          
                          SizedBox(height: height_new * 0.015),
                          RowTextField(
                            title: 'Tên khoa', 
                            title2: ' (*)', 
                            textController: tenkhoaController,
                            onChanged: (p0) => tenkhoaController.text = p0,
                          ),
                          SizedBox(height: height_new * 0.015),
                          TextFieldCheckBox(
                            title: 'Loại khoa', 
                            textController: dropbox1Controller,
                            title2: ' (*)',
                            onTap: () async{
                              String? selectedValue = await
                              Navigator.push( context, 
                                MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'loaiKhoa'))
                                );
                              if(selectedValue != null){
                                setState(() {
                                  dropbox1Controller.text = selectedValue;
                                });
                              }
                            },
                          ),
                          SizedBox(height: height_new * 0.015),
                          TextFieldCheckBox(
                            title: 'Chuyên khoa', 
                            textController: dropbox2Controller,
                            title2: '',
                            onTap: () async{
                              String? selectedValue = await
                              Navigator.push( context, 
                                MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'chuyenKhoa'))
                                );
                              if(selectedValue != null){
                                setState(() {
                                  dropbox2Controller.text = selectedValue;
                                });
                              }
                            },
                          ),            
                          SizedBox(height: height_new * 0.015),
                          RowTextField(
                            title: 'Mã lưu trú', 
                            title2: '', 
                            textController: maluutruController,
                            onChanged: (p0) => maluutruController.text = p0,
                          ),
                        
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: height_new * 0.01),
                  // bottomEditKhoa(
                  //   s1: widget.editList.ck1,
                  //   s2: widget.editList.ck2,
                  //   s3: widget.editList.ck3,
                  //   s4: widget.editList.ck4,
                  //   truongkhoa: widget.editList.managerDep,
                  //   hangbh: widget.editList.levelBh,
                  //   csyt: widget.editList.csyt,
                  //   ghichu: widget.editList.note, 
                  // ),
                  Container(
                    height: height_new * 0.65,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(8))
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 8),
                            child: Column(
                              children: [
                                editCheckBox(
                                  
                                  checks: ck1Controller, 
                                  title: 'Nhận cấp cứu',
                                  onChanged: (value) {
                                    setState(() {
                                      ck1Controller = value;
                                    });
                                  },
                                  
                                ),
                                editCheckBox(
                                  checks: ck2Controller, 
                                  title: 'Nhận PTTT',
                                  onChanged: (value) {
                                    setState(() {
                                      ck2Controller = value;
                                    });
                                  },
                                ),
                                editCheckBox(
                                  checks: ck3Controller, 
                                  title: 'Chọn giường khi chuyển đến',
                                  onChanged: (value) {
                                    setState(() {
                                      ck3Controller = value;
                                    });
                                  },
                                ),
                                editCheckBox(
                                  checks: ck4Controller, 
                                  title: 'Tính hóa đơn theo nguồn hoạch toán',
                                  onChanged: (value) {
                                    setState(() {
                                      ck4Controller = value;
                                    });
                                  },
                                )                
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 10),
                          child: Column( 
                            mainAxisAlignment: MainAxisAlignment.spaceAround,           
                            children: [ 
                              TextFieldCheckBox(
                                title: 'Trưởng khoa', 
                                textController: dropbox12Controller,
                                title2: '',
                                onTap: () async{
                                  String? selectedValue = await
                                  Navigator.push( context, 
                                    MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'truongKhoa'))
                                    );
                                  if(selectedValue != null){
                                    setState(() {
                                      dropbox12Controller.text = selectedValue;
                                    });
                                  }
                                },
                              ),
                              SizedBox(height: height_new * 0.015),
                              TextFieldCheckBox(
                                title: 'Hạng BH trái tuyến', 
                                textController: dropbox22Controller,
                                title2: '',
                                onTap: () async{
                                  String? selectedValue = await
                                  Navigator.push( context, 
                                    MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'hangBH'))
                                    );
                                  if(selectedValue != null){
                                    setState(() {
                                      dropbox22Controller.text = selectedValue;
                                    });
                                  }
                                },
                              ),
                              SizedBox(height: height_new * 0.015),               
                              TextFieldCheckBox(
                                title: 'Cơ sở y tế', 
                                textController: dropbox3Controller,
                                title2: '',
                                onTap: () async{
                                  String? selectedValue = await
                                  Navigator.push( context, 
                                    MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'csyt'))
                                    );
                                  if(selectedValue != null){
                                    setState(() {
                                      dropbox3Controller.text = selectedValue;
                                    });
                                  }
                                },
                              ),  
                              
                              SizedBox(height: height_new * 0.015),
                              TextArea(
                                title: 'Ghi chú', 
                                textController: ghichuController,
                                onChanged: (p0) => ghichuController.text = p0,
                              ),
                            ],
                          )
                        ),
                        ],
                      ),
                    )     
                  ),
                ],
                ),
              ),
            ),
            Container(
              width: double.infinity,
              height: height_new * 0.07,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    //flex: 1,
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 30),
                      child: ElevatedButton.icon(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Color(0xFF6f9bd4),
                        ),
                        onPressed: (){
                          // Navigator.of(context).pop();
                          if(formKey.currentState!.validate() )
                          {
                            //Navigator.of(context).push(MaterialPageRoute(builder: (context) => listKhoa()));
                            //formKey.currentState!.save();
                            //department = updateData('${widget.editList.id}','Vu Quynh', 'Quynh Quynh', '');
                             department = updateData('${widget.editList.id}',Department(
                              makhoaController.text, 
                              makhoaybtController.text, 
                              tenkhoaController.text, 
                              dropbox1Controller.text, 
                              dropbox2Controller.text, 
                              maluutruController.text, 
                              ck1Controller, 
                              ck2Controller, 
                              ck3Controller, 
                              ck4Controller, 
                              dropbox12Controller.text, 
                              dropbox22Controller.text, 
                              dropbox3Controller.text, 
                              ghichuController.text, 
                              'Sử dụng', 
                              null));
                            
                            Navigator.push(context,MaterialPageRoute(builder: (context) =>listKhoa())); 
                            print('check');
                          }else{
                            print('Lỗi'); 
                          }
                        }, 
                        // icon: Icon(Qlhk.save_outlined),
                        icon: Icon(Icons.save_outlined),
                        label: Text('Lưu')
                      ),
                    )
                  ),
                  
                ],
              ),
            ),   
          ],
          )
        ),
      )
    );
  }
}