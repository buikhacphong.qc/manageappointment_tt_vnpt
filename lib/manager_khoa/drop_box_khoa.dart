import 'package:flutter/material.dart';

import '../componentWidget/appBar/appbar.dart';

class dropboxKhoa extends StatefulWidget {
  final String value1;

  dropboxKhoa({Key? key, required this.value1}): super(key: key);

  @override
  State<dropboxKhoa> createState() => _dropboxKhoaState();
}

class _dropboxKhoaState extends State<dropboxKhoa> {
  final List<String> loaiKhoa = ['1. Khoa Khám bệnh', '2. Khoa Hồi sức', '3. Khoa Nội tổng hợp', '4. Khoa Nội tim mạch', '5. Khoa Nội tiêu hóa'];
  final List<String> chuyenKhoa = ['Nội khoa', 'Nhi khoa', 'Da liễu', 'Tâm thần', 'Nội tiết', 'Y học cổ truyền'];
  final List<String> truongKhoa = ['Trưởng khoa A', 'Trưởng khoa B', 'Trưởng khoa C', 'Trưởng khoa D'];
  final List<String> hangBH = ['Tuyen Xa', 'Tuyen Huyen', 'Tuyen Tinh', 'Tuyen TW'];
  final List<String> csyt = ['csyt1', 'csyt2', 'csyt3', 'csyt4'];
  final List<String> list = [];

  
  Map<String, String> result = {};
  late String index1 ;

  @override
  Widget build(BuildContext context) {
    if (widget.value1 == 'loaiKhoa') {
      loaiKhoa.map((e) => list.add(e)).toList();
    } else if (widget.value1 == 'chuyenKhoa') {
      chuyenKhoa.map((e) => list.add(e)).toList();
    } else if (widget.value1 == 'truongKhoa') {
      truongKhoa.map((e) => list.add(e)).toList();
    } else if (widget.value1 == 'hangBH') {
      hangBH.map((e) => list.add(e)).toList();
    }else if(widget.value1 == 'csyt'){
      csyt.map((e) => list.add(e)).toList();
    }
    //print(widget.value1);
    String newValue;

    return Scaffold(
        appBar: BaseAppBar(
          title: Text('Danh mục'), appBar: AppBar(), widgets: []
        ),
        body: Container(
          margin: EdgeInsets.only(top: 10),
          child: ListView.separated(
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      // list.clear();
                      result[widget.value1] = list[index].toString();
                      print(result);
                      index1 = list[index].toString();
                      print(list[index].toString());
                      Navigator.pop(context, index1);
                    });
                  },
                  child: Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
                    height: 45,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                        boxShadow: const [
                          BoxShadow(
                            color: Color.fromARGB(255, 110, 109, 109),
                            offset: Offset(1.0, 1.0),
                            blurRadius: 0.5,
                            spreadRadius: 0.0,
                          ),
                          BoxShadow(
                            color: Colors.white,
                            offset: const Offset(0.0, 0.0),
                            blurRadius: 0.0,
                            spreadRadius: 0.0,
                          ),
                        ]),
                    child: Container(
                      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                      child: Text(
                        list[index],
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  ),
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
              itemCount: list.length),
        ));
  }
}
