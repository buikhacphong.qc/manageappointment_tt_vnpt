import 'package:flutter/material.dart';
import '../componentWidget/checkbox/custom_listtitle_checkbox.dart';

class botDetailKhoa extends StatefulWidget {
  final bool? s1;
  final bool? s2;
  final bool? s3;
  final bool? s4;
  final String? truongkhoa;
  final String? hangbh;
  final String? csyt;
  final String? ghichu;


  const botDetailKhoa({
    Key? key,
    required this.s1,
    required this.s2,
    required this.s3,
    required this.s4,
    required this.truongkhoa,
    required this.hangbh,
    required this.csyt,
    required this.ghichu,
  }) : super(key: key);

  @override
  State<botDetailKhoa> createState() => _botDetailKhoaState();
}

class _botDetailKhoaState extends State<botDetailKhoa> {
 
  @override

  Widget build(BuildContext context) {
    double width_new = MediaQuery.of(context).size.width;
    double height_new = MediaQuery.of(context).size.height;
    
    return Container(
      height: height_new * 0.5,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(8))
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                listCheckBox(title: 'Nhận cấp cứu', value: widget.s1),
                listCheckBox(title: 'Nhận PTTT', value: widget.s2),
                listCheckBox(title: 'Chọn giường khi chuyển đến', value: widget.s3),
                listCheckBox(title: 'Tính hóa đơn theo nguồn hoạch toán', value: widget.s4),
              ],
              
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            child: Row(
              children: [
                Wrap(
                  
                  direction: Axis.vertical, 
                  spacing: 15, 
                  children: [
                    Text('Trưởng khoa'),
                    Text('Hạng BH trái tuyến'),
                    Text('Cơ sở y tế'),
                    Text('Ghi chú')
                  ],
                ),
                SizedBox(width: width_new*0.1),
                Expanded(
                  child: Wrap(
                    
                    direction: Axis.vertical, 
                    spacing: 15, 
                    children: [
                      Text(widget.truongkhoa!),
                      Text(widget.hangbh!),
                      Text(widget.csyt!),
                      Text(widget.ghichu!, maxLines: 2),
                  ],
                  )
                )                  
              ],
            )
          ),
        ],
      ),
    );
  }
}