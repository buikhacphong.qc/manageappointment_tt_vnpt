// ignore_for_file: unused_local_variable

import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:product_layout_app/componentWidget/appBar/appbar.dart';
import 'package:product_layout_app/homescreen/bottomnavigation.dart';
import 'package:product_layout_app/manager_khoa/search.dart';
import '../componentWidget/styles/CustomColor.dart';
import '../model/departmentModel.dart';
import '../setting/setting.dart';
import 'detail_khoa.dart';
import 'item_khoa.dart';
import 'package:http/http.dart' as http;
import '../componentWidget/loading/loading.dart';
import 'test_new_khoa.dart';


class listKhoa extends StatefulWidget {
  listKhoa({Key? key}) : super(key: key);
  
  @override
  State<listKhoa> createState() => _listKhoaState();
}

class _listKhoaState extends State<listKhoa> {
  // final String makhoaController = TextEditingController().text;
  // final String tenkhoaController = TextEditingController().text;
  // final String truongkhoaController = TextEditingController().text;
  // final String trangthaiController = TextEditingController().text;
  //late List<String> searchcheck;
  
  List<Department> listkhoa = [];

  Future<List<Department>> getPostApi ()async{
    final resposne = await http.get(Uri.parse('https://64d2f05867b2662bf3db84c5.mockapi.io/api/flutter/department')) ;
    var data = jsonDecode(utf8.decode(resposne.bodyBytes));
  
    if(resposne.statusCode == 200){
      listkhoa.clear();
      for (Map<dynamic, dynamic> i in data) {
        Map<String, dynamic> typedMap = Map<String, dynamic>.from(i);
        listkhoa.add(Department.fromJson(typedMap));
      }
      return listkhoa ;
    }else {
      return listkhoa ;
    }
  }

  late Future dsKhoa;
  @override
  void initState() {
    getPostApi();
    // TODO: implement initState
    super.initState();
    
    dsKhoa = getPostApi();
  }

  void searchlistKhoa(value){
    List<Department> listSearchKhoa = [];
    for (int i = 0; i < listkhoa.length; i++){
      if((listkhoa[i].idDep!.toLowerCase().contains(value[0])) &&
        (listkhoa[i].name!.toLowerCase().contains(value[1])) &&
        (listkhoa[i].managerDep!.toLowerCase().contains(value[2])) &&
        (listkhoa[i].state!.toLowerCase().contains(value[3]))
      )
      {
        listSearchKhoa.add(listkhoa[i]);
      }
    }
    setState(() {
      listkhoa = listSearchKhoa;
      print(listkhoa.length);
    });
    // print(listkhoa.length);
  }

  @override
  
  Widget build(BuildContext context) {
    double widthNew = MediaQuery.of(context).size.width;
    double heightNew = MediaQuery.of(context).size.height;
    
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child : Scaffold(
        appBar: BaseAppBar(title: Text('Danh sách khoa'), appBar: AppBar(), widgets: [], 
          //onPress: () => Navigator.pop(context)
          onPress: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => settingPage())),
          ),
        backgroundColor: const Color(0xFFF5F5F5),
        body: Column(
          children: [
            Expanded(
              child: 
              FutureBuilder(
                future: dsKhoa,
                builder: (context, snapshot){
                  if(snapshot.connectionState == ConnectionState.waiting){
                    return const Center(
                      child: Loader(),
                    );
                  }
                  if(!snapshot.hasData){
                    return const Center(
                      child: Loader(),
                    );
                  }else{
                    if(listkhoa.isEmpty) {
                      return Container
                      (
                        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 25),
                        child: Column(
                          children: [
                            Expanded(
                              flex: 8,
                              child: searchKhoa(
                                
                                clickSearch: (value) { 
                                  getPostApi();
                                    
                                  searchlistKhoa(value);
                                  //searchlistKhoa(searchcheck);
                                    // print(listkhoa.length);
                                  //print(searchcheck);
                                }, 
                                clickAdd: () { 
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => const newKhoa()),
                                  );
                                },
                              ),
                            ), 
                            const Expanded(
                              flex: 12,
                              child: Center(
                                child: Text('Không có khoa cần tìm'),
                              )
                            )
                          ],
                        )
                      );
                    }
                    return ListView.separated(
                      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 25),
                      separatorBuilder: (context, index) => const SizedBox(height: 20), 
                      itemCount: listkhoa.length,
                      
                      itemBuilder: (BuildContext context, int index) {
                        if(index == 0){
                          return Column(
                            children: [
                              Container(
                                child: searchKhoa(
                                  clickSearch: (value) { 
                                    getPostApi();
                                    // setState(() {
                                    //   searchcheck = List.from(value);
                                    // });
                                    searchlistKhoa(value);
                                    // if(searchcheck != searchcheck){
                                    //   //getPostApi();
                                    //   print('load all');
                                    // }
                                    //searchlistKhoa(searchcheck);
                                    //print(searchcheck);
                                  }, 
                                  clickAdd: () { 
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => const newKhoa()),
                                    );
                                  },
                                ),
                              ),
                              SizedBox(height: heightNew*0.03),
                              
                              GestureDetector(
                                onTap: (){
                                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => detailKhoa(
                                    dataList : listkhoa[index],
                                  )));
                                },
                                child: itemKhoa(
                                  maKhoa : listkhoa[index].idDep,
                                  tenKhoa : listkhoa[index].name,
                                  truongKhoa : listkhoa[index].managerDep,
                                  trangThai : listkhoa[index].state
                                ),
                              )
                            ],
                          );
                        }
                        return GestureDetector(
                          
                          onTap: (){
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => detailKhoa(
                              dataList : listkhoa[index],
                            )));
                          },
                          child: itemKhoa(
                            maKhoa : listkhoa[index].idDep,
                            tenKhoa : listkhoa[index].name,
                            truongKhoa : listkhoa[index].managerDep,
                            trangThai : listkhoa[index].state
                          ),
                        );
                      }, 
                    );
                  }
                }
              ) 
            
            )
          ],
        ),
      )
    );
  } 
}
