// ignore_for_file: unused_local_variable

import 'package:flutter/material.dart';

class itemKhoa extends StatelessWidget {
  final String? maKhoa;
  final String? tenKhoa;
  final String? truongKhoa;
  final String? trangThai;
  const itemKhoa({
    Key? key,
    required this.maKhoa,
    required this.tenKhoa,
    required this.truongKhoa,
    required this.trangThai
  }) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    double width_new = MediaQuery.of(context).size.width;
    double height_new = MediaQuery.of(context).size.height;

    return Container(
      height: height_new * 0.17,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(8)),
        boxShadow: [
          BoxShadow(
            color: Colors.black54,
            blurRadius: 8,
            spreadRadius: 0,
            offset: Offset(0.0, 3.0)
          )
        ]
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: height_new * 0.04,
            width: double.infinity,
            decoration: BoxDecoration(
              color: trangThai == 'Sử dụng' ? Color(0xFF6f9bd4) : Color(0xFFB43939),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8),
                topRight: Radius.circular(8))
            ),
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              child: Text(
                trangThai!,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 14
                ),
              ),
            )
          ),

          Container(
            height: height_new * 0.13,
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(8),
                bottomRight: Radius.circular(8))
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
              child: Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text('Mã khoa'),
                      Text('Tên khoa'),
                      Text('Trưởng khoa'),
                      Text('Trạng thái')
                    ],
                  ),
                  SizedBox(width: width_new*0.05),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(maKhoa!),
                        Text(tenKhoa!),
                        Text(truongKhoa!),
                        Text(trangThai!)
                    ],
                    )
                  )                  
                ],
              )
            )
          )
        ],
      ),
    );
  }
}
