import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../componentWidget/button/FixedBottomButtonWidget.dart';
import '../componentWidget/icon/qlhk_icons.dart';
import '../componentWidget/inputfield/customcolumntextfield.dart';

class searchKhoa extends StatefulWidget {
  final Function() clickAdd;
  final Function(List<String>) clickSearch;
  const searchKhoa({super.key, required this.clickAdd, required this.clickSearch});

  @override
  State<searchKhoa> createState() => _searchKhoaState();
}

class _searchKhoaState extends State<searchKhoa> {
  FocusNode focusTruongkhoa = FocusNode();
  FocusNode focusMakhoa = FocusNode();
  FocusNode focusTenkhoa = FocusNode();
  FocusNode focusTrangthai = FocusNode();

  TextEditingController makhoaController = TextEditingController();
  TextEditingController tenkhoaController = TextEditingController();
  TextEditingController truongkhoaController = TextEditingController();
  TextEditingController trangthaiController = TextEditingController();
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    focusMakhoa.addListener(_onFocusChange);
    focusTenkhoa.addListener(_onFocusChange);
    focusTruongkhoa.addListener(_onFocusChange);
    focusTrangthai.addListener(_onFocusChange);
  }
  List <String> result = ['', '', '', ''];
  void _onFocusChange(){
    setState(() {
      result[0] = makhoaController.text;
      result[1] = tenkhoaController.text;
      result[2] = truongkhoaController.text;
      result[3] = trangthaiController.text;
    });
  }

  @override
  void dispose(){
    super.dispose();
    focusMakhoa.dispose();
    focusTenkhoa.dispose();
    focusTruongkhoa.dispose();
    focusTrangthai.dispose();
  }
   
  @override
  Widget build(BuildContext context) {
    double width_new = MediaQuery.of(context).size.width;
    double height_new = MediaQuery.of(context).size.height;

    return Container(
      height: height_new * 0.33,
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 193, 215, 245),
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Column(
          children: [
            Container(
              height: height_new * 0.1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  flex: 1,
                  child: textField(
                    title: 'Mã khoa',
                    textController: makhoaController,
                    focusNode: focusMakhoa,
                  )
                ),
                SizedBox(width: width_new * 0.03),
                Expanded(
                  flex: 1,
                  child: textField(
                    title: 'Tên khoa',
                    textController: tenkhoaController,
                    focusNode: focusTenkhoa,
                  )
                )
              ],
            ),
            ),
            
            SizedBox(height: height_new * 0.02),
            Container(
              height: height_new * 0.1,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Expanded(
                  flex: 1,
                  child: textField(
                    title: 'Trưởng khoa',
                    textController: truongkhoaController,
                    focusNode: focusTruongkhoa,
                  )
                ),
                SizedBox(width: width_new * 0.03),
                Expanded(
                  flex: 1,
                  child: textField(
                    title: 'Trạng thái',
                    textController: trangthaiController,
                    focusNode: focusTrangthai,
                  )
                )
              ],
            ),
            ),
            SizedBox(height: height_new * 0.02),
            //rowButton(),
            Row(
              children: [
                Expanded(
                  flex: 1,
                    child: FixedBottomButton(
                      text: 'Tìm kiếm', 
                      icon: Icons.search,
                      iconSize: 20, 
                      height: height_new*0.05, 
                      press: (){
                        widget.clickSearch(result);
                    })
                ),
                  
                SizedBox(width: width_new * 0.02),
                Expanded(
                  flex: 1,
                  child:  FixedBottomButton(
                      text: 'Thêm mới', 
                      icon: Icons.add_circle_outline_sharp, 
                      iconSize: 20,
                      height: height_new*0.05, 
                      press: () {
                        widget.clickAdd();
                        // Navigator.push(
                        //   context,
                        //   MaterialPageRoute(builder: (context) => const newKhoa()),
                        // );
                      },)
                  ),
              ],
            ),
          ],
        ),
      ),

    );
  }
}

