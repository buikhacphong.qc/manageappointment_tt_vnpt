//import 'dart:html';

import 'package:flutter/material.dart';
import '../componentWidget/inputfield/RowTextFieldWidget.dart';
import '../componentWidget/textfieldDropbox/Custom_textfield-dropbox.dart';
import 'drop_box_khoa.dart';

class topNewKhoa extends StatefulWidget {
  //final String call1;
  const topNewKhoa({
    super.key, 
    //required this.call1, 
  });
  
  @override
  State<topNewKhoa> createState() => _topNewKhoaState();
}

class _topNewKhoaState extends State<topNewKhoa> {

  var makhoaController = TextEditingController();
  var tenkhoaController = TextEditingController();
  var dropbox1Controller = TextEditingController(text: '--Lựa chọn--');
  var dropbox2Controller = TextEditingController(text: '--Lựa chọn--');
  
  // List<TextEditingController> textControllers = [];
  // late final List<Department> matchList;
  // void initState() {
  //   super.initState();
  //   textControllers.add(tenkhoaController);
  //   textControllers.add(makhoaController);
  //   textControllers.add(dropbox1Controller);
  //   textControllers.add(dropbox2Controller);
  // }
  @override
  Widget build(BuildContext context) {
    double width_new = MediaQuery.of(context).size.width;
    double height_new = MediaQuery.of(context).size.height;
    return Container(
      height: height_new * 0.5,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(8))
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            RowTextField(
              title: 'Mã khoa', title2: ' (*)',
              textController: makhoaController,  
                 
            ),
            
            SizedBox(height: height_new * 0.015),
            const RowTextField(title: 'Mã khoa(BYT)', title2: ''),
            SizedBox(height: height_new * 0.015),
            RowTextField(
              title: 'Tên khoa', title2: ' (*)',
              textController: tenkhoaController,     
            ),
            SizedBox(height: height_new * 0.015),
            TextFieldCheckBox(
              title: 'Loại khoa', 
              textController: dropbox1Controller,
              title2: ' (*)',
              onTap: () async{
                String? selectedValue = await
                Navigator.push( context, 
                  MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'loaiKhoa'))
                  );
                if(selectedValue != '--Lựa chọn--' && selectedValue != null){
                  setState(() {
                    dropbox1Controller.text = selectedValue;
                  });
                }
              },
            ),
            SizedBox(height: height_new * 0.015),
            TextFieldCheckBox(
              title: 'Chuyên khoa', 
              textController: dropbox2Controller,
              title2: '',
              onTap: () async{
                String? selectedValue = await
                Navigator.push( context, 
                  MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'chuyenKhoa'))
                  );
                if(selectedValue != '--Lựa chọn--' && selectedValue != null){
                  setState(() {
                    dropbox2Controller.text = selectedValue;
                  });
                }
              },
            ),            
            SizedBox(height: height_new * 0.015),
            const RowTextField(title: 'Mã lưu trú', title2: ''),
          ],
        ),
      ),
    );
  }

  // @override
  // void dispose() {
  //   // Trả về danh sách textControllers khi trang con bị đóng
  //   Navigator.pop(context, textControllers);
  //   super.dispose();
  // }
}