import 'package:flutter/material.dart';
import '../componentWidget/checkbox/custom_edit_checkbox.dart';
import '../componentWidget/icon/qlhk_icons.dart';
import '../componentWidget/inputfield/RowTextFieldWidget.dart';
import '../componentWidget/inputfield/TextAreaWidget.dart';
import '../componentWidget/textfieldDropbox/Custom_textfield-dropbox.dart';
import '../componentWidget/appBar/appbar.dart';
import '../componentWidget/API/customAPI.dart';
import '../model/departmentModel.dart';
import 'drop_box_khoa.dart';
import 'test_list_khoa.dart';


class newKhoa extends StatefulWidget {
  const newKhoa({super.key});

  @override
  State<newKhoa> createState() => _newKhoaState();
}

class _newKhoaState extends State<newKhoa> {
  //Department department = Department();
  final formKey = GlobalKey<FormState>();
  late Future<Department> department;

  var makhoaController = TextEditingController();
  var makhoaybtController = TextEditingController();
  var tenkhoaController = TextEditingController();
  var dropbox1Controller = TextEditingController(text: '--Lựa chọn--');
  var dropbox2Controller = TextEditingController(text: '--Lựa chọn--');
  var maluutruController = TextEditingController();

  var dropbox12Controller = TextEditingController(text: '--Lựa chọn--');
  var dropbox22Controller = TextEditingController(text: '--Lựa chọn--');
  var dropbox3Controller = TextEditingController(text: '--Lựa chọn--');
  var ghichuController = TextEditingController();

  late bool ck1Controller;
  late bool ck2Controller;
  late bool ck3Controller;
  late bool ck4Controller;
  @override
  void initState() {
    super.initState();
    ck1Controller = false;
    ck2Controller = false;
    ck3Controller = false;
    ck4Controller = false;
  }
  @override
  Widget build(BuildContext context) {
    double width_new = MediaQuery.of(context).size.width;
    double height_new = MediaQuery.of(context).size.height;
    
    
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child : Scaffold(
        appBar: BaseAppBar(title: Text('Thêm mới thông tin Khoa'), appBar: AppBar(), widgets: [], onPress: () => Navigator.of(context).pop(),),
        backgroundColor: Color(0xFFF5F5F5),
        body: Form(
          key: formKey,
          child: 
        Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                padding: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //topNewKhoa(),
                    Container(
                      height: height_new * 0.5,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(8))
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            RowTextField(
                              title: 'Mã khoa', title2: ' (*)',
                              textController: makhoaController,  
                                
                            ),
                            
                            SizedBox(height: height_new * 0.015),
                            RowTextField(
                              title: 'Mã khoa(BYT)', 
                              title2: '',
                              textController: makhoaybtController,
                            ),
                            SizedBox(height: height_new * 0.015),
                            RowTextField(
                              title: 'Tên khoa', title2: ' (*)',
                              textController: tenkhoaController,     
                            ),
                            SizedBox(height: height_new * 0.015),
                            TextFieldCheckBox(
                              title: 'Loại khoa', 
                              textController: dropbox1Controller,
                              title2: ' (*)',
                              onTap: () async{
                                String? selectedValue = await
                                Navigator.push( context, 
                                  MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'loaiKhoa'))
                                  );
                                if(selectedValue != '--Lựa chọn--' && selectedValue != null){
                                  setState(() {
                                    dropbox1Controller.text = selectedValue;
                                  });
                                }
                              },
                            ),
                            SizedBox(height: height_new * 0.015),
                            TextFieldCheckBox(
                              title: 'Chuyên khoa', 
                              textController: dropbox2Controller,
                              title2: '',
                              onTap: () async{
                                String? selectedValue = await
                                Navigator.push( context, 
                                  MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'chuyenKhoa'))
                                  );
                                if(selectedValue != '--Lựa chọn--' && selectedValue != null){
                                  setState(() {
                                    dropbox2Controller.text = selectedValue;
                                  });
                                }
                              },
                            ),            
                            SizedBox(height: height_new * 0.015),
                            RowTextField(
                              title: 'Mã lưu trú', 
                              title2: '',
                              textController: maluutruController,
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: height_new * 0.01),
                    //bottomNewKhoa(),
                    Container(
                      height: height_new * 0.65,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(8))
                      ),
                      child: Padding(
                        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 0),
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.symmetric(vertical: 8),
                              child: Column(
                                children:
                                [
                                  // editCheckBox(checks: false, title: 'Nhận cấp cứu'),
                                  // editCheckBox(checks: false, title: 'Nhận PTTT'),
                                  // editCheckBox(checks: false, title: 'Chọn giường khi chuyển đến'),
                                  // editCheckBox(checks: false, title: 'Tính hóa đơn theo nguồn hoạch toán') 
                                  editCheckBox(
                                  
                                    checks: ck1Controller, 
                                    title: 'Nhận cấp cứu',
                                    onChanged: (value) {
                                      setState(() {
                                        ck1Controller = value;
                                      });
                                    },
                                    
                                  ),
                                  editCheckBox(
                                    checks: ck2Controller, 
                                    title: 'Nhận PTTT',
                                    onChanged: (value) {
                                      setState(() {
                                        ck2Controller = value;
                                      });
                                    },
                                  ),
                                  editCheckBox(
                                    checks: ck3Controller, 
                                    title: 'Chọn giường khi chuyển đến',
                                    onChanged: (value) {
                                      setState(() {
                                        ck3Controller = value;
                                      });
                                    },
                                  ),
                                  editCheckBox(
                                    checks: ck4Controller, 
                                    title: 'Tính hóa đơn theo nguồn hoạch toán',
                                    onChanged: (value) {
                                      setState(() {
                                        ck4Controller = value;
                                      });
                                    },
                                  )                
                                ], 
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 10),
                            child: Column( 
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,           
                              children: [ 
                                TextFieldCheckBox(
                                  title: 'Trưởng khoa', 
                                  textController: dropbox12Controller,
                                  title2: '',
                                  onTap: () async{
                                    String? selectedValue = await
                                    Navigator.push( context, 
                                      MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'truongKhoa'))
                                      );
                                    if(selectedValue != '--Lựa chọn--' && selectedValue != null){
                                      setState(() {
                                        dropbox12Controller.text = selectedValue;
                                      });
                                    }
                                  },
                                ),
                                SizedBox(height: height_new * 0.015),
                                TextFieldCheckBox(
                                  title: 'Hạng BH trái tuyến', 
                                  textController: dropbox22Controller,
                                  title2: '',
                                  onTap: () async{
                                    String? selectedValue = await
                                    Navigator.push( context, 
                                      MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'hangBH'))
                                      );
                                    if(selectedValue != '--Lựa chọn--' && selectedValue != null){
                                      setState(() {
                                        dropbox22Controller.text = selectedValue;
                                      });
                                    }
                                  },
                                ),
                                SizedBox(height: height_new * 0.015),
                                TextFieldCheckBox(
                                  title: 'Cơ sở y tế', 
                                  textController: dropbox3Controller,
                                  title2: '',
                                  onTap: () async{
                                    String? selectedValue = await
                                    Navigator.push( context, 
                                      MaterialPageRoute(builder: (context) => dropboxKhoa(value1: 'csyt'))
                                      );
                                    if(selectedValue != '--Lựa chọn--' && selectedValue != null){
                                      setState(() {
                                        dropbox3Controller.text = selectedValue;
                                      });
                                    }
                                  },
                                ),
                                SizedBox(height: height_new * 0.015),
                                TextArea(title: 'Ghi chú', textController: ghichuController),
                              ],
                            )
                          ),
                          ],
                        ),
                      )     
                    ),
                  ],
                ),
              ),
            ),
            
            Container(
              width: double.infinity,
              height: height_new * 0.07,
              decoration: BoxDecoration(
                color: Colors.white,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    //flex: 1,
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 30),
                      child: ElevatedButton.icon(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Color(0xFF6f9bd4),
                        ),
                        
                        onPressed: (){ 
                          if(formKey.currentState!.validate() )
                          { 
                            department = addData(Department(
                              makhoaController.text, 
                              makhoaybtController.text, 
                              tenkhoaController.text, 
                              dropbox1Controller.text, 
                              dropbox2Controller.text, 
                              maluutruController.text, 
                              ck1Controller, 
                              ck2Controller, 
                              ck3Controller, 
                              ck4Controller,  
                              dropbox12Controller.text, 
                              dropbox22Controller.text, 
                              dropbox3Controller.text, 
                              ghichuController.text, 
                              'Sử dụng', 
                              null));
                            Navigator.push(context,MaterialPageRoute(builder: (context) =>listKhoa())); 
                            print('check');
                            //print(textControllers);
                          }else{
                            print('Lỗi'); 
                          }
                        }, 
                        icon: Icon(Icons.save_outlined),
                        label: Text('Lưu')
                      ),
                    )
                  ),
                  
                ],
              ),
            ),   
          ],
        ),
        )
      )
    );
  }
}

