import 'dart:convert';

import 'package:flutter/material.dart';
// import 'package:test_item/vatTu/search_container.dart';

import '../api/callapi.dart';
import '../item/ItemListRangeHour.dart';
import '../item/itemDVT.dart';
import '../model/dvt.dart';
import '../model/time.dart';
import '../timeSchedule/listSearch.dart';
import 'add_dvt.dart';
import 'search_container.dart';

class QuanLyVatTu extends StatefulWidget {
  const QuanLyVatTu({super.key});

  @override
  State<QuanLyVatTu> createState() => _QuanLyVatTuState();
}

class _QuanLyVatTuState extends State<QuanLyVatTu> {
  List<DuocVatTu> listItem = [];
  List<DuocVatTu> responseAPI = [];
  @override
  void initState() {
    super.initState();
    callAPI();
  }

  initDataAgain() {
    setState(() {
      listItem = responseAPI;
    });
  }

  void searchVatTu(List<String> value) {
    print(value[3]);
    List<DuocVatTu> listSearch = [];
    for (int i = 0; i < listItem.length; i++) {
      if (listItem[i].ma!.toLowerCase().contains(value[0]) &&
          listItem[i].ten!.toLowerCase().contains(value[1]) &&
          listItem[i].hoatchat!.toLowerCase().contains(value[2]) &&
          (listItem[i].sudung == true &&
                  "Hiệu lực".toLowerCase().contains(value[3].toLowerCase()) ||
              (listItem[i].sudung == false &&
                  "Hết hiệu lực"
                      .toLowerCase()
                      .contains(value[3].toLowerCase())))) {
        listSearch.add(listItem[i]);
      }
    }
    setState(() {
      listItem = listSearch;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: const Color(0xFFEEEEEE),
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: Color(0xFF6F9BD4),
          title: const Text(
            'Quản lý vật tư',
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ),
        body: listItem.length > 0
            ? ListView.builder(
                itemCount: listItem.length,
                itemBuilder: (BuildContext context, int index) {
                  if (index == 0) {
                    return Column(
                      children: [
                        ListSearchItemVatTu(
                          onClickAdd: (value) {
                            if (value == 'refresh') {
                              callAPI();
                            }
                          },
                          onClickSearch: (value) {
                            initDataAgain();
                            searchVatTu(value);
                          },
                        ),
                        ItemQLDVT(
                          dvt: listItem[index],
                          callbackData: (value) {
                            if (value == 'refresh') {
                              callAPI();
                            }
                          },
                        )
                      ],
                    );
                  }
                  return ItemQLDVT(
                    dvt: listItem[index],
                    callbackData: (value) {
                      if (value == 'refresh') {
                        callAPI();
                      }
                    },
                  );
                })
            : Column(
                children: [
                  Expanded(
                    flex: 1,
                    child: ListSearchItemVatTu(
                      onClickAdd: (value) {
                        if (value == 'refresh') {
                          callAPI();
                        }
                      },
                      onClickSearch: (value) {
                        initDataAgain();
                        searchVatTu(value);
                      },
                    ),
                  ),
                  Expanded(
                      flex: 2,
                      child: Center(
                        child: Text('Không có data'),
                      )),
                ],
              ),
      ),
    );
  }

  void callAPI() async {
    try {
      String url =
          'https://64d2e2e667b2662bf3db7e0a.mockapi.io/api/test/vattu/';
      final result = await BaseAPI().get(url);
      final List<dynamic> jsonList = json.decode(result);
      final List<DuocVatTu> timeBlocks =
          jsonList.map((json) => DuocVatTu.fromJson(json)).toList();
      setState(() {
        listItem = timeBlocks;
        responseAPI = timeBlocks;
      });
    } catch (e) {
      print('=================================\nloi lay danh sach vat tu: ' +
          e.toString());
    }
  }
}
