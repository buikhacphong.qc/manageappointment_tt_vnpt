import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../directory/directory_schedule_add.dart';

class EditDVTItem extends StatelessWidget {
  const EditDVTItem(
      {required this.title,
      required this.value,
      required this.keyData,
      required this.returnData,
      super.key});
  final String title;
  final String value;
  final String keyData;
  final Function(String) returnData;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Text(title, style: TextStyle(fontSize: 12)),
          ),
          Expanded(
            flex: 1,
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DirectorySchedule(
                              keyData: keyData,
                              onPress: (value) {
                                returnData(value);
                              },
                            )));
              },
              child: Container(
                height: 25,
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: Colors.grey, width: 0.5)),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 125,
                        child: Text('  ' + value,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 12)),
                      ),
                      Container(
                        height: 21,
                        child: const Icon(Icons.arrow_drop_down),
                      )
                      // const Icon(Icons.arrow_drop_down)
                    ]),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
