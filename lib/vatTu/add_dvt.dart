import 'package:flutter/material.dart';
// import 'package:test_item/api/callapi.dart';
// import 'package:test_item/model/dvt.dart';

import '../api/callapi.dart';
import '../item/elevated_button.dart';
import '../item/line_dropbutton.dart';
import '../item/line_text_border.dart';
import '../model/dvt.dart';

class AddDVT extends StatefulWidget {
  const AddDVT({super.key});

  @override
  State<AddDVT> createState() => _AddDVTState();
}

class _AddDVTState extends State<AddDVT> {
  late final _textEditingControllerMa = TextEditingController();
  late final _textEditingControllerTen = TextEditingController();
  late final _textEditingControllerSDK = TextEditingController();
  late final _textEditingControllerSQD = TextEditingController();
  late final _textEditingControllerHCSDK = TextEditingController();
  late final _textEditingControllerHamLuong = TextEditingController();
  late final _textEditingControllerDongGoi = TextEditingController();
  late final _textEditingControllerHangSX = TextEditingController();
  late final _textEditingControllerNuocSX = TextEditingController();
  IconData icon = Icons.crop_square_sharp;
  String hoatchat = '';
  String duongdung = '';
  String dvcapphat = '';

  @override
  Widget build(BuildContext context) {
    void addDataDVT() async {
      try {
        String ma = _textEditingControllerMa.text;
        String ten = _textEditingControllerTen.text;
        String sdk = _textEditingControllerSDK.text;
        String sqd = _textEditingControllerSQD.text;
        String HCSDK = _textEditingControllerHCSDK.text;
        String hamluong = _textEditingControllerHamLuong.text;
        String donggoi = _textEditingControllerDongGoi.text;
        String hangsx = _textEditingControllerHangSX.text;
        String nuocsx = _textEditingControllerNuocSX.text;
        if (ma == '' ||
            ten == '' ||
            sdk == '' ||
            HCSDK == '' ||
            hamluong == '' ||
            donggoi == '' ||
            hangsx == '' ||
            nuocsx == '' ||
            hoatchat == '' ||
            duongdung == '' ||
            dvcapphat == '') {
          print('khong de trong');
        } else {
          DuocVatTu dvt = DuocVatTu(
            ma: ma,
            ten: ten,
            sdk: sdk,
            sqd: sqd,
            hcsdk: HCSDK,
            hamluong: hamluong,
            hoatchat: hoatchat,
            duongdung: duongdung,
            dvcapphat: dvcapphat,
            donggoi: donggoi,
            hangsx: hangsx,
            nuocsx: nuocsx,
            sudung: (icon == Icons.crop_square_sharp) ? false : true,
            id: '1',
          );
          print('ggggggggggggggg' + ma);
          String url =
              'https://64d2e2e667b2662bf3db7e0a.mockapi.io/api/test/vattu/';
          final result = await BaseAPI().addDVT(url, dvt);
          Navigator.of(context).pop('refresh');
        }
      } catch (e) {
        print('========================\nco loi add DVT: ' + e.toString());
      }
    }

    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: const Color(0xFFEEEEEE),
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: Color(0xFF6F9BD4),
          title: const Text(
            'Thêm dược vật tư',
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
        body: Scaffold(
          bottomNavigationBar: Container(
            height: 40,
            margin: EdgeInsets.fromLTRB(10, 10, 10, 5),
            child: CustomElevatedButton(
                onClickIcon: () {
                  addDataDVT();
                },
                text: 'Lưu',
                icon: Icons.save_alt,
                color: Color(0xFF6F9BD4),
                colorText: Colors.white),
          ),
          body: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                    margin: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 2,
                          offset: Offset(0, 2), // changes position of shadow
                        ),
                      ],
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 10,
                          ),
                          LineTextBorder(
                              title: 'Mã (*)',
                              value: '',
                              textEditingController: _textEditingControllerMa),
                          LineTextBorder(
                              title: 'Tên (*)',
                              value: '',
                              textEditingController: _textEditingControllerTen),
                          LineTextBorder(
                              title: 'Số đăng ký',
                              value: '',
                              textEditingController: _textEditingControllerSDK),
                          LineTextBorder(
                              title: 'Số quyết định',
                              value: '',
                              textEditingController: _textEditingControllerSQD),
                          LineTextBorder(
                              title: 'Hoạt chất số đăng ký',
                              value: '',
                              textEditingController:
                                  _textEditingControllerHCSDK),
                          LineTextBorder(
                              title: 'Hàm lượng',
                              value: '',
                              textEditingController:
                                  _textEditingControllerHamLuong),
                          LineInput(
                              title: 'Hoạt chất',
                              text: hoatchat,
                              keyData: 'HoatChat',
                              getData: (value) {
                                setState(() {
                                  hoatchat = value;
                                });
                              }),
                          LineInput(
                            title: 'Đường dùng',
                            text: duongdung,
                            keyData: 'DuongDung',
                            getData: (value) {
                              setState(() {
                                duongdung = value;
                              });
                            },
                          ),
                          LineInput(
                            title: 'Đơn vị cấp phát',
                            text: dvcapphat,
                            keyData: 'DonViCapPhat',
                            getData: (value) {
                              setState(() {
                                dvcapphat = value;
                              });
                            },
                          ),
                          LineTextBorder(
                              title: 'Đóng gói (*)',
                              value: '',
                              textEditingController:
                                  _textEditingControllerDongGoi),
                          LineTextBorder(
                              title: 'Hãng sản xuất (*)',
                              value: '',
                              textEditingController:
                                  _textEditingControllerHangSX),
                          LineTextBorder(
                              title: 'Nước sản xuất (*)',
                              value: '',
                              textEditingController:
                                  _textEditingControllerNuocSX),
                          Container(
                            margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  flex: 5,
                                  child: Container(
                                      child: Text('Sử dụng',
                                          style: TextStyle(fontSize: 12))),
                                ),
                                Expanded(
                                    flex: 5,
                                    child: GestureDetector(
                                      onTap: () {
                                        setState(() {
                                          icon =
                                              (icon == Icons.crop_square_sharp)
                                                  ? Icons.check_box_outlined
                                                  : Icons.crop_square_sharp;
                                        });
                                      },
                                      child: Container(
                                          alignment: Alignment.centerLeft,
                                          child: Icon(
                                            icon,
                                            color: Colors.blue,
                                          )),
                                    )),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
