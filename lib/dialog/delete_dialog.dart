import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../api/callapi.dart';
// import 'package:test_item/api/callapi.dart';

class AlertPopup extends StatefulWidget {
  const AlertPopup({required this.id, required this.url, super.key});
  final String id;
  final String url;
  @override
  State<AlertPopup> createState() => _AlertPopupState();
}

class _AlertPopupState extends State<AlertPopup> {
  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Align(
        alignment: Alignment.center,
        child: Container(
          width: 280,
          height: 220,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            color: Colors.white,
          ),
          child: Column(children: <Widget>[
            Container(
                height: 120,
                width: 120,
                child: Image.asset('assets/delete_icon.png')),
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 5),
              child: Text(
                'BẠN CHẮC CHẮN MUỐN XÓA?',
                style: TextStyle(
                  color: Colors.blue,
                  fontSize: 18,
                ),
              ),
            ),
            Padding(
                padding: const EdgeInsets.only(
                    top: 10, bottom: 0, left: 15, right: 15),
                child: Row(children: [
                  Expanded(
                    flex: 1,
                    child: ElevatedButton(
                      child: const Text('OK'),
                      onPressed: () {
                        try {
                          BaseAPI()
                              .deleteTimeAppointment(widget.url, widget.id);
                        } catch (e) {
                          print('=======================\nloi xoa' +
                              e.toString());
                        }
                        Navigator.of(context).pop('refresh');
                      },
                      style: ButtonStyle(
                          backgroundColor:
                              const MaterialStatePropertyAll(Color(0xFF6F9BD4)),
                          foregroundColor:
                              const MaterialStatePropertyAll(Colors.white),
                          side: const MaterialStatePropertyAll(
                              BorderSide(color: Color(0xFF6F9BD4))),
                          shape: MaterialStatePropertyAll(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)))),
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    flex: 1,
                    child: ElevatedButton(
                      child: const Text('HỦY'),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      style: ButtonStyle(
                          backgroundColor:
                              const MaterialStatePropertyAll(Colors.white),
                          foregroundColor:
                              const MaterialStatePropertyAll(Color(0xFF6F9BD4)),
                          side: const MaterialStatePropertyAll(
                              BorderSide(color: Color(0xFF6F9BD4))),
                          shape: MaterialStatePropertyAll(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)))),
                    ),
                  )
                ])),
          ]),
        ),
      ),
    );
  }
}
