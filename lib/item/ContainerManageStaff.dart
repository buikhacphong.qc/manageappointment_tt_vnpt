import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../common/CustomButton.dart';
import '../common/CustomContain.dart';

class ContainerManageStaff extends StatefulWidget {
  final void Function(Map<String, String> ketqua) dataSend;
  const ContainerManageStaff({
    Key? key,
    required this.dataSend,
  }) : super(key: key);

  @override
  State<ContainerManageStaff> createState() => _ContainerManageStaffState();
}

class _ContainerManageStaffState extends State<ContainerManageStaff> {
  final FocusNode _focusMaBS = FocusNode();
  final FocusNode _focusMaNV = FocusNode();
  final FocusNode _focusTenNV = FocusNode();
  final FocusNode _focusKhoa = FocusNode();
  final FocusNode _focusLoaiNV = FocusNode();

  final TextEditingController textMaBS = TextEditingController();
  final TextEditingController textMaNV = TextEditingController();
  final TextEditingController textTenNV = TextEditingController();
  final TextEditingController textKhoa = TextEditingController();
  final TextEditingController textLoaiNV = TextEditingController();

  @override
  void initState() {
    super.initState();
    _focusMaBS.addListener(_onFocusChange);
    _focusMaNV.addListener(_onFocusChange);
    _focusTenNV.addListener(_onFocusChange);
    _focusKhoa.addListener(_onFocusChange);
    _focusLoaiNV.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    _focusMaBS.dispose();
    _focusMaNV.dispose();
    _focusTenNV.dispose();
    _focusKhoa.dispose();
    _focusLoaiNV.dispose();

    super.dispose();
  }

  final Map<String, String> ketqua = {};
  void _onFocusChange() {
    setState(() {
      ketqua['textMaNV'] = textMaNV.text;
      ketqua['textMaBS'] = textMaBS.text;
      ketqua['textTenNV'] = textTenNV.text;
      ketqua['textKhoa'] = textKhoa.text;
      ketqua['textLoaiNV'] = textLoaiNV.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.all(10),
      child: Container(
          width: MediaQuery.of(context).size.width,
          height: 200,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: CustomContain(
                      containerText: 'Mã nhân viên',
                      width: width_screen / 2,
                      focusNode: _focusMaNV,
                      textEditingController: textMaNV,
                      onPress: () {
                        textMaNV.clear();
                      },
                    ),
                  ),
                  Container(
                    width: 5,
                  ),
                  Expanded(
                    child: CustomContain(
                      containerText: 'Mã bác sĩ',
                      width: width_screen / 2,
                      focusNode: _focusMaBS,
                      textEditingController: textMaBS,
                      onPress: () {
                        textMaBS.clear();
                      },
                    ),
                  )
                ],
              ),
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    child: CustomContain(
                      containerText: 'Tên nhân viên',
                      width: width_screen / 2,
                      focusNode: _focusTenNV,
                      textEditingController: textTenNV,
                      onPress: () {
                        textTenNV.clear();
                      },
                    ),
                  ),
                  Container(
                    width: 5,
                  ),
                  Expanded(
                    child: CustomContain(
                      containerText: 'Khoa',
                      width: width_screen / 2,
                      focusNode: _focusKhoa,
                      textEditingController: textKhoa,
                      onPress: () {
                        textKhoa.clear();
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    child: CustomContain(
                      containerText: 'Loại nhân viên',
                      width: width_screen / 2,
                      focusNode: _focusLoaiNV,
                      textEditingController: textLoaiNV,
                      onPress: () {
                        textLoaiNV.clear();
                      },
                    ),
                  ),
                  Container(
                    width: 5,
                  ),
                  Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: width_screen / 2,
                            height: 15,
                          ),
                          SizedBox(height: 5),
                          CustomButton(
                              width: width_screen / 2,
                              height: 30,
                              textButton: 'Tìm kiếm',
                              buttonIcon: SvgPicture.asset('assets/search.svg'),
                              onPressed: () {
                                print(ketqua);
                              }),
                        ]),
                  )
                ],
              ),
            ],
          )),
    );
  }
}
