import 'package:flutter/material.dart';

import '../common/CustomTextValue.dart';

class ItemManageRoom extends StatelessWidget {
  final bool trangThai;
  final String maPhong;
  final String tenPhong;
  final String khoa;
  const ItemManageRoom(
      {Key? key,
      required this.trangThai,
      required this.maPhong,
      required this.tenPhong,
      required this.khoa})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color? titleColor;
    if (trangThai == false) {
      titleColor = Color(0xFFB43939);
    } else {
      titleColor = Color(0xFF6F9BD4);
    }
    return Card(
        elevation: 4,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
            side: BorderSide(color: Color(0xFF6F9BD4))),
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: 37,
              decoration: BoxDecoration(
                color: titleColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8),
                  topRight: Radius.circular(8),
                ),
              ),
              child: Padding(
                  padding:
                      EdgeInsets.only(top: 12, bottom: 8, right: 12, left: 12),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        width: 130,
                        height: 21,
                        child: Text(
                          // title,
                          trangThai==false ? 'Hủy bỏ': 'Sử dụng',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                  )),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 130,
                color: Colors.white,
                child: Column(
                  children: [
                    CustomTextValue(
                      key1: 'Mã phòng',
                      value1: maPhong,
                    ),
                    CustomTextValue(
                      key1: 'Tên phòng',
                      value1: tenPhong,
                    ),
                    CustomTextValue(
                      key1: 'Khoa',
                      value1: khoa,
                    ),
                    CustomTextValue(
                      key1: 'Trạng thái',
                      value1: trangThai==false ? 'Hủy bỏ': 'Sử dụng',
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
