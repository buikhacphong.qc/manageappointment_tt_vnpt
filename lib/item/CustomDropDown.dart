import 'dart:io';

import 'package:flutter/material.dart';
import 'package:product_layout_app/directory/directory_schedule_add.dart';
// import 'package:test_item/directory/directory_schedule_add.dart';

class CustomDropDownByMyself extends StatelessWidget {
  final String title;
  final String value;
  final int maxLines;
  final Function(String) onPress;
  final IconData icon;
  final String keyData;

  const CustomDropDownByMyself(
      {Key? key,
      required this.title,
      required this.value,
      this.maxLines = 1,
      required this.onPress,
      required this.icon,
      required this.keyData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double deviceHeight = MediaQuery.of(context).size.height;
    final isSmallMobile =
        Platform.isAndroid ? deviceHeight < 600 : deviceHeight < 700;
    return Row(
      children: <Widget>[
        Expanded(
          flex: 4,
          child: Align(
            alignment: Alignment.topLeft,
            child: Text(
              title,
              style: const TextStyle(
                color: Color(0xFF535858),
                fontSize: 12,
                fontWeight: FontWeight.w400,
              ),
              textAlign: TextAlign.left,
            ),
          ),
        ),
        Expanded(
          flex: 6,
          child: TextButton(
            style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.black),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4.0),
                  side: BorderSide(color: Color(0xFFE0E0E0)),
                ),
              ),
            ),
            // onPressed: onPress,
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DirectorySchedule(
                            keyData: keyData,
                            onPress: (value) {
                              onPress(value);
                            },
                          )));
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ConstrainedBox(
                  constraints: BoxConstraints(
                      maxWidth: isSmallMobile
                          ? size.width * 0.42
                          : size.width * 0.45),
                  child: Container(
                    child: Text(
                      value,
                      maxLines: maxLines,
                      textAlign: TextAlign.left,
                      style: const TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Color(0xFF444444)),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
                Icon(
                  icon,
                  color: Color(0xFFBBC2C6),
                  size: 16,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
