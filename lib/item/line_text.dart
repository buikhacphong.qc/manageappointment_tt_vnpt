import 'package:flutter/material.dart';

class MyWidget extends StatelessWidget {
  const MyWidget({required this.title, required this.text, super.key});

  final String title;
  final String text;

  @override
  Widget build(BuildContext context) {
    Color _colorText = Colors.black;
    if (text == 'Đang áp dụng') {
      _colorText = Colors.blue;
    } else if (text == 'Chưa áp dụng') {
      _colorText = Colors.red;
    }

    return Container(
      margin: const EdgeInsets.fromLTRB(10, 10, 10, 0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 5,
            child:
                Container(child: Text(title, style: TextStyle(fontSize: 12))),
          ),
          Expanded(
            flex: 4,
            child: Container(
              // height: 27,
              child: Text(
                text,
                overflow: TextOverflow.ellipsis,
                maxLines: 5,
                style: TextStyle(fontSize: 12, color: _colorText),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
