import 'package:flutter/material.dart';

import '../directory/directory_schedule_add.dart';

class LineInput extends StatelessWidget {
  const LineInput(
      {required this.title,
      required this.text,
      this.flexLeft = 1,
      this.flexRight = 1,
      required this.getData,
      required this.keyData,
      super.key});
  final String title;
  final String text;
  final int? flexLeft;
  final int? flexRight;
  final String keyData;
  final Function(String) getData;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
      child: Row(
        children: [
          Expanded(
            flex: flexLeft ?? 1,
            child:
                Container(child: Text(title, style: TextStyle(fontSize: 12))),
          ),
          Expanded(
            flex: flexRight ?? 1,
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DirectorySchedule(
                              keyData: keyData,
                              onPress: (value) {
                                getData(value);
                              },
                            )));
              },
              child: Container(
                height: 25,
                alignment: Alignment.centerLeft,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: Colors.grey, width: 0.5)),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text(' ' + text,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 12)),
                      ),
                      Container(
                        height: 21,
                        child: const Icon(Icons.arrow_drop_down),
                      )
                      // const Icon(Icons.arrow_drop_down)
                    ]),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// class LineInput extends StatefulWidget {
//   const LineInput(
//       {required this.title,
//       required this.text,
//       this.flexLeft = 1,
//       this.flexRight = 1,
//       super.key});
//   final String title;
//   final String text;
//   final int? flexLeft;
//   final int? flexRight;
//   @override
//   State<LineInput> createState() => _LineInputState();
// }

// class _LineInputState extends State<LineInput> {
//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
//       child: Row(
//         children: [
//           Expanded(
//             flex: widget.flexLeft ?? 1,
//             child: Container(
//                 child: Text(widget.title, style: TextStyle(fontSize: 12))),
//           ),
//           Expanded(
//             flex: widget.flexRight ?? 1,
//             child: GestureDetector(
//               onTap: () {
//                 Navigator.push(
//                     context,
//                     MaterialPageRoute(
//                         builder: (context) => DirectorySchedule()));
//               },
//               child: Container(
//                 height: 25,
//                 alignment: Alignment.centerLeft,
//                 decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(5),
//                     border: Border.all(color: Colors.grey, width: 0.5)),
//                 child: Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                     children: [
//                       Container(
//                         child: Text(' ' + widget.text,
//                             maxLines: 1,
//                             overflow: TextOverflow.ellipsis,
//                             style: TextStyle(fontSize: 12)),
//                       ),
//                       Container(
//                         height: 21,
//                         child: const Icon(Icons.arrow_drop_down),
//                       )
//                       // const Icon(Icons.arrow_drop_down)
//                     ]),
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
