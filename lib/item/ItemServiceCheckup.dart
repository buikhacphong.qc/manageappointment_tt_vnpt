import 'package:flutter/material.dart';

import '../common/CustomTextValue.dart';

class ItemServiceCheckup extends StatelessWidget {
  final bool trangThai;
  final String maDichVu;
  final String tenDichVu;
  final String giaTuVan;
  final String giaDatLich;
  const ItemServiceCheckup({
    required this.trangThai,
    required this.maDichVu,
    required this.tenDichVu,
    required this.giaTuVan,
    required this.giaDatLich,
  });

  @override
  Widget build(BuildContext context) {
    Color? titleColor;
    if (trangThai == false) {
      titleColor = Color(0xFFB43939);
    } else {
      titleColor = Color(0xFF6F9BD4);
    }
    return Card(
        elevation: 4,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
            side: BorderSide(color: Color.fromARGB(255, 152, 158, 167))),
        child: Container(
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                height: 37,
                decoration: BoxDecoration(
                  color: titleColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8),
                    topRight: Radius.circular(8),
                  ),
                ),
                child: Padding(
                    padding:
                        EdgeInsets.only(top: 8, bottom: 8, right: 12, left: 12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: 130,
                          height: 21,
                          child: Text(
                            trangThai == true ? 'Hiệu lực' : 'Hết hiệu lực',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    )),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 130,
                  color: Colors.white,
                  child: Column(
                    children: [
                      CustomTextValue(
                        key1: 'Tên dịch vụ',
                        value1: tenDichVu,
                      ),
                      CustomTextValue(
                        key1: 'Mã dịch vụ',
                        value1: maDichVu,
                      ),
                      CustomTextValue(
                        key1: 'Giá tư vấn online',
                        value1: giaTuVan,
                      ),
                      CustomTextValue(
                        key1: 'Giá đặt lịch CSYT',
                        value1: giaDatLich,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
