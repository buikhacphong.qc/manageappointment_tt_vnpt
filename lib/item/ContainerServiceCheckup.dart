import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../common/CustomButton.dart';
import '../common/CustomContain.dart';
import '../model/dichVuKham.dart';
import '../servicecheckup/AddEditService.dart';

class ContainerServiceCheckup extends StatefulWidget {
  final void Function(Map<String, String> ketqua) onData;
  final void Function(DichVuKham dichVuKham) senData;
  ContainerServiceCheckup({
    Key? key,
    required this.onData,
    required this.senData,
  }) : super(key: key);

  @override
  State<ContainerServiceCheckup> createState() =>
      _ContainerServiceCheckupState();
}

class _ContainerServiceCheckupState extends State<ContainerServiceCheckup> {
  final FocusNode _focusTenDV = FocusNode();
  final FocusNode _focusMaDV = FocusNode();
  final FocusNode _focusTrangThai = FocusNode();
  final FocusNode _focusTuVan = FocusNode();
  final FocusNode _focusDatLich = FocusNode();

  final TextEditingController textTen = TextEditingController();
  final TextEditingController textMa = TextEditingController();
  final TextEditingController textTrangThai = TextEditingController();
  final TextEditingController textTuVan = TextEditingController();
  final TextEditingController textDatLich = TextEditingController();

  @override
  void initState() {
    super.initState();
    _focusTenDV.addListener(_onFocusChange);
    _focusMaDV.addListener(_onFocusChange);
    _focusTrangThai.addListener(_onFocusChange);
    _focusTuVan.addListener(_onFocusChange);
    _focusDatLich.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    _focusTenDV.dispose();
    _focusMaDV.dispose();
    _focusTrangThai.dispose();
    _focusTuVan.dispose();
    _focusDatLich.dispose();

    super.dispose();
  }

  void _onFocusChange() {
    setState(() {
      ketqua['textTen'] = textTen.text;
      ketqua['textMa'] = textMa.text;
      ketqua['textTrangThai'] = textTrangThai.text;
      ketqua['textTuVan'] = textTuVan.text;
      ketqua['textDatLich'] = textDatLich.text;
    });
  }

  Map<String, String> ketqua = {};

  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.all(10),
      child: Container(
          width: MediaQuery.of(context).size.width,
          height: 200,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: CustomContain(
                      containerText: 'Tên dịch vụ',
                      width: width_screen,
                      focusNode: _focusTenDV,
                      textEditingController: textTen,
                      onPress: () {
                        textTen.clear();
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    child: CustomContain(
                      containerText: 'Mã dịch vụ',
                      width: width_screen / 2,
                      focusNode: _focusMaDV,
                      textEditingController: textMa,
                      onPress: () {
                        textMa.clear();
                      },
                    ),
                  ),
                  Container(
                    width: 5,
                  ),
                  Expanded(
                    child: CustomContain(
                      containerText: 'Trạng thái',
                      width: width_screen / 2,
                      focusNode: _focusTrangThai,
                      textEditingController: textTrangThai,
                      onPress: () {
                        textTrangThai.clear();
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    child: CustomContain(
                      containerText: 'Giá tư vấn online',
                      width: width_screen / 2,
                      focusNode: _focusTuVan,
                      textEditingController: textTuVan,
                      onPress: () {
                        textTuVan.clear();
                      },
                    ),
                  ),
                  Container(
                    width: 5,
                  ),
                  Expanded(
                    child: CustomContain(
                      containerText: 'Giá đặt lịch CSYT',
                      width: width_screen / 2,
                      focusNode: _focusDatLich,
                      textEditingController: textDatLich,
                      onPress: () {
                        textDatLich.clear();
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Container(
                width: width_screen,
                height: 30,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                        child: CustomButton(
                            width: width_screen / 2,
                            height: 30,
                            textButton: 'Tìm kiếm',
                            buttonIcon: SvgPicture.asset('assets/search.svg'),
                            onPressed: () {
                              widget.onData(ketqua);
                            }),
                      ),
                      Container(
                        width: 5,
                      ),
                      Expanded(
                        child: CustomButton(
                            width: width_screen / 2,
                            height: 30,
                            textButton: 'Thêm mới',
                            buttonIcon: SvgPicture.asset('assets/add.svg'),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => AddServiceCheckup(
                                            check: true,
                                            dichvukham: DichVuKham(
                                              maDichVu: '',
                                              tenDichVu: '',
                                              phiTuVanCu: '',
                                              phiTuVanMoi: '',
                                              phiDatLichCu: '',
                                              phiDatLichMoi: '',
                                              ngayAdGiaMoi: '',
                                              trangThai: false,
                                              listPhong: [],
                                              id: '',
                                            ),
                                          ))).then((value) {
                                            if(value[1]=='add'){
                                              widget.senData(value[0]);
                                            }
                              });
                            }),
                      )
                    ]),
              )
            ],
          )),
    );
  }
}
