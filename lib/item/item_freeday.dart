import 'package:flutter/material.dart';

import '../freeday/freeDay_detail.dart';
import '../freeday/freeDay_detail_range_date.dart';
import '../model/freeday.dart';

class ItemFreeday extends StatefulWidget {
  const ItemFreeday(
      {required this.freeDay, required this.callbackdata, super.key});
  final FreeDay freeDay;
  final Function(String) callbackdata;
  @override
  State<ItemFreeday> createState() => _ItemFreedayState();
}

class _ItemFreedayState extends State<ItemFreeday> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => widget.freeDay.type == '1'
                      ? FreeDayDetail(
                          freeDay: widget.freeDay,
                        )
                      : FreeDayDetail2(freeDay: widget.freeDay))).then((value) {
            widget.callbackdata(value);
          });
        },
        child: Card(
          color: Colors.white,
          shape: RoundedRectangleBorder(
            side: BorderSide(
                color: widget.freeDay.type == '1'
                    ? Color(0xFF6F9BD4)
                    : Color(0xFF5CBBB8),
                width: 1),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            children: [
              Container(
                height: 35,
                decoration: BoxDecoration(
                    color: widget.freeDay.type == '1'
                        ? Color(0xFF6F9BD4)
                        : Color(0xFF5CBBB8),
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10))),
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    widget.freeDay.type == '1'
                        ? '  Ngày cụ thể trong năm'
                        : '  Ngày cố định hàng tuần',
                    style: TextStyle(fontSize: 12, color: Colors.white),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.all(5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Expanded(
                      flex: 1,
                      child: Text('Ngày nghỉ', style: TextStyle(fontSize: 12)),
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(
                        widget.freeDay.type == '1'
                            ? '${widget.freeDay.fromDay} - ${widget.freeDay.toDay}'
                            : widget.freeDay.thu!,
                        style: const TextStyle(
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.fromLTRB(5, 0, 5, 5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Expanded(
                      flex: 1,
                      child: Text('Nghỉ sáng', style: TextStyle(fontSize: 12)),
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(
                          widget.freeDay.freeMorning!
                              ? 'Hiệu lực'
                              : 'Không có hiệu lực',
                          style: const TextStyle(fontSize: 12)),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.fromLTRB(5, 0, 5, 5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Expanded(
                      flex: 1,
                      child: Text('Nghỉ chiều', style: TextStyle(fontSize: 12)),
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(
                          widget.freeDay.freeAfternoon!
                              ? 'Hiệu lực'
                              : 'Không có hiệu lực',
                          style: TextStyle(fontSize: 12)),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.fromLTRB(5, 0, 5, 5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Expanded(
                      flex: 1,
                      child: Text('Nghỉ tư vấn từ xa',
                          style: TextStyle(fontSize: 12)),
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(
                          widget.freeDay.freeRemote!
                              ? 'Hiệu lực'
                              : 'Không có hiệu lực',
                          style: TextStyle(fontSize: 12)),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.fromLTRB(5, 0, 5, 5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                          child: Text('Nghỉ tư vấn tại CSYT',
                              style: TextStyle(fontSize: 12))),
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(
                          widget.freeDay.freeCSYT!
                              ? 'Hiệu lực'
                              : 'Không có hiệu lực',
                          style: TextStyle(fontSize: 12)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
