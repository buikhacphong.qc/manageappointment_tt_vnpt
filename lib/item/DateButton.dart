import 'package:flutter/material.dart';
// import 'package:vncare_qlhk/config/styles/Dimens.dart';
// import 'package:vncare_qlhk/qlhk_icons.dart';

class DateButton extends StatelessWidget {
  final String? value;
  final Function()? onPressed;
  final int? maxLines;
  final IconData? icon;
  const DateButton(
      {Key? key, this.value, this.onPressed, this.maxLines = 1, this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10, bottom: 8.0),
      child: TextButton(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
          foregroundColor: MaterialStateProperty.all<Color>(Colors.black),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
              side: BorderSide(color: Color(0xFFE0E0E0)),
            ),
          ),
        ),
        onPressed: onPressed,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ConstrainedBox(
              constraints: BoxConstraints(
                  maxWidth: size.width * 0.75, minWidth: size.width * 0.68),
              child: Container(
                child: Text(
                  value ?? "",
                  maxLines: maxLines,
                  textAlign: TextAlign.left,
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFF444444)),
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
            Icon(
              icon ?? Icons.calendar_month,
              color: Color(0xFFBBC2C6),
              size: 16,
            ),
          ],
        ),
      ),
    );
  }
}
