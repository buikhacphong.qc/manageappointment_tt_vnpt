import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

class CustomButton extends StatelessWidget {
  const CustomButton(
      {required this.text,
      required this.icon,
      required this.onClick,
      super.key});
  final String text;
  final IconData icon;
  final Function() onClick;
  @override
  Widget build(BuildContext context) {
    return Container(
      // width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.fromLTRB(10, 5, 10, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SizedBox(
            height: 30,
            child: ElevatedButton(
              onPressed: () {
                print('===============================\nvao tim kiem');
                onClick();
              },
              style: ButtonStyle(
                  backgroundColor:
                      const MaterialStatePropertyAll(Color(0xFF6F9BD4)),
                  foregroundColor: const MaterialStatePropertyAll(Colors.white),
                  shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)))),
              child: Row(
                children: [
                  Icon(
                    icon,
                    size: 14,
                  ),
                  Text(text, style: TextStyle(fontSize: 12))
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
