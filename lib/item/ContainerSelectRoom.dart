import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../common/CustomButton.dart';
import '../common/CustomContain.dart';

class ContainerSelectRoom extends StatefulWidget {
  final void Function(Map<String, String> resultTimKiem) dataSend;
  ContainerSelectRoom({
    Key? key,
    required this.dataSend,
  }) : super(key: key);

  @override
  State<ContainerSelectRoom> createState() => _ContainerSelectRoomState();
}

class _ContainerSelectRoomState extends State<ContainerSelectRoom> {
  final FocusNode _focusTenPhong = FocusNode();
  final FocusNode _focusMaPhong = FocusNode();
  final FocusNode _focusTuVan = FocusNode();
  final FocusNode _focusDatLich = FocusNode();

  final TextEditingController textTen = TextEditingController();
  final TextEditingController textMa = TextEditingController();
  final TextEditingController textDatLich = TextEditingController();
  final TextEditingController textTuVan = TextEditingController();

  Map<String, String> resultTimKiem = {};

  @override
  void initState() {
    super.initState();
    _focusTenPhong.addListener(_onFocusChange);
    _focusMaPhong.addListener(_onFocusChange);
    _focusTuVan.addListener(_onFocusChange);
    _focusDatLich.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    _focusMaPhong.dispose();
    _focusTenPhong.dispose();
    _focusTuVan.dispose();
    _focusDatLich.dispose();

    super.dispose();
  }

  void _onFocusChange() {
    setState(() {
      resultTimKiem['textTen'] = textTen.text;
      resultTimKiem['textMa'] = textMa.text;
      resultTimKiem['textTuVan'] = textTuVan.text;
      resultTimKiem['textDatLich'] = textDatLich.text;
    });
  }

  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.all(10),
      child: Container(
          width: MediaQuery.of(context).size.width,
          height: 200,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Expanded(
                    child: CustomContain(
                      containerText: 'Tên phòng',
                      width: width_screen,
                      focusNode: _focusTenPhong,
                      textEditingController: textTen,
                      onPress: () {
                        textTen.clear();
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    child: CustomContain(
                      containerText: 'Mã phòng',
                      width: width_screen / 2,
                      focusNode: _focusMaPhong,
                      textEditingController: textMa,
                      onPress: () {
                        textMa.clear();
                      },
                    ),
                  ),
                  Container(
                    width: 5,
                  ),
                  Expanded(
                    child: CustomContain(
                      containerText: 'Giá tư vấn online',
                      width: width_screen / 2,
                      focusNode: _focusTuVan,
                      textEditingController: textTuVan,
                      onPress: () {
                        textTuVan.clear();
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    child: CustomContain(
                      containerText: 'Giá đặt lịch CSYT',
                      width: width_screen / 2,
                      focusNode: _focusDatLich,
                      textEditingController: textDatLich,
                      onPress: () {
                        textDatLich.clear();
                      },
                    ),
                  ),
                  Container(
                    width: 5,
                  ),
                  Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: width_screen / 2,
                            height: 15,
                          ),
                          SizedBox(height: 5),
                          CustomButton(
                              width: width_screen / 2,
                              height: 30,
                              textButton: 'Tìm kiếm',
                              buttonIcon: SvgPicture.asset('assets/search.svg'),
                              onPressed: () {
                                widget.dataSend(resultTimKiem);
                              }),
                        ]),
                  )
                ],
              ),
            ],
          )),
    );
  }
}
