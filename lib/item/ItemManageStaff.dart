import 'package:flutter/material.dart';

import '../common/CustomTextValue.dart';

class ItemManageStaff extends StatelessWidget {
  final String maNV;
  final String maBacSy;
  final String tenNV;
  final String khoa;
  final String loaiNV;
  final String phong;
  const ItemManageStaff(
      {Key? key,
      required this.maNV,
      required this.maBacSy,
      required this.tenNV,
      required this.khoa,
      required this.loaiNV,
      required this.phong})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 4,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
            side: BorderSide(color: Color(0xFF6F9BD4))),
        child: Container(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 180,
                  color: Colors.white,
                  child: Column(
                    children: [
                      CustomTextValue(
                        key1: 'Mã nhân viên',
                        value1: maNV,
                      ),
                      CustomTextValue(
                        key1: 'Mã bác sỹ',
                        value1: maBacSy,
                      ),
                      CustomTextValue(
                        key1: 'Tên nhân viên',
                        value1: tenNV,
                      ),
                      CustomTextValue(
                        key1: 'Loại nhân viên',
                        value1: loaiNV,
                      ),
                      CustomTextValue(
                        key1: 'Khoa',
                        value1: khoa,
                      ),
                      CustomTextValue(
                        key1: 'Phòng',
                        value1: phong,
                      ),
                    ],
                  ),
                ),
              ),
              Text(
                tenNV
                // title,
                ,
                style: TextStyle(color: Colors.white),
              ),
            ],
          ),
        ));
  }
}
