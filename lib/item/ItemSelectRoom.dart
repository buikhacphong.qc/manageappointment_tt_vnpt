import 'package:flutter/material.dart';

import '../common/CustomButtonCheckbox.dart';
import '../common/CustomTextValue.dart';

// ignore: must_be_immutable
class ItemSelectRoom extends StatelessWidget {
  final String tenPhong;
  final String maPhong;
  bool ktraTuVan;
  final VoidCallback onClickTuVan;
  final VoidCallback onClickDatLich;
  bool ktraDatLich;
  ItemSelectRoom(
      {Key? key,
      required this.tenPhong,
      required this.maPhong,
      required this.onClickTuVan,
      required this.onClickDatLich,
      required this.ktraTuVan,
      required this.ktraDatLich})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double width_screen = MediaQuery.of(context).size.width;
    return Card(
      elevation: 4,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
          side: BorderSide(color: Color(0xFF6F9BD4))),
      child: Container(
        width: 200,
        child: Padding(
          padding: EdgeInsets.only(top: 10, left: 15, right: 15, bottom: 0),
          child: Column(
            children: [
              CustomTextValue(
                key1: 'Tên phòng',
                value1: tenPhong,
              ),
              CustomTextValue(
                key1: 'Mã phòng',
                value1: maPhong,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 60,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                        child: CustomButtonCheckbox(
                            width: width_screen / 2,
                            height: 30,
                            textButton: 'Tư vấn online',
                            checkIcon: ktraTuVan,
                            onPressed: onClickTuVan),
                      ),
                      Container(
                        width: 5,
                      ),
                      Expanded(
                        child: CustomButtonCheckbox(
                            width: width_screen / 2,
                            height: 30,
                            checkIcon: ktraDatLich,
                            textButton: 'Đặt lịch CSYT',
                            onPressed: onClickDatLich),
                      ),
                    ]),
              )
            ],
          ),
        ),
      ),
    );
  }
}
