import 'package:flutter/material.dart';
import 'package:product_layout_app/freeday/freeday_manage.dart';
import 'package:product_layout_app/manageroom/ManageRoom.dart';
import 'package:product_layout_app/managestaff/ManageStaff.dart';
import 'package:product_layout_app/servicecheckup/ManageService.dart';
import 'package:product_layout_app/timeSchedule/createschedule.dart';
import 'package:product_layout_app/vatTu/ql_dvt.dart';

import '../componentWidget/appBar/appbar.dart';
import '../componentWidget/styles/CustomColor.dart';
import '../homescreen/bottomnavigation.dart';
import '../manager_khoa/test_list_khoa.dart';

class settingPage extends StatelessWidget {
  const settingPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: Text('Cài đặt'),
        appBar: AppBar(),
        widgets: [],
        onPress: () => Navigator.push(context,
            MaterialPageRoute(builder: (context) => BottomNavigationBarHome())),
      ),
      body: Container(
          padding: EdgeInsets.all(15),
          child: ListView(
            children: [
              SizedBox(height: 15),
              Row(
                children: [
                  Icon(
                    Icons.category_sharp,
                    color: primaryColor,
                  ),
                  SizedBox(width: 15),
                  Text('Danh mục',
                      style: TextStyle(
                          color: primaryColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 22))
                ],
              ),
              Divider(height: 20, thickness: 1),
              SizedBox(height: 5),
              itemEachlist(context, 'Quản lý khoa', listKhoa()),
              itemEachlist(
                  context, 'Quản lý phòng', ManageRoom(checkLoading: false)),
              itemEachlist(context, 'Quản lý nhân viên',
                  ManageStaff(checkLoading: false)),
              itemEachlist(
                  context,
                  'Dịch vụ khám',
                  ManageService(
                    checkLoading: false,
                  )),
              itemEachlist(context, 'Tạo thời gian khám bệnh', CreateTimes()),
              itemEachlist(context, 'Dược vật tư', QuanLyVatTu()),
              itemEachlist(context, 'Ngày nghỉ', FreeDayManager())
            ],
          )),
    );
  }

  GestureDetector itemEachlist(BuildContext context, String title, widget) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => widget));
      },
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              title,
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  color: Colors.black87),
            ),
            Icon(
              Icons.arrow_forward_ios_rounded,
              color: Colors.grey,
              size: 20,
            ),
          ],
        ),
      ),
    );
  }
}
