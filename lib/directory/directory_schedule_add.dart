import 'package:flutter/material.dart';

import '../item/box_decoration_container.dart';

class DirectorySchedule extends StatelessWidget {
  final Function(String) onPress;
  String keyData;
  DirectorySchedule({required this.onPress, required this.keyData, super.key});

  Map<String, List<String>> directory = {
    'BeginTimeAM': ['07:00', '08:00', '09:00', '10:00', '11:00', '12:00'],
    'FinishTimeAM': ['07:00', '08:00', '09:00', '10:00', '11:00', '12:00'],
    'BeginTimePM': ['13:00', '14:00', '15:00', '16:00', '17:00', '18:00'],
    'FinishTimePM': ['13:00', '14:00', '15:00', '16:00', '17:00', '18:00'],
    'AmountPatient': ['1', '2', '3', '4', '5'],
    'TimePerPatient': ['05', '10', '15', '20', '25', '30'],
    'Type': ['Khám tại cơ sở', 'Tư vấn từ xa'],
    'HoatChat': [
      'Paracetamol (Acetaminophen)',
      'Ibuprofen',
      'Aspirin',
      'Penicillin',
      'Amoxicillin',
      'Cetirizine'
    ],
    'DuongDung': [
      'Đường uống (Oral)',
      'Đường tiêm (Injection)',
      'Đường bôi ngoài da (Topical)',
      'Đường hít (Inhalation)'
    ],
    'DonViCapPhat': [
      'Community Health Centers',
      'Hospitals',
      'Emergency Rooms',
      'Nursing Homes and Assisted Living Facilities'
    ],
    'TypeFreeday': ['Ngày cụ thể trong năm', 'Ngày cố định hàng tuần'],
    'ThuTrongTuan': [
      'Thứ 2',
      'Thứ 3',
      'Thứ 4',
      'Thứ 5',
      'Thứ 6',
      'Thứ 7',
      'Chủ Nhật'
    ],
  };
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          // backgroundColor: Colors.grey,
          appBar: AppBar(
            iconTheme: IconThemeData(color: Colors.white),
            backgroundColor: Color(0xFF6F9BD4),
            title: const Text(
              'Danh mục',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
          body: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              margin: EdgeInsets.only(top: 10),
              child: ListView.separated(
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        onPress(directory[keyData]![index]);
                        Navigator.pop(context);
                      },
                      child: Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
                        height: 45,
                        decoration: BoxecorationContainer,
                        child: Text(
                          directory[keyData]![index],
                          // 'Values',
                          style: TextStyle(fontSize: 15),
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(),
                  itemCount: directory[keyData]!.length),
            ),
          )),
    );
  }
}

// class DirectorySchedule extends StatefulWidget {
//   const DirectorySchedule({super.key});

//   @override
//   State<DirectorySchedule> createState() => _DirectoryScheduleState();
// }

// class _DirectoryScheduleState extends State<DirectorySchedule> {
//   Map<String, List<String>> directory = {
//     'BeginTimeAM': ['07:00', '08:00', '09:00', '10:00', '11:00', '12:00'],
//     'FinishTimeAM': ['07:00', '08:00', '09:00', '10:00', '11:00', '12:00'],
//     'BeginTimePM': ['13:00', '14:00', '15:00', '16:00', '17:00', '18:00'],
//     'FinishTimePM': ['13:00', '14:00', '15:00', '16:00', '17:00', '18:00'],
//     'TimePerPatient': ['05', '10', '15', '20', '25', '30'],
//     'Type': ['Khám tại cơ sở', 'Tư vấn từ xa'],
//   };
//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//       child: Scaffold(
//           // backgroundColor: Colors.grey,
//           appBar: AppBar(
//             iconTheme: IconThemeData(color: Colors.white),
//             backgroundColor: Color(0xFF6F9BD4),
//             title: const Text(
//               'Danh mục',
//               style: TextStyle(color: Colors.white, fontSize: 20),
//             ),
//           ),
//           body: Container(
//             margin: EdgeInsets.only(top: 10),
//             child: ListView.separated(
//                 itemBuilder: (BuildContext context, int index) {
//                   return GestureDetector(
//                     onTap: () {},
//                     child: Container(
//                       alignment: Alignment.centerLeft,
//                       margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
//                       height: 45,
//                       decoration: BoxDecoration(
//                           color: Colors.white,
//                           borderRadius: BorderRadius.circular(5),
//                           boxShadow: const [
//                             BoxShadow(
//                               color: Colors.black,
//                               offset: Offset(1.0, 1.0),
//                               blurRadius: 0.5,
//                               spreadRadius: 0.0,
//                             ),
//                             BoxShadow(
//                               color: Colors.white,
//                               offset: const Offset(0.0, 0.0),
//                               blurRadius: 0.0,
//                               spreadRadius: 0.0,
//                             ),
//                           ]),
//                       child: Text(
//                         // myList![index],
//                         'Values',
//                         style: TextStyle(fontSize: 15),
//                       ),
//                     ),
//                   );
//                 },
//                 separatorBuilder: (BuildContext context, int index) =>
//                     const Divider(),
//                 itemCount: 5),
//           )),
//     );
//   }
// }
