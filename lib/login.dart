import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:http/http.dart' as http;
import 'package:product_layout_app/homescreen/bottomnavigation.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'componentWidget/styles/CustomColor.dart';

class SharedPrefs {
  static late SharedPreferences _sharedPrefs;

  static final SharedPrefs _instance = SharedPrefs._internal();

  factory SharedPrefs() => _instance;

  SharedPrefs._internal();

  Future<void> init() async {
    _sharedPrefs = await SharedPreferences.getInstance();
  }

  String get token => _sharedPrefs.getString(keyToken) ?? "";

  set token(String value) {
    _sharedPrefs.setString(keyToken, value);
  }
}

const String keyToken = "token";

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool isLogin = false;
  String name = '';
  String password = '';
  var isPassword;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isPassword = true;
    // name = sharedPreferences.getUsername() ?? '';
    // password = sharedPreferences.getToken() ?? '';
  }

  final url =
      "https://staging-apidatlichkham.vncare.vn/patient_api/api/QlhkMobile/v1.3.0/public/auth/gettoken";

  void login(String username, String password) async {
    try {
      Map<String, dynamic> requestPayload = {
        // "USERNAME":username,
        // "PASSWORD":password
        "USERNAME": "01009.QLDLK",
        "PASSWORD": "Gppm2#2018"
      };

      final response = await http.post(
        Uri.parse(url),
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode(requestPayload),
      );

      print(response.statusCode);
      var data = jsonDecode(response.body.toString());

      SharedPrefs().token = data['result']['access_token'];
      print(SharedPrefs().token);
      print(data);
      //print(data['result']['access_token']);
      //print('Login successfully');
      Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => BottomNavigationBarHome()));
      // setState(() {
      //   isLogin = true;
      // });
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          body: Container(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Spacer(flex: 3),
                Expanded(
                  flex: 6,
                  child: SvgPicture.asset('assets/medical-doctor-back.svg'),
                ),
                Spacer(),
                Expanded(
                    flex: 10,
                    child: Form(
                        child: ListView(
                      children: [
                        TextFormField(
                          cursorColor: primaryColor,
                          controller: usernameController,
                          textInputAction: TextInputAction.next,
                          decoration: InputDecoration(
                              hintText: 'Tài khoản',
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: primaryColor)),
                              prefixIcon: Padding(
                                padding: EdgeInsets.all(10),
                                child:
                                    Icon(Icons.person_2, color: primaryColor),
                              )),
                        ),
                        SizedBox(height: 20),
                        TextFormField(
                          cursorColor: primaryColor,
                          controller: passwordController,
                          obscureText: isPassword,
                          textInputAction: TextInputAction.done,
                          validator: validatePassword,
                          decoration: InputDecoration(
                              hintText: 'Mật khẩu',
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: primaryColor)),
                              prefixIcon: Padding(
                                padding: EdgeInsets.all(10),
                                child: Icon(Icons.lock, color: primaryColor),
                              ),
                              suffixIcon: IconButton(
                                  padding: EdgeInsetsDirectional.only(end: 10),
                                  onPressed: () {
                                    setState(() {
                                      isPassword = !isPassword;
                                    });
                                  },
                                  icon: isPassword
                                      ? Icon(
                                          Icons.visibility,
                                          color: primaryColor,
                                        )
                                      : Icon(
                                          Icons.visibility_off,
                                          color: primaryColor,
                                        ))),
                        ),
                        SizedBox(
                          height: 45,
                        ),
                        Hero(
                            tag: 'login',
                            child: ElevatedButton(
                                onPressed: () async {
                                  login(usernameController.text.toString(),
                                      passwordController.text.toString());
                                  //await sharedPreferences.setUsername(usernameController.text.toString());
                                  //await sharedPreferences.setPassword(passwordController.text.toString());
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) =>
                                          BottomNavigationBarHome()));
                                },
                                child: Text('ĐĂNG NHẬP',
                                    style: TextStyle(fontSize: 16)),
                                style: ElevatedButton.styleFrom(
                                    backgroundColor: primaryColor,
                                    fixedSize: Size(300, 40))))
                      ],
                    )))
              ],
            ),
          ),
        ));
  }
}

String? validatePassword(String? value) {
  RegExp regex =
      RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$');
  if (value!.isEmpty) {
    return 'Mật khẩu không được để trống.';
  } else {
    if (!regex.hasMatch(value) && (value.length) >= 8) {
      return 'Mật khẩu phải chứa chữ hoa, chữ thường, chữ số và ký tự đặc biệt.';
    } else if (value.length < 8) {
      return 'Mật khẩu tối thiểu 8 kí tự';
    } else if (!regex.hasMatch(value) && (value.length) < 8) {
      return 'Mật khẩu phải chứa chữ hoa, chữ thường, chữ số và ký tự đặc biệt, tối thiểu 8 kí tự';
    } else {
      return null;
    }
  }
}
