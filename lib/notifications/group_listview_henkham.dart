// import 'dart:convert';
// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';
// import 'package:product_layout_app/model/notifiModel.dart';
// import '../login.dart';
// import 'package:http/http.dart' as http;


// class groupDate extends StatefulWidget {
//   const groupDate({super.key});

//   @override
//   State<groupDate> createState() => _groupDateState();
// }

// class _groupDateState extends State<groupDate> {
//   String token = SharedPrefs().token;

//   final notifiController = TextEditingController();

//   ScrollController _scrollController = new ScrollController();


//   static DateTime returnDateAndTimeFormat(String time) {
//     //var dt = DateTime.fromMicrosecondsSinceEpoch(int.parse(time.toString()));
//     var dt = DateFormat("dd/MM/yyyy HH:mm:ss").parse(time.toString());
//     var originalDate = DateFormat('dd/MM/yyyy').format(dt);
//     return DateTime(dt.day, dt.month, dt.year);
//   }

//   static String returnHoursAndMinutes(String time){
//     var hourMinutes = DateFormat("dd/MM/yyyy HH:mm:ss").parse(time);
//     String result = DateFormat("HH:mm").format(hourMinutes);
//     return result;
//   }

//   static String groupMessageDateAndTime(String time) {
//     //var dt = DateTime.fromMicrosecondsSinceEpoch(int.parse(time.toString()));
//     var dt = DateFormat("dd/MM/yyyy HH:mm:ss").parse(time.toString());
//     var originalDate = DateFormat('dd/MM/yyyy').format(dt);

//     final todayDate = DateTime.now();

//     final today = DateTime(todayDate.day, todayDate.month, todayDate.year);
//     final yesterday =
//         DateTime(todayDate.day-1, todayDate.month, todayDate.year);
//     String difference = '';
//     final aDate = DateTime(dt.day, dt.month, dt.year);

//     if (aDate == today) {
//       difference = "Hôm nay";
//     } else if (aDate == yesterday) {
//       difference = "Hôm qua";
//     } else {
//       difference = DateFormat('dd/MM/yyyy').format(dt).toString();
//     }

//     return difference;
//   }

//   Future<Notifications> getApinotification() async{
//     final url = 'https://staging-apidatlichkham.vncare.vn/patient_api/api/QlhkMobile/v1.3.0/business-service/danhSachThongBao?loaiThongBao=1&trang=1&soDong=10';
//     Map<String, String>  headers = {
//         'Accept-Language': 'vi',
//         'Content-Type': 'application/json',
//         'Authorization': 'Bearer $token',
//     };
    
//     final response = await http.get(Uri.parse(url), headers: headers);

//     print(utf8.decode(response.bodyBytes));
//     if(response.statusCode == 200){
//       var action = json.decode(utf8.decode(response.bodyBytes).toString());
      
//       return Notifications.fromJson(action);
      
//     }
//     else{
//       throw Exception('Failed to load data');
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.transparent,
//       // body: SafeArea(
//       body: Padding(
//         padding: const EdgeInsets.all(0),
//         child: Column(
//           children: [
//             Expanded(
//               child: FutureBuilder<Notifications>(
//                 future: getApinotification(),
//                 builder: (context , snapshot){
//                   if(snapshot.hasData){
//                     return ListView.separated(
//                       padding: EdgeInsets.only(left: 15, right: 15),
//                       separatorBuilder: (context, index) => const SizedBox(height: 0),
//                       controller: _scrollController,
//                       shrinkWrap: true,
//                       physics: const ClampingScrollPhysics(), // ← can't
//                       itemCount: snapshot.data!.result!.danhSach!.length,
//                       itemBuilder: (context, index) {
//                         // bool isSameDate = false;
//                         // String? newDate = '';

//                         // final DateTime date = returnDateAndTimeFormat(snapshot.data!.result!.danhSach![index].createDate.toString());

//                         // if (index == 0 && snapshot.data!.result!.danhSach!.length == 1) {
//                         //   newDate =
//                         //       groupMessageDateAndTime(snapshot.data!.result!.danhSach![index].createDate.toString())
//                         //           .toString();
//                         // } else if (index == snapshot.data!.result!.danhSach!.length - 1) {
//                         //   newDate =
//                         //       groupMessageDateAndTime(snapshot.data!.result!.danhSach![index].createDate.toString())
//                         //           .toString();
//                         // } else {
//                         //   final DateTime date =
//                         //       returnDateAndTimeFormat(snapshot.data!.result!.danhSach![index].createDate.toString());
//                         //   final DateTime prevDate = returnDateAndTimeFormat(
//                         //       snapshot.data!.result!.danhSach![index+1].createDate.toString());
//                         //   isSameDate = date.isAtSameMomentAs(prevDate);

//                         //   if (kDebugMode) {
//                         //     print("$date $prevDate $isSameDate");
//                         //   }
//                         //   newDate = isSameDate
//                         //       ? ''
//                         //       : groupMessageDateAndTime(
//                         //               snapshot.data!.result!.danhSach![index-1].createDate.toString())
//                         //           .toString();
//                         // }

//                         return Container(
//                           child: Column(
//                             children: [
//                               //if (newDate.isNotEmpty)
//                                 Align(
//                                   alignment: Alignment.topLeft,
//                                     child: Container(
//                                         decoration: BoxDecoration(
//                                             color: Colors.transparent,
//                                             //borderRadius: BorderRadius.circular(20)
//                                             ),
//                                         child: Padding(
//                                           padding: const EdgeInsets.only(left: 10, top: 5, bottom: 5),
//                                           child: Text(
//                                             '',
//                                             //newDate,
//                                             //style: TextStyle(color: Colors.black, fontSize: 14, fontWeight: FontWeight.w700),
//                                           ),
//                                         ))),
//                               Padding(
//                                 padding: EdgeInsets.all(0),
//                                 //rheight: 117,
//                                 child: Stack(
//                                   children: [
//                                     Container(
//                                       padding:
//                                           EdgeInsets.only(left: 22, right: 10, top: 30, bottom: 5),
//                                           height: 115,
//                                       decoration: BoxDecoration(
//                                         borderRadius:
//                                             BorderRadius.all(Radius.circular(10)),
//                                         color: Colors.white,
//                                         boxShadow: [
//                                               BoxShadow(
                                                
//                                                 color: Colors.black54,
//                                                 blurRadius: 5, //độ mềm
//                                                 spreadRadius: 0, //mở rộng
//                                                 offset: Offset(
//                                                   0.0, 
//                                                   5.0, 
//                                                   )
//                                               )
//                                             ]
//                                       ),
//                                       child: Column(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                         children: [
//                                           Row(
//                                             children: [
//                                               Text(
//                                                 'Bệnh nhân',
//                                                 style: TextStyle(
//                                                     color: Color(0xFF535858),
//                                                     fontSize: 12),
//                                               ),
//                                               SizedBox(width: 35),
//                                               Text(
//                                                 snapshot.data!.result!.danhSach![index].data!.tenBenhNhan.toString(),
//                                                 style: TextStyle(
//                                                     color: Color(0xFF444444),
//                                                     fontSize: 11,
//                                                     fontWeight: FontWeight.w500),
//                                               )
//                                             ],
//                                           ),
//                                           Row(
//                                             mainAxisAlignment: MainAxisAlignment.start,
//                                             children: [
//                                               Text(
//                                                 'Phòng khám',
//                                                 style: TextStyle(
//                                                     color: Color(0xFF535858),
//                                                     fontSize: 12),
//                                               ),
//                                               SizedBox(width: 24),
//                                               Text(
//                                                 snapshot.data!.result!.danhSach![index].data!.tenPhongKham.toString(),
//                                                 style: TextStyle(
//                                                     color: Color(0xFF444444),
//                                                     fontSize: 11,
//                                                     fontWeight: FontWeight.w500),
//                                               )
//                                             ],
//                                           ),
//                                           Row(
//                                             mainAxisAlignment: MainAxisAlignment.start,
//                                             children: [
//                                               Text(
//                                                 'Lý do khám',
//                                                 style: TextStyle(
//                                                     color: Color(0xFF535858),
//                                                     fontSize: 12),
//                                               ),
//                                               SizedBox(width: 30),
//                                               Text(
//                                                 snapshot.data!.result!.danhSach![index].data!.yeuCauKham.toString(),
//                                                 style: TextStyle(
//                                                     color: Color(0xFF444444),
//                                                     fontSize: 11,
//                                                     fontWeight: FontWeight.w500),
//                                               )
//                                             ],
//                                           ),
//                                           Row(
//                                             children: [
//                                               Text(
//                                                 'Thời gian',
//                                                 style: TextStyle(
//                                                     color: Color(0xFF535858),
//                                                     fontSize: 12),
//                                               ),
//                                               SizedBox(width: 42),
//                                               Text(
//                                                 snapshot.data!.result!.danhSach![index].data!.ngayGioHen.toString(),
//                                                 style: TextStyle(
//                                                     color: Color(0xFF444444),
//                                                     fontSize: 11,
//                                                     fontWeight: FontWeight.w500),
//                                               )
//                                             ],
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                     //SizedBox(height: 10),
//                                     Container(
//                                       padding: EdgeInsets.symmetric(horizontal: 20),
//                                       height: 23,
//                                       decoration: BoxDecoration(
//                                         //borderRadius: BorderRadius.all(Radius.circular(10)),
//                                         borderRadius: BorderRadius.only(
//                                             topLeft: Radius.circular(10),
//                                             topRight: Radius.circular(10)),
//                                         color: Color(0xFF6f9bd4),
//                                       ),
//                                       child: Row(
//                                         children: [
//                                           //Padding(padding: EdgeInsets.symmetric(vertical: 10)),
//                                           Text(
//                                             snapshot.data!.result!.danhSach![index].data!.title.toString(),
//                                             style: TextStyle(
//                                               color: Colors.white,
//                                               fontSize: 14,
//                                               fontWeight: FontWeight.normal,
//                                             ),
//                                           ),
//                                           Spacer(),
//                                           Text(
//                                             returnHoursAndMinutes(snapshot.data!.result!.danhSach![index].createDate.toString()),
//                                             style: TextStyle(
//                                               color: Colors.white,
//                                               fontSize: 12,
//                                               fontWeight: FontWeight.normal,
//                                             ),
//                                           ),
//                                         ],
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                             ],
//                           ),
//                         );
//                       }
//                       );
//                   }else{
//                     return Text('Loading');
//                   }
//                 }
//               )
//             )
//           ],
//         ),
//       )
          
            
     
//     );
//   }
// }
