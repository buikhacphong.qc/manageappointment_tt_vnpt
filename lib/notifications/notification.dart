import 'dart:convert';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:product_layout_app/notifications/test_all.dart';
import 'package:product_layout_app/notifications/test_hen_kham.dart';
import 'package:product_layout_app/notifications/test_online.dart';
import '../login.dart';
import '../model/notifiModel.dart';
import 'package:http/http.dart' as http;

class notifications extends StatelessWidget {
  notifications({super.key});
  String token = SharedPrefs().token;

  // List<DanhSach> listthongbao = [];
  // void notification() async{
  //   String apiUrl =
  //       'https://staging-apidatlichkham.vncare.vn/patient_api/api/QlhkMobile/v1.3.0/business-service/';
  //   String param =
  //       'danhSachThongBao?loaiThongBao=1&trang=1&soDong=10';
  //   final result = await BaseApi().get(apiUrl, param);
  //   final kqua = listNotificationsFromJson(result);
  //   // setState(() {
  //   //   DanhSach danhsach = kqua.danhsach;
  //   //   listthongbao = result.danhSach;
  //   // });
  // }

  Future<Notifications> getApinotification() async{
    final url = 'https://staging-apidatlichkham.vncare.vn/patient_api/api/QlhkMobile/v1.3.0/business-service/danhSachThongBao?loaiThongBao=1&trang=1&soDong=10';
    Map<String, String>  headers = {
        'Accept-Language': 'vi',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
    };
    
    final response = await http.get(Uri.parse(url), headers: headers);

    print(utf8.decode(response.bodyBytes));
    if(response.statusCode == 200){
      var action = json.decode(utf8.decode(response.bodyBytes).toString());
      // return action.map((e) => Notifications.fromJson(e)).toList();
      //final List listNotifications = action['result']['danhSach'];
      // List<DanhSach> data = listNotifications
      //       .map((contactRaw) => DanhSach.fromJson(contactRaw))
      //       .toList();
      return Notifications.fromJson(action);
      //print(listNotifications);
      //print(data);
    }
    else{
      throw Exception('Failed to load data');
    }
  }
  
  @override
  Widget build(BuildContext context) {
    getApinotification();
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(Icons.chevron_left),
          ),

          centerTitle: true, //newkt
          title: Text(
            'Thông báo',
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w600,
            ),
          ),
          backgroundColor: Color(0xFF6f9bd4),
        ),
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/image 1.png'),
              fit: BoxFit.cover,
            ),
            color: Colors.white.withOpacity(0.8),
          ),
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
            child: Container(
              padding: EdgeInsets.all(8.0),
              child: Column(
                children: [
                  Container(
                    width: 280,
                    height: 40,
                    decoration: BoxDecoration(
                        //color: Colors.grey[300],
                        color: Colors.white.withOpacity(0.5), //mờ trong suốt
                        borderRadius: BorderRadius.circular(22.0)),
                    child: TabBar(
                      indicator: BoxDecoration(
                        color: Color(0xFF6f9bd4),
                        borderRadius: BorderRadius.circular(22.0),
                      ),
                      labelColor: Colors.white,
                      labelStyle:
                          TextStyle(fontSize: 13, fontWeight: FontWeight.w500),
                      unselectedLabelColor: Colors.black,
                      tabs: const [
                        Tab(
                          text: 'Tất cả',
                        ),
                        Tab(
                          text: 'Hẹn khám',
                        ),
                        Tab(
                          text: 'Tư vấn',
                        ),
                      ],
                    ),
                  ),
                  
                  Expanded(
                      child: TabBarView(
                    children: [
                      allDate(),
                      groupDate(),
                      onlineDate(),                     
                    ],
                  ))
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
