import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:product_layout_app/model/lich.dart';
import 'package:grouped_list/grouped_list.dart';

import 'item_noti.dart';


class allDate extends StatefulWidget {
  const allDate({super.key});

  @override
  State<allDate> createState() => _allDateState();
}

class _allDateState extends State<allDate> {
  //notification controller
  final notifiController = TextEditingController();

  // scroll controller
  ScrollController _scrollController = new ScrollController();

  static List<lich> main_hen = [
    // lich(
    //     '10 phút trước',
    //     DateTime.now().microsecondsSinceEpoch,
    //     'Nguyễn Thị Ánh Nguyệt(viện phí)',
    //     'Ngoại tổng hợp',
    //     'Ngoại tổng hợp',
    //     '07/07/2021 9:00'),
    lich(
        '2',
        'Lịch tư vấn online',
        '11:53',
        //DateTime.now().microsecondsSinceEpoch,
        '01/06/2021',
        'Nguyễn Thị Ánh Nguyệt',
        'Ngoại tổng hợp',
        'Ngoại tổng hợp',
        '07/07/2021 9:00'),
    lich(
      '1',
      'Lịch hẹn khám',
        '12:30',
        //DateTime.now().microsecondsSinceEpoch,
        '03/05/2021',
        'Nguyễn Thị Ánh Nguyệt',
        'Ngoại tổng hợp',
        'Ngoại tổng hợp',
        '07/07/2021 9:00'),
    // lich(
    //     '10:11:20',
    //     DateTime(DateTime.now().year, DateTime.now().month,
    //             DateTime.now().day - 1, 02, 02)
    //         .microsecondsSinceEpoch,
    //     'Nguyễn Thị Ánh Nguyệt(viện phí)',
    //     'Ngoại tổng hợp',
    //     'Ngoại tổng hợp',
    //     '07/07/2021 9:00'),
    // lich(
    //     '10:11:20',
    //     DateTime(2023, 07, 11, 11, 10).microsecondsSinceEpoch,
    //     'Nguyễn Thị Ánh Nguyệt(viện phí)',
    //     'Ngoại tổng hợp',
    //     'Ngoại tổng hợp',
    //     '07/07/2021 9:00'),
    lich(
      '1',
        'Lịch hẹn khám',
        '11:20',
        //DateTime(2023, 07, 11, 11, 10).microsecondsSinceEpoch,
        '29/04/2021',
        'Nguyễn Thị Ánh Nguyệt',
        'Ngoại tổng hợp',
        'Ngoại tổng hợp',
        '07/07/2021 9:00'),
    //  lich(
    //     '10:11:20',
    //     DateTime(2023, 07, 07, 11, 10).microsecondsSinceEpoch,
    //     'Nguyễn Thị Ánh Nguyệt(viện phí)',
    //     'Ngoại tổng hợp',
    //     'Ngoại tổng hợp',
    //     '07/07/2021 9:00'),
  ];

  List<lich> display = List.from(main_hen);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      // body: SafeArea(
      body: GroupedListView<lich, String>(
        elements: main_hen, 
        groupBy: (lich g) => g.day,
        itemBuilder: (BuildContext context, lich g) 
          => GestureDetector(
            child: itemNotifi(
                    tenbenhNhan: '${g.name}',
                    tenphongKham: '${g.phong}',
                    lydo: '${g.ly_do}',
                    thoigianhen: '${g.time}',
                    loailich:'${g.category}',
                    thoigiandat: '${g.khoang}',
                  ),
          ),
        groupSeparatorBuilder: (String name) => 
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Text(name, style: TextStyle(fontWeight: FontWeight.w500),),
          )
      )
              
    );
  }
}
