import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:product_layout_app/Container/DetailManageCaculator.dart';
import 'package:product_layout_app/componentWidget/loading/loading.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:product_layout_app/model/departmentModel.dart';
import 'package:product_layout_app/model/notifiModel.dart';
import 'package:product_layout_app/notifications/item_noti.dart';
import '../login.dart';
import 'package:http/http.dart' as http;


class groupDate extends StatefulWidget {
  const groupDate({super.key});

  @override
  State<groupDate> createState() => _groupDateState();
}

class _groupDateState extends State<groupDate> {
  String token = SharedPrefs().token;

  final notifiController = TextEditingController();

  ScrollController _scrollController = new ScrollController();


  // static DateTime returnDateAndTimeFormat(String time) {
  //   //var dt = DateTime.fromMicrosecondsSinceEpoch(int.parse(time.toString()));
  //   var dt = DateFormat("dd/MM/yyyy HH:mm:ss").parse(time.toString());
  //   var originalDate = DateFormat('dd/MM/yyyy').format(dt);
  //   return DateTime(dt.day, dt.month, dt.year);
  // }
  static String returnDateAndTimeFormat(String time) {
    //var dt = DateTime.fromMicrosecondsSinceEpoch(int.parse(time.toString()));
    DateTime dateTime = DateFormat("MM/dd/yyyy HH:mm:ss").parse(time);
    String formattedDate = DateFormat("MM/dd/yyyy").format(dateTime);
    return formattedDate;
  }

  static String returnGroupTime(String time){
    var hourMinutes = DateFormat("dd/MM/yyyy HH:mm:ss").parse(time);
    String result = DateFormat("HH:mm").format(hourMinutes);
    return result;
  }



  Future<Notifications> getApinotification() async{
    final url = 'https://staging-apidatlichkham.vncare.vn/patient_api/api/QlhkMobile/v1.3.0/business-service/danhSachThongBao?loaiThongBao=1&trang=1&soDong=10';
    Map<String, String>  headers = {
        'Accept-Language': 'vi',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
    };
    
    final response = await http.get(Uri.parse(url), headers: headers);

    print(utf8.decode(response.bodyBytes));
    if(response.statusCode == 200){
      var action = json.decode(utf8.decode(response.bodyBytes).toString());
      
      return Notifications.fromJson(action);
      
    }
    else{
      throw Exception('Failed to load data');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      // body: SafeArea(
      body: 
      Padding(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Column(
          children: [
            Expanded(
              child: FutureBuilder<Notifications>(
                future: getApinotification(),
                builder: (context , snapshot){
                  if(snapshot.hasData){
                    return GroupedListView<DanhSach, String>(
                      elements: snapshot.data!.result!.danhSach!, 
                      groupBy: (DanhSach g) => returnDateAndTimeFormat(g.createDate!).toString(),
                      itemBuilder: (BuildContext context, DanhSach g) 
                        => GestureDetector(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => DetailManageCaculator(idHenKham:g.notificationId.toString())));
                          },
                          child: itemNotifi(
                                  //tenbenhNhan: snapshot.data!.result!.danhSach![index].data!.tenBenhNhan.toString(),
                                  tenbenhNhan: g.data!.tenBenhNhan.toString(),
                                  tenphongKham: g.data!.tenPhongKham.toString(),
                                  lydo: g.data!.yeuCauKham.toString(),
                                  thoigianhen: g.data!.ngayGioHen.toString(),
                                  loailich:g.data!.title.toString(),
                                  thoigiandat: returnGroupTime(g.createDate!),
                                ),
                        ),
                      groupSeparatorBuilder: (String name) => 
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                          child: Text(name, style: TextStyle(fontWeight: FontWeight.w500),),
                        )
                    );
                  }else{
                    return Loader();
                  }
                }
              )
            )
          ],
        ),
      )
          
              
    );
  }
}
      

