// import 'dart:io';
import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:product_layout_app/model/schedule_detail.dart';
// import 'package:week2/directory.dart';
// import 'package:week2/edit_schedule2.dart';
// import 'package:week2/line_input.dart';


import 'edit_schedule2.dart';
import 'line_input.dart';

class EditSchedule extends StatefulWidget {
  const EditSchedule({super.key});

  @override
  State<EditSchedule> createState() => _EditScheduleState();
}



class _EditScheduleState extends State<EditSchedule> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color(0xFFEEEEEE),
        appBar: AppBar(
          backgroundColor: Color(0xFF6F9BD4),
          title: const Text('Sửa lịch hẹn khám tại CSYT'),
        ),
        body: Column(
          children: [
            Expanded(
              flex: 8,
              child: Container(
                height: 460,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      StateInfo(),
                      PatientInfo(),
                    ],
                  ),
                ),
              ),
            ),
            //button continue
            Expanded(
              flex: 1,
              child: Container(
                height: 40,
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.fromLTRB(15, 5, 15, 5),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => EditSchedule2()));
                  },
                  child: Card(
                    color: Color(0xFF6F9BD4),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: const Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.arrow_circle_right_outlined,
                          color: Colors.white,
                        ),
                        Text(
                          ' Tiếp tục',
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ));
  }
}

class StateInfo extends StatefulWidget {
  final List<String> listItem = [];

  @override
  State<StateInfo> createState() => _StateInfoState();
}

class _StateInfoState extends State<StateInfo> {
  @override
  Widget build(BuildContext context) {
    return Container(
      // padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
      child: Card(
        child: Column(
          children: [
            Row(
              children: [
                Container(
                  width: 150,
                  height: 21,
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.fromLTRB(10, 5, 0, 15),
                  child: const Text('Trạng thái'),
                ),
                Container(
                  width: 165,
                  height: 21,
                  child: const Text('Đặt lịch thành công'),
                )
              ],
            ),
            Row(
              children: [
                Container(
                  width: 150,
                  height: 21,
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.fromLTRB(10, 5, 0, 15),
                  child: const Text('Trạng thái đến khám'),
                ),
                Container(
                  width: 165,
                  height: 21,
                  child: const Text(
                    'Đã đến khám',
                    style: TextStyle(color: Colors.blue),
                  ),
                )
              ],
            ),
            const LineInput(
              title: "Kênh đăng ký (*)",
              type: "dropbutton",
              index: 1,
            ),
            const LineInput(
              title: "Đối tượng (*)",
              type: "dropbutton",
              index: 2,
            ),
          ],
        ),
      ),
    );
  }
}

class PatientInfo extends StatefulWidget {
  final List<String> listItem = [];
  @override
  State<PatientInfo> createState() => _PatientInfoState();
}

class _PatientInfoState extends State<PatientInfo> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(0),
      child: const Card(
        child: Column(
          children: [
            LineInput(
              title: "Họ tên (*)",
              type: "input",
              index: 14,
            ),
            LineInput(
              title: "Ngày sinh (*)",
              type: "dropbutton",
              index: 15,
            ),
            LineInput(
              title: "Nghề nghiệp (*)",
              type: "dropbutton",
              index: 3,
            ),
            LineInput(
              title: "Giới tính (*)",
              type: "dropbutton",
              index: 4,
            ),
            LineInput(
              title: "Dân tộc (*)",
              type: "dropbutton",
              index: 5,
            ),
            LineInput(
              title: "Quốc tịch (*)",
              type: "dropbutton",
              index: 6,
            ),
            LineInput(
              title: "Tỉnh khai sinh (*)",
              type: "dropbutton",
              index: 7,
            ),
            LineInput(
              title: "Số điện thoại (*)",
              type: "input",
              index: 16,
            ),
            LineInput(
              title: "Chứng minh thư",
              type: "input",
              index: 17,
            ),
            LineInput(
              title: "Tỉnh/Thành phố (*)",
              type: "dropbutton",
              index: 8,
            ),
            LineInput(
              title: "Huyện (*)",
              type: "dropbutton",
              index: 9,
            ),
            LineInput(
              title: "Xã (*)",
              type: "dropbutton",
              index: 10,
            ),
            LineInput(
              title: "Số nhà (*)",
              type: "input",
              index: 20,
            ),
          ],
        ),
      ),
    );
  }
}
