import 'package:flutter/material.dart';

// import 'dialogAlert.dart';
// import 'package:week2/dialogAlert.dart';

class Directorys extends StatefulWidget {
  const Directorys({required this.index, super.key});
  final int index;

  @override
  State<Directorys> createState() => _DirectorysState();
}

void getDirectoryByKey(String key) async {
  String token;
  String url = "";
}

class _DirectorysState extends State<Directorys> {
  // List<bool> myLists = List.filled(7, false);
  String itemSelected = '';
  static Map<int, List<String>> danhmuc = {
    1: ['Qua VNCare', 'Trực tiếp'],
    2: ['Viện phí', 'Bảo hiểm y tế', 'Bảo hiểm xã hội'],
    3: ['Tri thức', 'Sinh viên', 'Kỹ sư', 'Lao động phổ thông', 'Giảng viên'],
    4: [
      'Nam',
      'Nữ',
    ],
    5: ['Kinh', 'Mường', 'Mông', 'Dao ', 'Ê đê'],
    6: ['Việt Nam', 'Hoa Kỳ', 'Đài Loan', 'Nhật Bản ', 'Khác'],
    7: ['Thái Bình', 'Hà Nội', 'Lào Cai', 'Nam Định ', 'Hải Dương', 'Khác'],
    8: ['Thái Bình', 'Hà Nội', 'Lào Cai', 'Nam Định ', 'Hải Dương', 'Khác'],
    9: [
      'Tiền Hải',
      'Kiến Xương',
      'Thái Thụy',
      'Quỳnh Phụ ',
      'Đông Hưng',
      'Hưng Hà',
      'Khác'
    ],
    10: [
      'Quỳnh Trang',
      'Quỳnh Xá',
      'Quỳnh Hưng',
      'Quỳnh Hải ',
      'Đông Hải',
      'Khác'
    ],
    11: ['Khám sản', 'Khám nội', 'Khám ngoại', 'Khám lung tung'],
    12: ['Phòng khám 1', 'Phòng khám 2', 'Phòng khám 3', 'Phòng thường'],
    13: ['BS.Minh Phương', 'BS.Mạnh Hùng', 'BS.Đình Quế']
  };

  @override
  Widget build(BuildContext context) {
    List<String>? myList = danhmuc[widget.index];
    // void callBackData() {
    //   int count = 0;
    //   for (int tmp = 0; tmp < myLists.length; tmp++) {
    //     if (myLists[tmp] == true) {
    //       count++;
    //       itemSelected = myList![tmp];
    //     }
    //   }
    //   if (count > 1) {
    //     showDataAlert(context, "Chỉ được chọn một ${widget.title}");
    //   } else if (count < 1) {
    //     showDataAlert(
    //         context, "Vui lòng chọn một ${widget.title} trước khi tiếp tục");
    //   } else {
    //     Navigator.pop(context, itemSelected);
    //   }
    // }

    return SafeArea(
      child: Scaffold(
          // backgroundColor: Colors.grey,
          appBar: AppBar(
            title: Text("Danh muc"),
          ),
          body: Container(
            margin: EdgeInsets.only(top: 10),
            child: ListView.separated(
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.pop(context, myList[index]);
                    },
                    child: Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.fromLTRB(8, 0, 8, 0),
                      height: 45,
                      // color: Colors.white,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5),
                          boxShadow: const [
                            BoxShadow(
                              color: Colors.black,
                              offset: Offset(1.0, 1.0),
                              blurRadius: 0.5,
                              spreadRadius: 0.0,
                            ),
                            BoxShadow(
                              color: Colors.white,
                              offset: const Offset(0.0, 0.0),
                              blurRadius: 0.0,
                              spreadRadius: 0.0,
                            ),
                          ]),
                      // margin: EdgeInsets.all(10),
                      child: Text(
                        myList![index],
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                  );
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const Divider(),
                itemCount: danhmuc[widget.index]!.length),
          )),
    );
  }
}

// void showDataAlert(context, String t) {
//   showDialog(
//       context: context,
//       builder: (context) {
//         return Container(
//           child: Container(
//               decoration: BoxDecoration(
//                 borderRadius: BorderRadius.circular(20),
//               ),
//               child: MyWidget(
//                 title: t,
//               )),
//         );
//       });
// }
