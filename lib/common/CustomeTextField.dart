import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final String keyText;
  final Icon icon;
  final VoidCallback onpress;
  final bool checkRangBuoc;
  final int left;
  final int right;
  final FocusNode focusNode;
  final TextEditingController textEditingController;
  CustomTextField(
      {Key? key,
      required this.keyText,
      this.icon = const Icon(
        Icons.clear,
        size: 14,
      ),
      this.checkRangBuoc = false,
      this.left = 2,
      this.right = 3,
      required this.focusNode,
      required this.textEditingController,
      required this.onpress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 5),
      child: Row(
        children: [
          Expanded(
            flex: left,
            child: Container(
              alignment: Alignment.centerLeft,
              height: 30,
              child: checkRangBuoc == true
                  ? Row(
                      children: [
                        Text(
                          keyText,
                          style: TextStyle(fontSize: 14),
                        ),
                        Text(
                          ' (*)',
                          style: TextStyle(color: Colors.red),
                        )
                      ],
                    )
                  : Text(
                      keyText,
                      style: TextStyle(fontSize: 14),
                    ),
            ),
          ),
          Expanded(
            flex: right,
            child: Container(
                height: 30,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey, width: 1.5),
                    borderRadius: BorderRadius.all(Radius.circular(8))),
                child: TextFormField(
                  // maxLines: 3,
                  style: TextStyle(
                    fontSize: 14,
                  ),
                  focusNode: focusNode,
                  controller: textEditingController,
                  // validator: (value) {
                  //   if (checkRangBuoc == true && value == '') {
                  //     print('Vui long nhap gia tri');
                  //   }
                  // },
                  decoration: InputDecoration(
                      suffixIcon: focusNode.hasFocus == false &&
                              textEditingController.text.isNotEmpty
                          ? GestureDetector(
                              child: icon,
                              onTap: onpress,
                            )
                          : null,
                      border: InputBorder.none,
                      focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(width: 0.5, color: Colors.grey)),
                      enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          borderSide:
                              BorderSide(color: Colors.white, width: 0.5))),
                )),
          )
        ],
      ),
    );
  }
  // void deleteText() {
  //   textEditingController.clear();
  // }
}
