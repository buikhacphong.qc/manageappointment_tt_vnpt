import 'package:flutter/material.dart';

class CustomTextElevated extends StatelessWidget {
  final String title;
  final IconData icon;
  final VoidCallback onpressed;
  final String text;
  final bool checkRangBuoc;
  const CustomTextElevated({
    Key? key,
    required this.onpressed,
    required this.text,
    required this.title,
    this.checkRangBuoc = false,
    this.icon = Icons.arrow_drop_down_sharp,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 5),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        Expanded(
          child: Container(
            height: 30,
            alignment: Alignment.centerLeft,
            child: checkRangBuoc == false
                ? Text(
                    title,
                    style: TextStyle(
                      fontSize: 14,
                    ),
                  )
                : Row(
                    children: [
                      Text(
                        title,
                        style: TextStyle(fontSize: 14),
                      ),
                      Text(
                        ' (*)',
                        style: TextStyle(color: Colors.red),
                      )
                    ],
                  ),
          ),
          flex: 2,
        ),
        Expanded(
          flex: 3,
          child: GestureDetector(
            onTap: onpressed,
            child: Container(
                height: 30,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: Colors.grey, width: 1.5),
                    borderRadius: BorderRadius.all(Radius.circular(8))),
                child: Padding(
                  padding: const EdgeInsets.only(left: 12, right: 17),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        text,
                        style: TextStyle(
                          fontSize: 14,
                        ),
                      ),
                      Icon(
                        icon,
                        size: 14,
                        color: Color(0xFF444444),
                      )
                    ],
                  ),
                )),
          ),
        ),
      ]),
    );
  }
}
