import 'package:flutter/cupertino.dart';

class CustomTitleValue extends StatelessWidget {
  final String key1;
  final String value1;
  final String trangThaiDenKham;

  const CustomTitleValue({
    super.key,
    required this.key1,
    required this.value1,
    this.trangThaiDenKham = '',
  });

  @override
  Widget build(BuildContext context) {
    Color colorValue;
    Color colorTitle;
    if (trangThaiDenKham == '2') {
      // textColor = Color(0xFF5CBBB8);
      colorValue = Color(0xFFB43939);
    } else if (trangThaiDenKham == '1') {
      colorValue = Color(0xFF5CBBB8);
    } else {
      colorValue = Color(0xFF535858);
    }

    if (key1 == 'Số thẻ BHYT (*)') {
      colorTitle = Color(0xFFB43939);
      colorValue = Color(0xFFB43939);
    } else {
      colorTitle = Color(0xFF535858);
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          child: Container(
            height: 30,
            child:
                Text(key1, style: TextStyle(fontSize: 14, color: colorTitle)),
          ),
          flex: 1,
        ),
        Expanded(
          child: Container(
            margin: EdgeInsets.only(left: 8),
            height: 30,
            child:
                Text(value1, style: TextStyle(fontSize: 14, color: colorValue)),
          ),
          flex: 3,
        )
      ],
    );
  }
}
