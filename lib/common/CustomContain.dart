import 'package:flutter/material.dart';

class CustomContain extends StatelessWidget {
  final String containerText;
  final double width;
  final VoidCallback onPress;
  final FocusNode focusNode;
  final TextEditingController textEditingController;
  CustomContain({
    Key? key,
    required this.containerText,
    required this.width,
    required this.onPress,
    required this.textEditingController,
    required this.focusNode,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: width,
            height: 15,
            child: Text(
              containerText,
              style: TextStyle(fontSize: 12),
            ),
          ),
          SizedBox(height: 5),
          Container(
              // margin: EdgeInsets.only(right: 5),
              width: width,
              height: 30,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey, width: 0.5),
                  borderRadius: BorderRadius.all(Radius.circular(8))),
              child: TextFormField(
                controller: textEditingController,
                focusNode: focusNode,
                style: TextStyle(
                  fontSize: 14,
                ),
                decoration: InputDecoration(
                    suffixIcon: focusNode.hasFocus == false &&
                            textEditingController.text.isNotEmpty
                        ? GestureDetector(
                            onTap: onPress,
                            child: Icon(
                              Icons.clear,
                              size: 14,
                            ),
                          )
                        : null,
                    border: InputBorder.none,
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(width: 1, color: Colors.grey)),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide:
                            BorderSide(color: Colors.white, width: 1.5))),
              )),
        ]);
  }
}
