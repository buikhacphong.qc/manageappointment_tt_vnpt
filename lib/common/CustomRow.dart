import 'package:flutter/cupertino.dart';

class CustomRow extends StatelessWidget {
  final String key1;
  final String value1;

  const CustomRow({
    super.key,
    required this.key1,
    required this.value1,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          child: Container(
            height: 25,
            child: Text(key1,
                style: TextStyle(fontSize: 11, color: Color(0xFF535858))),
          ),
          flex: 2,
        ),
        Expanded(
          child: Container(
            margin: EdgeInsets.only(left: 8),
            height: 25,
            child: Text(value1,
                style: TextStyle(fontSize: 11, color: Color(0xFF535858))),
          ),
          flex: 4,
        )
      ],
    );
  }
}
