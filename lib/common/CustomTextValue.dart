import 'package:flutter/material.dart';

class CustomTextValue extends StatelessWidget {
  final String key1;
  final String value1;
  final int left;
  final int right;
  CustomTextValue({
    Key? key,
    required this.key1,
    required this.value1,
    this.left = 2,
    this.right = 3,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Container(
            height: 30,
            alignment: Alignment.centerLeft,
            child: Text(key1,
                style: TextStyle(
                  fontSize: 14,
                )),
          ),
          flex: left,
        ),
        Expanded(
          child: Container(
            height: 30,
            alignment: Alignment.centerLeft,
            child: Text(value1,
                style: TextStyle(
                  fontSize: 14,
                )),
          ),
          flex: right,
        )
      ],
    );
  }
}
