import 'package:flutter/material.dart';

import '../common/BaseApiNoToken.dart';
import '../common/CustomAppBar.dart';
import '../item/ContainerManageRoom.dart';
import '../item/ItemManageRoom.dart';
import '../model/listPhongKham.dart';
import 'DetailRoom.dart';

// ignore: must_be_immutable
class ManageRoom extends StatefulWidget {
  bool checkLoading;
  ManageRoom({
    Key? key,
    required this.checkLoading,
  }) : super(key: key);

  @override
  State<ManageRoom> createState() => _ManageRoomState();
}

class _ManageRoomState extends State<ManageRoom> {
  Map<String, String> resultTimKiem = {};
  List<dynamic> trave = [];

  void receiveData(Map<String, String> ketqua) {
    setState(() {
      resultTimKiem = ketqua;
      loadData();
    });
  }

  //nhận data thêm mới
  void receiveDataAdd(ListPhongKham listPhongKham) {
    setState(() {
      trave.add(listPhongKham);
      widget.checkLoading = true;
    });
  }

  @override
  void initState() {
    super.initState();
    widget.checkLoading = false;
    loadData();
  }

  Future<void> loadData() async {
    String apiUrl = 'https://64d2f44d67b2662bf3db88c8.mockapi.io/';
    String param = 'listPhongKham';
    final result = await BaseApiNoToken().get(apiUrl, param);
    List<dynamic> kqua = listPhongKhamFromJson(result);
    if (kqua.length != 0) {
      setState(() {
        trave = kqua;
        widget.checkLoading = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: CustomAppBar(
            text: 'Danh sách phòng khám',
            check: false,
            onPress: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: Container(
          width: width_screen,
          height: MediaQuery.of(context).size.height,
          child: Padding(
            padding: EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 0),
            child: ListView.builder(
                itemCount: 2,
                itemBuilder: (BuildContext context, index) {
                  if (index == 0) {
                    return Card(
                      elevation: 1,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8),
                          side: BorderSide(
                            color: Color.fromARGB(255, 178, 200, 219),
                          )),
                      child: Container(
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(8)),
                          color: Color.fromARGB(255, 178, 200, 219),
                        ),
                        width: MediaQuery.of(context).size.width,
                        height: 170,
                        child: ContainerManageRoom(
                          dataSend: receiveData,
                          senDataAdd: receiveDataAdd,
                        ),
                      ),
                    );
                  } else {
                    return Container(
                      padding:
                          EdgeInsets.only(top: 8, left: 0, right: 0, bottom: 0),
                      child: Column(
                        children: [
                          Visibility(
                            child: CircularProgressIndicator(),
                            visible: !widget.checkLoading,
                          ),
                          Visibility(
                            visible: widget.checkLoading,
                            child: ListView.separated(
                              physics: NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemCount: trave.length,
                              itemBuilder: (BuildContext context, int index) {
                                ListPhongKham listPhongKham = trave[index];
                                return GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => DetailRoom(
                                                  listPhongKham:
                                                      listPhongKham))).then(
                                          (value) {
                                        if (value[1] == 'xoa') {
                                          for (int i = 0;
                                              i < trave.length;
                                              i++) {
                                            ListPhongKham listPhong = trave[i];
                                            if (listPhong.id == value[0]) {
                                              setState(() {
                                                trave.removeAt(i);
                                              });
                                            }
                                          }
                                        } else if (value[1] == 'sua') {
                                          ListPhongKham listPhongTraVe =
                                              value[0];
                                          for (int i = 0;
                                              i < trave.length;
                                              i++) {
                                            ListPhongKham listPhong = trave[i];
                                            if (listPhong.id ==
                                                listPhongTraVe.id) {
                                              setState(() {
                                                listPhong = listPhongTraVe;
                                              });
                                            }
                                          }
                                        }
                                      });
                                    },
                                    child: ItemManageRoom(
                                      maPhong: listPhongKham.maPhong,
                                      tenPhong: listPhongKham.tenPhong,
                                      khoa: listPhongKham.khoa,
                                      trangThai: listPhongKham.checkSuDung,
                                    ));
                                // }
                              },
                              separatorBuilder:
                                  (BuildContext context, int index) {
                                return Divider(
                                  height: 8,
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    );
                  }
                }),
          ),
        ),
      ),
    );
  }
}
