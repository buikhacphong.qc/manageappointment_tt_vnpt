import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:product_layout_app/common/BaseApiNoToken.dart';

import '../common/CustomAppBar.dart';
import '../common/CustomButton.dart';
import '../common/CustomTextCheckbox.dart';
import '../common/CustomeTextElevated.dart';
import '../common/CustomeTextField.dart';
import '../model/listPhongKham.dart';
import 'ListDrop.dart';

class AddEditRoom extends StatefulWidget {
  final ListPhongKham listPhongKham;
  final bool checkAddEdit;
  const AddEditRoom(
      {Key? key, required this.checkAddEdit, required this.listPhongKham})
      : super(key: key);

  @override
  State<AddEditRoom> createState() => _AddEditRoomState();
}

class _AddEditRoomState extends State<AddEditRoom> {
  final FocusNode _focusMaPhong = FocusNode();
  final FocusNode _focusMaPhongBHYT = FocusNode();
  final FocusNode _focusSoPhong = FocusNode();
  final FocusNode _focusTenPhong = FocusNode();
  final FocusNode _focusMaDauDoc = FocusNode();
  final FocusNode _focusGhiChu = FocusNode();
  final FocusNode _focusQRCode = FocusNode();
  final FocusNode _focusDiaChiPhong = FocusNode();
  final FocusNode _focusMaMau = FocusNode();
  final FocusNode _focusSTT = FocusNode();
  final FocusNode _focusSTTPhong = FocusNode();
  final FocusNode _focusChuyenKhoa = FocusNode();

  final TextEditingController textMaPhong = TextEditingController();
  final TextEditingController textMaPhongBHYT = TextEditingController();
  final TextEditingController textSoPhong = TextEditingController();
  final TextEditingController textTenPhong = TextEditingController();
  final TextEditingController textMaDauDoc = TextEditingController();
  final TextEditingController textGhiChu = TextEditingController();
  final TextEditingController textQRCode = TextEditingController();
  final TextEditingController textDiaChiPhong = TextEditingController();
  final TextEditingController textMaMau = TextEditingController();
  final TextEditingController textSTT = TextEditingController();
  final TextEditingController textSTTPhong = TextEditingController();
  final TextEditingController textChuyenKhoa = TextEditingController();
  String textKhoa = '';
  String textKhoaNoiTru = '';
  String textChuyenKhoaDrop = '';
  String textLoaiPhong = '';
  String textCongKham = '';

  @override
  void initState() {
    super.initState();
    if (widget.checkAddEdit == false) {
      textMaPhong.text = widget.listPhongKham.maPhong;
      textMaPhongBHYT.text = widget.listPhongKham.maPhongBhyt;
      textSoPhong.text = widget.listPhongKham.soPhong;
      textTenPhong.text = widget.listPhongKham.tenPhong;
      textMaDauDoc.text = widget.listPhongKham.maDauDoc;
      textGhiChu.text = widget.listPhongKham.ghiChu;
      textQRCode.text = widget.listPhongKham.qrCode;
      textDiaChiPhong.text = widget.listPhongKham.diaChiPhong;
      textMaMau.text = widget.listPhongKham.maMau;
      textSTT.text = widget.listPhongKham.stt;
      textSTTPhong.text = widget.listPhongKham.sttPhong;
      textChuyenKhoa.text = widget.listPhongKham.chuyenKhoa;
      textKhoa = widget.listPhongKham.khoa;
      textKhoaNoiTru = widget.listPhongKham.khoaNoiTru;
      textChuyenKhoaDrop = widget.listPhongKham.chuyenKhoaDrop;
      textLoaiPhong = widget.listPhongKham.loaiPhong;
      textCongKham = widget.listPhongKham.congKham;
    }
    _focusMaPhong.addListener(_onFocusChange);
    _focusMaPhongBHYT.addListener(_onFocusChange);
    _focusSoPhong.addListener(_onFocusChange);
    _focusTenPhong.addListener(_onFocusChange);
    _focusMaDauDoc.addListener(_onFocusChange);
    _focusGhiChu.addListener(_onFocusChange);
    _focusQRCode.addListener(_onFocusChange);
    _focusDiaChiPhong.addListener(_onFocusChange);
    _focusMaMau.addListener(_onFocusChange);
    _focusSTT.addListener(_onFocusChange);
    _focusSTTPhong.addListener(_onFocusChange);
    _focusChuyenKhoa.addListener(_onFocusChange);
    // textMaPhong.text = 'Phong 1';
  }

  @override
  void dispose() {
    _focusMaPhong.dispose();
    _focusMaPhongBHYT.dispose();
    _focusSoPhong.dispose();
    _focusTenPhong.dispose();
    _focusMaDauDoc.dispose();
    _focusGhiChu.dispose();
    _focusQRCode.dispose();
    _focusDiaChiPhong.dispose();
    _focusMaMau.dispose();
    _focusSTT.dispose();
    _focusSTTPhong.dispose();
    _focusChuyenKhoa.dispose();

    super.dispose();
  }

  void _onFocusChange() {
    setState(() {
      widget.listPhongKham.maPhong = textMaPhong.text;
      widget.listPhongKham.maPhongBhyt = textMaPhongBHYT.text;
      widget.listPhongKham.soPhong = textSoPhong.text;
      widget.listPhongKham.tenPhong = textTenPhong.text;
      widget.listPhongKham.maDauDoc = textMaDauDoc.text;
      widget.listPhongKham.ghiChu = textGhiChu.text;
      widget.listPhongKham.qrCode = textQRCode.text;
      widget.listPhongKham.diaChiPhong = textDiaChiPhong.text;
      widget.listPhongKham.maMau = textMaMau.text;
      widget.listPhongKham.stt = textSTT.text;
      widget.listPhongKham.sttPhong = textSTTPhong.text;
      widget.listPhongKham.chuyenKhoa = textChuyenKhoa.text;
    });
  }

  void updateCheckPhongGiaoSu() {
    setState(() {
      widget.listPhongKham.checkPhongGiaoSu =
          !widget.listPhongKham.checkPhongGiaoSu;
    });
  }

  void updateCheckDangKiHen() {
    setState(() {
      widget.listPhongKham.checkDangKiHen =
          !widget.listPhongKham.checkDangKiHen;
    });
  }

  void updateCheckSuDung() {
    setState(() {
      widget.listPhongKham.checkSuDung = !widget.listPhongKham.checkSuDung;
    });
  }

  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    void guinhanValue(String loai) async {
      var value = await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ListDepartment(
                    loai: loai,
                  )));
      if (value != null) {
        if (value['khoa'] != null) {
          setState(() {
            textKhoa = value['khoa'];
            widget.listPhongKham.khoa = textKhoa;
          });
        }
        if (value['khoa nội trú'] != null) {
          setState(() {
            textKhoaNoiTru = value['khoa nội trú'];
            widget.listPhongKham.khoaNoiTru = textKhoaNoiTru;
          });
        }
        if (value['chuyên khoa'] != null) {
          setState(() {
            textChuyenKhoaDrop = value['chuyên khoa'];
            widget.listPhongKham.chuyenKhoaDrop = textChuyenKhoaDrop;
          });
        }
        if (value['phòng'] != null) {
          setState(() {
            textLoaiPhong = value['phòng'];
            widget.listPhongKham.loaiPhong = textLoaiPhong;
          });
        }
        if (value['công khám'] != null) {
          setState(() {
            textCongKham = value['công khám'];
            widget.listPhongKham.congKham = textCongKham;
          });
        }
      } else {
        value = null;
      }
    }

    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: widget.checkAddEdit == true
                  ? 'Thêm mới phòng'
                  : 'Sửa thông tin phòng',
              check: false,
              onPress: () {
                Navigator.pop(context, [widget.listPhongKham, 'back']);
              },
            ),
          ),
          body: Scaffold(
            body: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 8,
                vertical: 8,
              ),
              child: Container(
                  height: MediaQuery.of(context).size.height,
                  child: SingleChildScrollView(
                    child: Column(children: [
                      Card(
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                            side: BorderSide(color: Colors.white)),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 12,
                            vertical: 12,
                          ),
                          child: Column(
                            children: [
                              CustomTextElevated(
                                onpressed: () {
                                  guinhanValue('khoa');
                                },
                                text: textKhoa,
                                title: 'Khoa',
                                checkRangBuoc: true,
                              ),
                              CustomTextElevated(
                                onpressed: () {
                                  guinhanValue('khoa nội trú');
                                },
                                text: textKhoaNoiTru,
                                title: 'Khoa nội trú',
                              ),
                              CustomTextField(
                                keyText: 'Mã phòng',
                                checkRangBuoc: true,
                                focusNode: _focusMaPhong,
                                textEditingController: textMaPhong,
                                onpress: () {
                                  textMaPhong.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'Mã phòng bhyt',
                                focusNode: _focusMaPhongBHYT,
                                textEditingController: textMaPhongBHYT,
                                onpress: () {
                                  textMaPhongBHYT.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'Số phòng',
                                checkRangBuoc: true,
                                focusNode: _focusSoPhong,
                                textEditingController: textSoPhong,
                                onpress: () {
                                  textSoPhong.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'Tên phòng',
                                checkRangBuoc: true,
                                focusNode: _focusTenPhong,
                                textEditingController: textTenPhong,
                                onpress: () {
                                  textTenPhong.clear();
                                },
                              ),
                              CustomTextElevated(
                                onpressed: () {
                                  guinhanValue('chuyên khoa');
                                },
                                text: textChuyenKhoaDrop,
                                title: 'Chuyên khoa',
                              ),
                              CustomTextElevated(
                                onpressed: () {
                                  guinhanValue('phòng');
                                },
                                text: textLoaiPhong,
                                title: 'Loại phòng',
                                checkRangBuoc: true,
                              ),
                              CustomTextField(
                                keyText: 'Mã đầu đọc',
                                focusNode: _focusMaDauDoc,
                                textEditingController: textMaDauDoc,
                                onpress: () {
                                  textMaDauDoc.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'Ghi chú',
                                textEditingController: textGhiChu,
                                focusNode: _focusGhiChu,
                                onpress: () {
                                  textGhiChu.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'QR code',
                                textEditingController: textQRCode,
                                focusNode: _focusQRCode,
                                onpress: () {
                                  textQRCode.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'Địa Chỉ Phòng',
                                focusNode: _focusDiaChiPhong,
                                textEditingController: textDiaChiPhong,
                                onpress: () {
                                  textDiaChiPhong.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'Chọn mã màu',
                                focusNode: _focusMaMau,
                                textEditingController: textMaMau,
                                onpress: () {
                                  textMaMau.clear();
                                },
                              ),
                              CustomTextCheckbox(
                                checkIcon:
                                    widget.listPhongKham.checkPhongGiaoSu,
                                text: 'Phòng giáo sư',
                                onpress: () {
                                  updateCheckPhongGiaoSu();
                                },
                              ),
                              CustomTextCheckbox(
                                checkIcon: widget.listPhongKham.checkDangKiHen,
                                text: 'Đăng ký hẹn khám',
                                onpress: () {
                                  updateCheckDangKiHen();
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        width: 240,
                        height: 60,
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            "Thông tin hiển thị trên màn hình đăng ký khám tự động",
                            style: TextStyle(
                                color: Color(0xFF4F82C2),
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      Card(
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                            side: BorderSide(color: Colors.white)),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 12,
                            vertical: 12,
                          ),
                          child: Column(
                            children: [
                              CustomTextField(
                                keyText: 'STT',
                                focusNode: _focusSTT,
                                textEditingController: textSTT,
                                onpress: () {
                                  textSTT.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'STT phòng',
                                focusNode: _focusSTTPhong,
                                textEditingController: textSTTPhong,
                                onpress: () {
                                  textSTTPhong.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'Chuyên khoa',
                                focusNode: _focusChuyenKhoa,
                                textEditingController: textChuyenKhoa,
                                onpress: () {
                                  textChuyenKhoa.clear();
                                },
                              ),
                              CustomTextElevated(
                                onpressed: () {
                                  guinhanValue('xác nhận');
                                },
                                text: textCongKham,
                                title: 'Xác nhận',
                              ),
                              CustomTextCheckbox(
                                checkIcon: widget.listPhongKham.checkSuDung,
                                text: 'Sử dụng',
                                onpress: () {
                                  updateCheckSuDung();
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ]),
                  )),
            ),
            bottomNavigationBar: Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(255, 88, 87, 87).withOpacity(0.2),
                    spreadRadius: 3,
                    blurRadius: 10,
                    offset: Offset(0, 3),
                  ),
                ],
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: CustomButton(
                          width: width_screen,
                          height: 30,
                          textButton: 'Lưu',
                          buttonIcon: SvgPicture.asset('assets/save.svg'),
                          onPressed: () {
                            if (widget.checkAddEdit == true) {
                              postData();
                              Navigator.pop(
                                  context, [widget.listPhongKham, 'add']);
                            } else {
                              putData();
                              Navigator.pop(
                                  context, [widget.listPhongKham, 'edit']);
                            }
                          }),
                    )
                  ],
                ),
              ),
            ),
          )),
    );
  }

  Future<void> postData() async {
    String apiUrl = 'https://64d2f44d67b2662bf3db88c8.mockapi.io/';
    String param = 'dichVuKham/';
    var test1 = jsonEncode(widget.listPhongKham.toJson());
    print(test1);
    await BaseApiNoToken().post(apiUrl, param, test1);
  }

  Future<void> putData() async {
    String apiUrl = 'https://64d2f44d67b2662bf3db88c8.mockapi.io/';
    String param = 'dichVuKham/${widget.listPhongKham.id}';
    var test1 = jsonEncode(widget.listPhongKham.toJson());
    print(test1);
    await BaseApiNoToken().put(apiUrl, param, test1);
  }
}
