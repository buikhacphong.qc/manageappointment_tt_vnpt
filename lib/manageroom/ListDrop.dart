import 'package:flutter/material.dart';

import '../common/BaseApiNoToken.dart';
import '../common/CustomAppBar.dart';
import '../model/listDrop.dart';

class ListDepartment extends StatefulWidget {
  final String loai;
  const ListDepartment({Key? key, required this.loai}) : super(key: key);

  @override
  State<ListDepartment> createState() => _ListDepartmentState();
}

class _ListDepartmentState extends State<ListDepartment> {
  ListDrop trave = ListDrop(
      listKhoa: [],
      listLoaiNv: [],
      listPhong: [],
      listGioiTinh: [],
      listKhoaNoiTru: [],
      listChuyenKhoa: [],
      listCongKham: []);
  @override
  void initState() {
    super.initState();
    loadData();
  }

  Future<void> loadData() async {
    String apiUrl = 'https://dichvukham.free.mockoapp.net/';
    String param = 'listDrop';
    final result = await BaseApiNoToken().get(apiUrl, param);
    var kqua = listDropFromJson(result);
    setState(() {
      trave = kqua;
    });
  }

  List<String> list = [];
  @override
  Widget build(BuildContext context) {
    if (widget.loai == 'khoa') {
      setState(() {
        list = trave.listKhoa;
      });
    } else if (widget.loai == 'phòng') {
      setState(() {
        list = trave.listPhong;
      });
    } else if (widget.loai == 'nhân viên') {
      list = trave.listLoaiNv;
    } else if (widget.loai == 'giới tính') {
      list = trave.listGioiTinh;
    } else if (widget.loai == 'khoa nội trú') {
      list = trave.listKhoaNoiTru;
    } else if (widget.loai == 'chuyên khoa') {
      list = trave.listChuyenKhoa;
    } else if (widget.loai == 'công khám') {
      list = trave.listCongKham;
    }
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          appBar:  PreferredSize(
              preferredSize: Size.fromHeight(50),
              child: CustomAppBar(
                text: 'Danh mục ${widget.loai}',
                check: false, 
                onPress: () {Navigator.pop(context);},
              ),
            ),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 8,
                  vertical: 8,
                ),
                child: Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: const Color.fromRGBO(255, 255, 255, 1),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.2),
                            spreadRadius: 3,
                            blurRadius: 10,
                            offset: Offset(0, 3),
                          ),
                        ]),
                    width: MediaQuery.of(context).size.width,
                    height: 40,
                    child: TextFormField(
                      // maxLines: 3,
                      style: TextStyle(
                        fontSize: 14,
                      ),
                      textAlign: TextAlign.left,
                      decoration: InputDecoration(
                          // hintText: 'Nhap',
                          suffixIcon: GestureDetector(
                            child: Icon(
                              Icons.clear,
                              size: 20,
                            ),
                            onTap: () {},
                          ),
                          prefixIcon: Icon(
                            Icons.search,
                            size: 20,
                          ),
                          border: InputBorder.none,
                          focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(width: 0.5, color: Colors.black)),
                          enabledBorder: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(8)),
                              borderSide:
                                  BorderSide(color: Colors.grey, width: 0.5))),
                    )),
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: 8,
                    vertical: 0,
                  ),
                  child: Container(
                    // color: Colors.blue,
                    child: ListView.builder(
                      itemCount: list.length,
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            Navigator.pop(context, {widget.loai: list[index]});
                          },
                          child: Container(
                            child: Padding(
                                padding: EdgeInsets.all(8),
                                child: Text(
                                  '${index + 1}. ${list[index]}',
                                  style: TextStyle(color: Color(0xFF4F82C2)),
                                )),
                            alignment: Alignment.centerLeft,
                            height: 40,
                            decoration: BoxDecoration(
                              border: Border(
                                  bottom: BorderSide(
                                    color: Color(0xFf4F82C2),
                                    width: 2.0,
                                  ),
                                  left: BorderSide.none,
                                  right: BorderSide.none),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ),
              )
            ],
          )),
    );
  }
}
