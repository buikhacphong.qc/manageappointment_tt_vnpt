import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../common/BaseApiNoToken.dart';
import '../common/CustomAppBar.dart';
import '../common/CustomButton.dart';
import '../item/ContainerManageStaff.dart';
import '../item/ItemManageStaff.dart';
import '../model/nhanVien.dart';
import 'AddEditStaff.dart';
import 'DetailStaff.dart';

// ignore: must_be_immutable
class ManageStaff extends StatefulWidget {
  bool checkLoading;
  ManageStaff({Key? key, required this.checkLoading}) : super(key: key);

  @override
  State<ManageStaff> createState() => _ManageStaffState();
}

class _ManageStaffState extends State<ManageStaff> {
  Map<String, String> resultTimKiem = {};
  List<dynamic> trave = [];

  void receiveData(Map<String, String> ketqua) {
    setState(() {
      resultTimKiem = ketqua;
      loadData();
    });
  }

  @override
  void initState() {
    super.initState();
    widget.checkLoading = false;
    loadData();
  }

  Future<void> loadData() async {
    String apiUrl = 'https://64d4af54b592423e46947f71.mockapi.io/';
    String param = 'nhanVien';
    final result = await BaseApiNoToken().get(apiUrl, param);
    List<dynamic> kqua = nhanVienFromJson(result);
    setState(() {
      trave = kqua;
      widget.checkLoading = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: 'Danh sách nhân viên',
              check: false,
              onPress: () {
                Navigator.pop(context);
              },
            ),
          ),
          body: Scaffold(
            body: Container(
              width: width_screen,
              height: MediaQuery.of(context).size.height - 136,
              child: Padding(
                padding: EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 0),
                child: ListView.builder(
                    itemCount: 2,
                    itemBuilder: (BuildContext context, index) {
                      if (index == 0) {
                        return Card(
                            elevation: 4,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                                side: BorderSide(
                                  color: Color.fromARGB(255, 178, 200, 219),
                                )),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                                color: Color.fromARGB(255, 178, 200, 219),
                              ),
                              width: MediaQuery.of(context).size.width,
                              height: 180,
                              child: ContainerManageStaff(
                                dataSend: receiveData,
                              ),
                            ));
                      } else {
                        return Container(
                          margin: EdgeInsets.only(top: 8),
                          // color: Color.fromARGB(255, 178, 200, 219),
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            children: [
                              Visibility(
                                child: CircularProgressIndicator(),
                                visible: !widget.checkLoading,
                              ),
                              Visibility(
                                visible: widget.checkLoading,
                                child: ListView.separated(
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: trave.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    NhanVien nhanVien = trave[index];
                                    return GestureDetector(
                                      onTap: () {
                                        Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        DetailStaff(
                                                            nhanVien:
                                                                nhanVien)))
                                            .then((value) {
                                          if (value[1] == 'xoa') {
                                            for (int i = 0;
                                                i < trave.length;
                                                i++) {
                                              NhanVien xoaNhanVien = trave[i];
                                              if (xoaNhanVien.id == value[0]) {
                                                setState(() {
                                                  trave.removeAt(i);
                                                });
                                              }
                                            }
                                          } else if (value[1] == 'sua') {
                                            NhanVien suaNhanVienTraVe =
                                                value[0];
                                            for (int i = 0;
                                                i < trave.length;
                                                i++) {
                                              NhanVien suaNhanVien = trave[i];
                                              if (suaNhanVien.id ==
                                                  suaNhanVienTraVe.id) {
                                                setState(() {
                                                  suaNhanVien =
                                                      suaNhanVienTraVe;
                                                });
                                              }
                                            }
                                          }
                                        });
                                      },
                                      child: ItemManageStaff(
                                          maNV: nhanVien.maNv,
                                          maBacSy: nhanVien.maBs,
                                          tenNV: nhanVien.tenNv,
                                          khoa: nhanVien.khoa,
                                          loaiNV: nhanVien.loaiNv,
                                          phong: nhanVien.phong),
                                    );
                                  },
                                  separatorBuilder:
                                      (BuildContext context, int index) {
                                    return Divider(
                                      height: 8,
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                        );
                      }
                    }),
              ),
            ),
            bottomNavigationBar: Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(255, 88, 87, 87).withOpacity(0.2),
                    spreadRadius: 3,
                    blurRadius: 10,
                    offset: Offset(0, 3),
                  ),
                ],
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: CustomButton(
                          width: width_screen / 2,
                          height: 30,
                          textButton: 'Thêm mới',
                          buttonIcon: SvgPicture.asset('assets/save.svg'),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AddEditStaff(
                                        check: true,
                                        nhanVien: NhanVien(
                                            maNv: '',
                                            tenNv: '',
                                            maBs: '',
                                            loaiNv: '',
                                            khoa: '',
                                            phong: '',
                                            email: '',
                                            chucDanh: '',
                                            trinhDo: '',
                                            tenPhong: '',
                                            hocHam: '',
                                            hocVi: '',
                                            sdt: '',
                                            diaChi: '',
                                            ngaySinh: '',
                                            gioiTinh: '',
                                            soBhyt: '',
                                            soBhxh: '',
                                            id: '',
                                            listPhong: [])))).then((value) {
                              setState(() {
                                if (value[1] == 'add') {
                                  trave.add(value);
                                  widget.checkLoading = true;
                                }
                              });
                            });
                          }),
                    ),
                    Container(
                      width: 5,
                    ),
                    Expanded(
                      child: CustomButton(
                          width: width_screen / 2,
                          height: 30,
                          textButton: 'Xuất DSVN',
                          buttonIcon: SvgPicture.asset('assets/save.svg'),
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                    ),
                  ],
                ),
              ),
            ),
          )),
    );
  }
}
