import 'dart:convert';

import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../common/BaseApiNoToken.dart';
import '../common/CustomAppBar.dart';
import '../common/CustomButton.dart';
import '../common/CustomeTextElevated.dart';
import '../common/CustomeTextField.dart';
import '../manageroom/ListDrop.dart';
import '../model/dichVuKham.dart';
import '../model/nhanVien.dart';
import '../servicecheckup/SelectRoom.dart';

// ignore: must_be_immutable
class AddEditStaff extends StatefulWidget {
  final bool check;
  NhanVien nhanVien;
  AddEditStaff({
    Key? key,
    required this.check,
    required this.nhanVien,
  }) : super(key: key);

  @override
  State<AddEditStaff> createState() => _AddEditStaffState();
}

class _AddEditStaffState extends State<AddEditStaff> {
  final Map<String, String> result = {};
  final FocusNode _focusMaNV = FocusNode();
  final FocusNode _focusTenNV = FocusNode();
  final FocusNode _focusMaBS = FocusNode();
  final FocusNode _focusEmail = FocusNode();
  final FocusNode _focusChucDanh = FocusNode();
  final FocusNode _focusTrinhDo = FocusNode();
  final FocusNode _focusTenPhong = FocusNode();
  final FocusNode _focusHocHam = FocusNode();
  final FocusNode _focusHocVi = FocusNode();
  final FocusNode _focusSDT = FocusNode();
  final FocusNode _focusDiaChi = FocusNode();
  final FocusNode _focusBHYT = FocusNode();
  final FocusNode _focusBHXH = FocusNode();

  final TextEditingController textMaNV = TextEditingController();
  final TextEditingController textTenNV = TextEditingController();
  final TextEditingController textMaBS = TextEditingController();
  final TextEditingController textEmail = TextEditingController();
  final TextEditingController textChucDanh = TextEditingController();
  final TextEditingController textTrinhDo = TextEditingController();
  final TextEditingController textTenPhong = TextEditingController();
  final TextEditingController textHocHam = TextEditingController();
  final TextEditingController textHocVi = TextEditingController();
  final TextEditingController textSDT = TextEditingController();
  final TextEditingController textDiaChi = TextEditingController();
  final TextEditingController textBHYT = TextEditingController();
  final TextEditingController textBHXH = TextEditingController();

  //lay ket qua drowdown
  String textLoaiNV = '';
  String textKhoa = '';
  String textPhong = '';
  String textGioiTinh = '';
  String textNgaySinh = '';
  @override
  void initState() {
    super.initState();
    if (widget.check == false) {
      textMaNV.text = widget.nhanVien.maNv;
      textTenNV.text = widget.nhanVien.tenNv;
      textMaBS.text = widget.nhanVien.maBs;
      textEmail.text = widget.nhanVien.email;
      textChucDanh.text = widget.nhanVien.chucDanh;
      textTrinhDo.text = widget.nhanVien.trinhDo;
      textTenPhong.text = widget.nhanVien.tenPhong;
      textHocHam.text = widget.nhanVien.hocHam;
      textHocVi.text = widget.nhanVien.hocVi;
      textSDT.text = widget.nhanVien.sdt;
      textDiaChi.text = widget.nhanVien.diaChi;
      textBHYT.text = widget.nhanVien.soBhyt;
      textBHXH.text = widget.nhanVien.soBhxh;
      textLoaiNV = widget.nhanVien.loaiNv;
      textKhoa = widget.nhanVien.khoa;
      textPhong = widget.nhanVien.phong;
      textGioiTinh = widget.nhanVien.gioiTinh;
      textNgaySinh = widget.nhanVien.ngaySinh;
    }
    _focusMaNV.addListener(_onFocusChange);
    _focusTenNV.addListener(_onFocusChange);
    _focusMaBS.addListener(_onFocusChange);
    _focusEmail.addListener(_onFocusChange);
    _focusChucDanh.addListener(_onFocusChange);
    _focusTrinhDo.addListener(_onFocusChange);
    _focusTenPhong.addListener(_onFocusChange);
    _focusHocHam.addListener(_onFocusChange);
    _focusHocVi.addListener(_onFocusChange);
    _focusSDT.addListener(_onFocusChange);
    _focusDiaChi.addListener(_onFocusChange);
    _focusBHYT.addListener(_onFocusChange);
    _focusBHXH.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    _focusMaNV.dispose();
    _focusTenNV.dispose();
    _focusMaBS.dispose();
    _focusEmail.dispose();
    _focusChucDanh.dispose();
    _focusTrinhDo.dispose();
    _focusTenPhong.dispose();
    _focusHocHam.dispose();
    _focusHocVi.dispose();
    _focusSDT.dispose();
    _focusDiaChi.dispose();
    _focusBHYT.dispose();
    _focusBHXH.dispose();

    super.dispose();
  }

  void _onFocusChange() {
    setState(() {
      widget.nhanVien.maNv = textMaNV.text;
      widget.nhanVien.tenNv = textTenNV.text;
      widget.nhanVien.maBs = textMaBS.text;
      widget.nhanVien.email = textEmail.text;
      widget.nhanVien.chucDanh = textChucDanh.text;
      widget.nhanVien.trinhDo = textTrinhDo.text;
      widget.nhanVien.tenPhong = textTenPhong.text;
      widget.nhanVien.hocHam = textHocHam.text;
      widget.nhanVien.hocVi = textHocVi.text;
      widget.nhanVien.sdt = textSDT.text;
      widget.nhanVien.diaChi = textDiaChi.text;
      widget.nhanVien.soBhyt = textBHYT.text;
      widget.nhanVien.soBhxh = textBHXH.text;
    });
  }

  DateTime dateTime = DateTime.now();
  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    void guinhangValue(String loai) async {
      var value = await Navigator.push(context,
          MaterialPageRoute(builder: (context) => ListDepartment(loai: loai)));
      if (value != null) {
        if (value['khoa'] != null) {
          setState(() {
            textKhoa = value['khoa'];
            widget.nhanVien.khoa = textKhoa;
          });
        }
        if (value['nhân viên'] != null) {
          setState(() {
            textLoaiNV = value['nhân viên'];
            widget.nhanVien.loaiNv = textLoaiNV;
          });
        }
        if (value['phòng'] != null) {
          setState(() {
            textPhong = value['phòng'];
            widget.nhanVien.tenPhong = textPhong;
          });
        }
        if (value['giới tính'] != null) {
          setState(() {
            textGioiTinh = value['giới tính'];
            widget.nhanVien.gioiTinh = textGioiTinh;
          });
        }
      } else {
        value = null;
      }
    }

    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(50),
            child: CustomAppBar(
              text: widget.check == true
                  ? 'Thêm mới nhân viên'
                  : 'Sửa thông tin nhân viên',
              check: false,
              onPress: () {
                Navigator.pop(context, [widget.nhanVien, 'back']);
              },
            ),
          ),
          body: Scaffold(
            body: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 8,
                vertical: 8,
              ),
              child: Container(
                  height: MediaQuery.of(context).size.height,
                  child: SingleChildScrollView(
                    child: Column(children: [
                      Card(
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                            side: BorderSide(color: Colors.white)),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 12,
                            vertical: 12,
                          ),
                          child: Column(
                            children: [
                              CustomTextField(
                                keyText: 'Mã N/viên',
                                checkRangBuoc: true,
                                focusNode: _focusMaNV,
                                textEditingController: textMaNV,
                                onpress: () {
                                  textMaNV.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'Tên N/viên',
                                checkRangBuoc: true,
                                focusNode: _focusTenNV,
                                textEditingController: textTenNV,
                                onpress: () {
                                  textTenNV.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'Mã bác sỹ',
                                focusNode: _focusMaBS,
                                textEditingController: textMaBS,
                                onpress: () {
                                  textMaBS.clear();
                                },
                              ),
                              CustomTextElevated(
                                onpressed: () {
                                  guinhangValue('nhân viên');
                                },
                                text: textLoaiNV,
                                title: 'Loại N/viên',
                              ),
                              CustomTextElevated(
                                onpressed: () {
                                  guinhangValue('khoa');
                                },
                                text: textKhoa,
                                title: 'Khoa',
                                checkRangBuoc: true,
                              ),
                              CustomTextElevated(
                                onpressed: () {
                                  guinhangValue('phòng');
                                },
                                text: textPhong,
                                title: 'Phòng',
                              ),
                              CustomTextField(
                                keyText: 'Email',
                                focusNode: _focusEmail,
                                textEditingController: textEmail,
                                onpress: () {
                                  textEmail.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'Chức danh',
                                focusNode: _focusChucDanh,
                                textEditingController: textChucDanh,
                                onpress: () {
                                  textChucDanh.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'Trình độ',
                                focusNode: _focusTrinhDo,
                                textEditingController: textTrinhDo,
                                onpress: () {
                                  textTrinhDo.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'Tên phòng',
                                focusNode: _focusTenPhong,
                                textEditingController: textTenPhong,
                                onpress: () {
                                  textTenPhong.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'Học hàm',
                                focusNode: _focusHocHam,
                                textEditingController: textHocHam,
                                onpress: () {
                                  textHocHam.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'Học vị',
                                focusNode: _focusHocVi,
                                textEditingController: textHocVi,
                                onpress: () {
                                  textHocVi.clear();
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                      Card(
                        elevation: 4,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                            side: BorderSide(color: Colors.white)),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 12,
                            vertical: 12,
                          ),
                          child: Column(
                            children: [
                              CustomTextField(
                                keyText: 'Số điện thoại',
                                focusNode: _focusSDT,
                                textEditingController: textSDT,
                                onpress: () {
                                  textSDT.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'Địa chỉ',
                                focusNode: _focusDiaChi,
                                textEditingController: textDiaChi,
                                onpress: () {
                                  textDiaChi.clear();
                                },
                              ),
                              CustomTextElevated(
                                onpressed: () {
                                  _show();
                                },
                                text: textNgaySinh,
                                title: 'Ngày sinh',
                                icon: Icons.calendar_month,
                              ),
                              CustomTextElevated(
                                onpressed: () {
                                  guinhangValue('giới tính');
                                },
                                text: textGioiTinh,
                                title: 'Giới tính',
                              ),
                              CustomTextField(
                                keyText: 'Số BHYT',
                                focusNode: _focusBHYT,
                                textEditingController: textBHYT,
                                onpress: () {
                                  textBHYT.clear();
                                },
                              ),
                              CustomTextField(
                                keyText: 'Số BHXH',
                                focusNode: _focusBHXH,
                                textEditingController: textBHXH,
                                onpress: () {
                                  textBHXH.clear();
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ]),
                  )),
            ),
            bottomNavigationBar: Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Color.fromARGB(255, 88, 87, 87).withOpacity(0.2),
                    spreadRadius: 3,
                    blurRadius: 10,
                    offset: Offset(0, 3),
                  ),
                ],
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: CustomButton(
                          width: width_screen / 2,
                          height: 30,
                          textButton: 'Lưu',
                          buttonIcon: SvgPicture.asset('assets/save.svg'),
                          onPressed: () {
                            if (widget.check == true) {
                              postData();
                              Navigator.pop(context, [widget.nhanVien, 'add']);
                            } else {
                              putData();
                              Navigator.pop(context, [widget.nhanVien, 'edit']);
                            }
                          }),
                    ),
                    Container(
                      width: 5,
                    ),
                    Expanded(
                      child: CustomButton(
                          width: width_screen / 2,
                          height: 30,
                          textButton: 'Chọn phòng',
                          buttonIcon: SvgPicture.asset(
                            'assets/room.svg',
                            width: 14,
                            height: 14,
                          ),
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => SelectRoom(
                                          checkBool: false,
                                          dichVuKham: DichVuKham(
                                              maDichVu: '',
                                              tenDichVu: '',
                                              phiTuVanCu: '',
                                              phiTuVanMoi: '',
                                              phiDatLichCu: '',
                                              phiDatLichMoi: '',
                                              ngayAdGiaMoi: '',
                                              trangThai: false,
                                              id: '',
                                              listPhong: []),
                                          nhanVien: widget.nhanVien,
                                        ))).then((value) {
                              if (value[1] == 'update_nhanVien') {
                                setState(() {
                                  widget.nhanVien = value[0];
                                });
                              }
                            });
                          }),
                    )
                  ],
                ),
              ),
            ),
          )),
    );
  }

  void _show() async {
    final DateTime? result = await showDatePicker(
      context: context,
      initialDate: dateTime,
      firstDate: DateTime(2019),
      lastDate: DateTime(2024),
      locale: const Locale('vi'),
    );
    if (result != null) {
      setState(() {
        dateTime = result;
        textNgaySinh = '${formatDate(dateTime, [dd, '/', mm, '/', yyyy])}';

        dateTime.toString();
        print(dateTime);
      });
    }
  }

  Future<void> postData() async {
    String apiUrl = 'https://64d4af54b592423e46947f71.mockapi.io/';
    String param = 'nhanVien/';
    var test1 = jsonEncode(widget.nhanVien.toJson());
    print(test1);
    await BaseApiNoToken().post(apiUrl, param, test1);
  }

  Future<void> putData() async {
    String apiUrl = 'https://64d4af54b592423e46947f71.mockapi.io/';
    String param = 'nhanVien/${widget.nhanVien.id}';
    var test1 = jsonEncode(widget.nhanVien.toJson());
    print(test1);
    await BaseApiNoToken().put(apiUrl, param, test1);
  }
}
