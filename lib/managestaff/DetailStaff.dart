import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../common/BaseApiNoToken.dart';
import '../common/CustomAppBar.dart';
import '../common/CustomButton.dart';
import '../common/CustomTextValue.dart';
import '../model/nhanVien.dart';
import 'AddEditStaff.dart';

// ignore: must_be_immutable
class DetailStaff extends StatefulWidget {
  NhanVien nhanVien;
  DetailStaff({Key? key, required this.nhanVien}) : super(key: key);

  @override
  State<DetailStaff> createState() => _DetailStaffState();
}

class _DetailStaffState extends State<DetailStaff> {
  @override
  Widget build(BuildContext context) {
    final width_screen = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(50),
        child: CustomAppBar(
          text: 'Chi tiết thông tin nhân viên',
          check: false,
          onPress: () {
            Navigator.pop(context, [widget.nhanVien, 'sua']);
          },
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 8,
          vertical: 8,
        ),
        child: Container(
          height: MediaQuery.of(context).size.height - 152,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Wrap(children: [
                  Card(
                    elevation: 4,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                        side: BorderSide(color: Colors.white)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 12,
                        vertical: 12,
                      ),
                      child: Column(
                        children: [
                          CustomTextValue(
                              key1: 'Mã N/viên', value1: widget.nhanVien.maNv),
                          CustomTextValue(
                              key1: 'Tên N/viên',
                              value1: widget.nhanVien.tenNv),
                          CustomTextValue(
                              key1: 'Mã bác sỹ', value1: widget.nhanVien.maBs),
                          CustomTextValue(
                              key1: 'Loại N/viên',
                              value1: widget.nhanVien.loaiNv),
                          CustomTextValue(
                              key1: 'Khoa', value1: widget.nhanVien.khoa),
                          CustomTextValue(
                              key1: 'Phòng', value1: widget.nhanVien.phong),
                          CustomTextValue(
                              key1: 'Email', value1: widget.nhanVien.email),
                          CustomTextValue(
                              key1: 'Chức danh',
                              value1: widget.nhanVien.chucDanh),
                          CustomTextValue(
                              key1: 'Trình độ',
                              value1: widget.nhanVien.trinhDo),
                          CustomTextValue(
                              key1: 'Học hàm', value1: widget.nhanVien.hocHam),
                          CustomTextValue(
                              key1: 'Học vị', value1: widget.nhanVien.hocVi),
                        ],
                      ),
                    ),
                  ),
                ]),
                Card(
                  elevation: 4,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8),
                      side: BorderSide(color: Colors.white)),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 12,
                      vertical: 12,
                    ),
                    child: Column(
                      children: [
                        CustomTextValue(
                            key1: 'Số điện thoại', value1: widget.nhanVien.sdt),
                        CustomTextValue(
                            key1: 'Địa chỉ', value1: widget.nhanVien.diaChi),
                        CustomTextValue(
                            key1: 'Ngày sinh',
                            value1: widget.nhanVien.ngaySinh),
                        CustomTextValue(
                            key1: 'Giới tính',
                            value1: widget.nhanVien.gioiTinh),
                        CustomTextValue(
                            key1: 'Số BHYT', value1: widget.nhanVien.soBhyt),
                        CustomTextValue(
                            key1: 'Số BHXH', value1: widget.nhanVien.soBhxh),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: Container(
        width: MediaQuery.of(context).size.width,
        height: 50,
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Color.fromARGB(255, 88, 87, 87).withOpacity(0.2),
              spreadRadius: 3,
              blurRadius: 10,
              offset: Offset(0, 3),
            ),
          ],
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                child: CustomButton(
                    width: width_screen / 2,
                    height: 30,
                    textButton: 'Sửa',
                    buttonIcon: SvgPicture.asset('assets/edit.svg'),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AddEditStaff(
                                  check: false,
                                  nhanVien: widget.nhanVien))).then((value) {
                        if (value[1] == 'edit') {
                          loadData(value[0]);
                        }
                      });
                    }),
              ),
              Container(
                width: 5,
              ),
              Expanded(
                child: CustomButton(
                    width: width_screen / 2,
                    height: 30,
                    textButton: 'Xóa',
                    buttonIcon: SvgPicture.asset('assets/deny.svg'),
                    onPressed: () {
                      deleteData();
                      Navigator.pop(context, [widget.nhanVien.id, 'xoa']);
                    }),
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> deleteData() async {
    String apiUrl = 'https://64d4af54b592423e46947f71.mockapi.io/';
    String param = 'nhanVien/${widget.nhanVien.id}';
    await BaseApiNoToken().delete(apiUrl, param);
  }

  void loadData(NhanVien value) {
    setState(() {
      widget.nhanVien = value;
    });
  }
}
