import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FailApiPopup extends StatefulWidget {
  final String trangthai_api;
  const FailApiPopup({super.key, required this.trangthai_api});

  @override
  State<FailApiPopup> createState() => _FailApiPopupState();
}

class _FailApiPopupState extends State<FailApiPopup> {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        width: 300,
        height: 230,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.white,
        ),
        child: Column(children: <Widget>[
          Padding(
              padding: EdgeInsets.symmetric(vertical: 7),
              child: SvgPicture.asset(
                'assets/api_fail.svg',
                width: 70,
                height: 70,
              )),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5),
            child: Text(
              'RẤT TIẾC',
              style: TextStyle(
                color: Color(0xFF444444),
                fontSize: 20,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5),
            child: Text(
              widget.trangthai_api,
              style: TextStyle(
                color: Color(0xFF535858),
                fontSize: 14,
              ),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(top: 10, bottom: 0, left: 15, right: 15),
              child: ElevatedButton(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [Text('OK')],
                ),
                onPressed: () {},
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStatePropertyAll(Color(0xFF6F9BD4)),
                    foregroundColor: MaterialStatePropertyAll(Colors.white),
                    side: MaterialStatePropertyAll(
                        BorderSide(color: Color(0xFF6F9BD4))),
                    shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)))),
              )),
        ]),
      ),
    );
  }
}
