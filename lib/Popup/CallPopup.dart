import 'package:flutter/material.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CallPopup extends StatefulWidget {
  const CallPopup({super.key});

  @override
  State<CallPopup> createState() => _CallPopupState();
}

class _CallPopupState extends State<CallPopup> {
  SvgPicture? svgPicture = null;
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        width: 300,
        height: 300,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          color: Colors.white,
        ),
        child: Column(children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 10, bottom: 0, right: 5, left: 270),
            child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: SvgPicture.asset('assets/cancel.svg')),
          ),
          Padding(
              padding: EdgeInsets.symmetric(vertical: 7),
              child: SvgPicture.asset('assets/call_popup.svg')),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5),
            child: Text(
              'GỌI ĐIỆN',
              style: TextStyle(
                color: Color(0xFF444444),
                fontSize: 20,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 5),
            child: Text(
              'Vui lòng chọn phương thức gọi',
              style: TextStyle(
                color: Color(0xFF535858),
                fontSize: 14,
              ),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(top: 10, bottom: 0, left: 15, right: 15),
              child: ElevatedButton(
                // ignore: sort_child_properties_last
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [Text('Gọi qua vnCare')],
                ),
                onPressed: () {},
                style: ButtonStyle(
                    backgroundColor:
                        MaterialStatePropertyAll(Color(0xFF6F9BD4)),
                    foregroundColor: MaterialStatePropertyAll(Colors.white),
                    side: MaterialStatePropertyAll(
                        BorderSide(color: Color(0xFF6F9BD4))),
                    shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)))),
              )),
          Padding(
              padding: EdgeInsets.only(top: 0, bottom: 5, left: 15, right: 15),
              child: ElevatedButton(
                // ignore: sort_child_properties_last
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [Text('Gọi bằng sim điện thoại')],
                ),
                onPressed: () {
                  callPhone();
                },
                style: ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(Colors.white),
                    foregroundColor:
                        MaterialStatePropertyAll(Color(0xFF6F9BD4)),
                    side: MaterialStatePropertyAll(
                        BorderSide(color: Color(0xFF6F9BD4))),
                    shape: MaterialStatePropertyAll(RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8)))),
              ))
        ]),
      ),
    );
  }

  Future<void> callPhone() async {
    bool? res = await FlutterPhoneDirectCaller.callNumber('0978137372');
  }
}
