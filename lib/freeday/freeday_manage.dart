import 'dart:convert';

import 'package:flutter/material.dart';
// import 'package:test_item/api/callapi.dart';
// import 'package:test_item/freeday/add_freeday.dart';
// import 'package:test_item/freeday/listSearchFreeDay.dart';

import '../api/callapi.dart';
import '../item/item_freeday.dart';
import '../model/freeday.dart';
import 'listSearchFreeDay.dart';

class FreeDayManager extends StatefulWidget {
  const FreeDayManager({super.key});

  @override
  State<FreeDayManager> createState() => _FreeDayManagerState();
}

class _FreeDayManagerState extends State<FreeDayManager> {
  List<FreeDay> listItem = [];
  List<FreeDay> responseAPI = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    callAPI();
  }

  void searchFreeDay(List<String> value) {
    print(value[0]);
    print(value[1]);
    print('vao search');
    List<FreeDay> listSearch = [];
    for (int i = 0; i < listItem.length; i++) {
      String loai = listItem[i].type == '1'
          ? "Ngay cu the trong nam"
          : "Ngay co dinh hang tuan";
      if (loai.toLowerCase().contains(value[0]) &&
          ((listItem[i].thu!.toLowerCase().contains(value[1])) ||
              (listItem[i].fromDay!.contains(value[1])) ||
              (listItem[i].toDay!.contains(value[1])))) {
        listSearch.add(listItem[i]);
      }
    }
    setState(() {
      listItem = listSearch;
    });
  }

  void initDataAgain() {
    setState(() {
      listItem = responseAPI;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        backgroundColor: const Color(0xFFEEEEEE),
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: Color(0xFF6F9BD4),
          title: const Text(
            'Quản lý ngày nghỉ',
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ),
        body: listItem.length > 0
            ? ListView.builder(
                itemCount: listItem.length,
                itemBuilder: (BuildContext context, int index) {
                  if (index == 0) {
                    return Column(
                      children: [
                        ListSearchFreeDay(
                          addFreeDay: (value) {
                            if (value == 'refresh') {
                              callAPI();
                            }
                          },
                          SearchFreeDay: (value) {
                            initDataAgain();
                            searchFreeDay(value);
                          },
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        ItemFreeday(
                          freeDay: listItem[index],
                          callbackdata: (value) {
                            if (value == 'refresh') {
                              callAPI();
                            }
                          },
                        )
                      ],
                    );
                  }
                  return ItemFreeday(
                    freeDay: listItem[index],
                    callbackdata: (value) {
                      if (value == 'refresh') {
                        callAPI();
                      }
                    },
                  );
                })
            : Column(
                children: [
                  Expanded(
                    flex: 1,
                    child: ListSearchFreeDay(
                      SearchFreeDay: (value) {
                        initDataAgain();
                        searchFreeDay(value);
                      },
                      addFreeDay: (value) {
                        if (value == 'refresh') {
                          callAPI();
                        }
                      },
                    ),
                  ),
                  const Expanded(
                      flex: 2,
                      child: Center(
                        child: Text('Không có data'),
                      )),
                ],
              ),
      ),
    );
  }

  void callAPI() async {
    try {
      String url =
          'https://64d48506b592423e46943417.mockapi.io/flutter/api/freeday';
      final result = await BaseAPI().get(url);
      final List<dynamic> jsonList = json.decode(result);
      final List<FreeDay> timeBlocks =
          jsonList.map((json) => FreeDay.fromJson(json)).toList();
      setState(() {
        listItem = timeBlocks;
        responseAPI = timeBlocks;
      });
    } catch (e) {
      print('===============\nco loi call api freeday' + e.toString());
    }
  }
}
