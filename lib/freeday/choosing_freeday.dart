import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';

import '../item/line_text.dart';

class ChoosingFreeDay extends StatelessWidget {
  const ChoosingFreeDay(
      {required this.isMorning,
      required this.isAfternoon,
      required this.isRemote,
      required this.isCSYT,
      required this.onClickMorning,
      required this.onClickAfternoon,
      required this.onClickRemote,
      required this.onClickCSYT,
      super.key});
  final bool isMorning;
  final bool isAfternoon;
  final bool isRemote;
  final bool isCSYT;
  final Function(bool) onClickMorning;
  final Function(bool) onClickAfternoon;
  final Function(bool) onClickRemote;
  final Function(bool) onClickCSYT;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        MyWidget(title: 'Nghỉ buổi', text: ''),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            GestureDetector(
              onTap: () {
                onClickMorning(!isMorning);
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(
                    isMorning
                        ? Icons.check_box_outlined
                        : Icons.crop_square_outlined,
                    size: 14,
                    color: Colors.blue,
                  ),
                  Text('Sáng', style: TextStyle(fontSize: 12))
                ],
              ),
            ),
            GestureDetector(
              onTap: () {
                onClickAfternoon(!isAfternoon);
              },
              child: Row(
                children: [
                  Icon(
                    isAfternoon
                        ? Icons.check_box_outlined
                        : Icons.crop_square_outlined,
                    size: 14,
                    color: Colors.blue,
                  ),
                  Text('Chiều', style: TextStyle(fontSize: 12))
                ],
              ),
            )
          ],
        ),
        MyWidget(title: 'Nghỉ dịch vụ', text: ''),
        Padding(
          padding: EdgeInsets.fromLTRB(70, 5, 15, 5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: GestureDetector(
                  onTap: () {
                    onClickRemote(!isRemote);
                  },
                  child: Row(
                    children: [
                      Icon(
                        isRemote
                            ? Icons.check_box_outlined
                            : Icons.crop_square_outlined,
                        size: 14,
                        color: Colors.blue,
                      ),
                      const Text(
                        'Tư vấn từ xa',
                        style: TextStyle(fontSize: 12),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(8, 0, 15, 5),
                child: GestureDetector(
                  onTap: () {
                    onClickCSYT(!isCSYT);
                  },
                  child: Row(
                    children: [
                      Icon(
                        isCSYT
                            ? Icons.check_box_outlined
                            : Icons.crop_square_outlined,
                        size: 14,
                        color: Colors.blue,
                      ),
                      Text('Khám tại CSYT', style: TextStyle(fontSize: 12))
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
