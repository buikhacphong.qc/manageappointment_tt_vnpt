// To parse this JSON data, do
//
//     final listDrop = listDropFromJson(jsonString);

import 'dart:convert';

ListDrop listDropFromJson(String str) => ListDrop.fromJson(json.decode(str));

String listDropToJson(ListDrop data) => json.encode(data.toJson());

class ListDrop {
    List<String> listKhoa;
    List<String> listLoaiNv;
    List<String> listPhong;
    List<String> listGioiTinh;
    List<String> listKhoaNoiTru;
    List<String> listChuyenKhoa;
    List<String> listCongKham;

    ListDrop({
        required this.listKhoa,
        required this.listLoaiNv,
        required this.listPhong,
        required this.listGioiTinh,
        required this.listKhoaNoiTru,
        required this.listChuyenKhoa,
        required this.listCongKham,
    });

    factory ListDrop.fromJson(Map<String, dynamic> json) => ListDrop(
        listKhoa: List<String>.from(json["listKhoa"].map((x) => x)),
        listLoaiNv: List<String>.from(json["listLoaiNV"].map((x) => x)),
        listPhong: List<String>.from(json["listPhong"].map((x) => x)),
        listGioiTinh: List<String>.from(json["listGioiTinh"].map((x) => x)),
        listKhoaNoiTru: List<String>.from(json["listKhoaNoiTru"].map((x) => x)),
        listChuyenKhoa: List<String>.from(json["listChuyenKhoa"].map((x) => x)),
        listCongKham: List<String>.from(json["listCongKham"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "listKhoa": List<dynamic>.from(listKhoa.map((x) => x)),
        "listLoaiNV": List<dynamic>.from(listLoaiNv.map((x) => x)),
        "listPhong": List<dynamic>.from(listPhong.map((x) => x)),
        "listGioiTinh": List<dynamic>.from(listGioiTinh.map((x) => x)),
        "listKhoaNoiTru": List<dynamic>.from(listKhoaNoiTru.map((x) => x)),
        "listChuyenKhoa": List<dynamic>.from(listChuyenKhoa.map((x) => x)),
        "listCongKham": List<dynamic>.from(listCongKham.map((x) => x)),
    };
}
