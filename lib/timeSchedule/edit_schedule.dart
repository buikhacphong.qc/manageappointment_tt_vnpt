import 'package:flutter/material.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:test_item/item/CustomDropDown.dart';
// import 'package:test_item/item/elevated_button.dart';
// import 'package:test_item/timeSchedule/createschedule.dart';
// import 'package:test_item/timeSchedule/edit_textformfield.dart';

import '../api/callapi.dart';
import '../item/CustomDropDown.dart';
import '../item/box_decoration_container.dart';
import '../item/elevated_button.dart';
import '../item/line_dropbutton.dart';
import '../model/time.dart';
import 'edit_textformfield.dart';

class EditTimeAppointment extends StatefulWidget {
  const EditTimeAppointment({required this.editTimes, super.key});
  final Times editTimes;
  @override
  State<EditTimeAppointment> createState() => _EditTimeAppointmentState();
}

class _EditTimeAppointmentState extends State<EditTimeAppointment> {
  late final _textEditingController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _textEditingController = TextEditingController(text: widget.editTimes.note);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).unfocus();
      },
      child: Scaffold(
        backgroundColor: const Color(0xFFEEEEEE),
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: Color(0xFF6F9BD4),
          title: const Text(
            'Sửa lịch trình khám',
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ),
        body: Scaffold(
          body: Column(
            children: [
              Expanded(
                child: Container(
                  margin: EdgeInsets.fromLTRB(10, 10, 8, 0),
                  padding: EdgeInsets.all(8),
                  decoration: BoxecorationContainer,
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        CustomDropDownByMyself(
                            title: 'TG BĐ sáng (*)',
                            value: widget.editTimes.timeStartAM!,
                            icon: Icons.arrow_drop_down_outlined,
                            keyData: 'BeginTimeAM',
                            onPress: (value) {
                              setState(() {
                                widget.editTimes.timeStartAM = value;
                              });
                            }),
                        CustomDropDownByMyself(
                            title: 'TG KT sáng (*)',
                            value: widget.editTimes.timeEndAM!,
                            icon: Icons.arrow_drop_down_outlined,
                            keyData: 'FinishTimeAM',
                            onPress: (value) {
                              setState(() {
                                widget.editTimes.timeEndAM = value;
                              });
                            }),
                        CustomDropDownByMyself(
                            title: 'TG BĐ chiều (*)',
                            value: widget.editTimes.timeStartPM!,
                            icon: Icons.arrow_drop_down_outlined,
                            keyData: 'BeginTimePM',
                            onPress: (value) {
                              setState(() {
                                widget.editTimes.timeStartPM = value;
                              });
                            }),
                        CustomDropDownByMyself(
                            title: 'TG KT chiều (*)',
                            value: widget.editTimes.timeEndPM!,
                            icon: Icons.arrow_drop_down_outlined,
                            keyData: 'FinishTimePM',
                            onPress: (value) {
                              setState(() {
                                widget.editTimes.timeEndPM = value;
                              });
                            }),
                        CustomDropDownByMyself(
                            title: 'TG khám TB/ BN',
                            value: widget.editTimes.time!,
                            icon: Icons.arrow_drop_down_outlined,
                            keyData: 'TimePerPatient',
                            onPress: (value) {
                              setState(() {
                                widget.editTimes.time = value;
                              });
                            }),
                        CustomDropDownByMyself(
                            title: 'SL BN/ thời điểm',
                            value: widget.editTimes.amountPatient!,
                            icon: Icons.arrow_drop_down_outlined,
                            keyData: 'AmountPatient',
                            onPress: (value) {
                              setState(() {
                                widget.editTimes.amountPatient = value;
                              });
                            }),
                        CustomDropDownByMyself(
                            title: 'Loại khám (*)',
                            value: widget.editTimes.state == 1
                                ? 'Khám tại cơ sở'
                                : 'Tư vấn từ xa',
                            icon: Icons.arrow_drop_down_outlined,
                            keyData: 'Type',
                            onPress: (value) {
                              setState(() {
                                widget.editTimes.state =
                                    value == 'Khám tại cơ sở' ? 1 : 0;
                              });
                            }),
                        EditTextFormField(
                            textEditingController: _textEditingController,
                            title: 'Ghi chú'),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          bottomNavigationBar: Container(
            height: 35,
            margin: EdgeInsets.fromLTRB(10, 10, 10, 5),
            // padding: EdgeInsets.fromLTRB(10, 10, 10, 5),
            child: CustomElevatedButton(
                onClickIcon: () {
                  print(widget.editTimes.timeStartAM);
                  updateTime();
                },
                text: 'Lưu',
                icon: Icons.save_alt,
                color: Color(0xFF6F9BD4),
                colorText: Colors.white),
          ),
        ),
      ),
    );
  }

  void updateTime() async {
    String hour1 =
        widget.editTimes.timeStartAM![0] + widget.editTimes.timeStartAM![1];
    String hour2 =
        widget.editTimes.timeEndAM![0] + widget.editTimes.timeEndAM![1];

    String hour3 =
        widget.editTimes.timeStartPM![0] + widget.editTimes.timeStartPM![1];
    String hour4 =
        widget.editTimes.timeEndPM![0] + widget.editTimes.timeEndPM![1];
    if (int.parse(hour1) > int.parse(hour2) ||
        int.parse(hour3) > int.parse(hour4)) {
      print('Thoi gian khong hop le');
    } else {
      String note = _textEditingController.text;
      setState(() {
        widget.editTimes.note = note;
      });
      try {
        String url =
            'https://64d2e2e667b2662bf3db7e0a.mockapi.io/api/test/time_appointment/';
        final result = await BaseAPI().updateStateAppointment(
            url, widget.editTimes.id!, widget.editTimes);
        Navigator.of(context).pop('refresh');
      } catch (e) {
        print('===============\nloi update: ' + e.toString());
      }
    }
    // Navigator.popUntil(context, ModalRoute.withName('/time_manage'));
  }
}
